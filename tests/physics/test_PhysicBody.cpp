/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_PhysicBody_ROOT_INCLUDER
#endif

#include "../../physics/PhysicBody.hpp"
#include "../../physics/Force.hpp"
#include "../../physics/PhysicLayer.hpp"
#include <glm/glm.hpp>
#include <memory>
#include <iostream>

using namespace nde;
using namespace glm;

SUITE(DynamicPhysicBody){

TEST(SingleSpeedForce)
{
    
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);
    body.addForce(std::make_shared<PlaneSpeedForce>
        (vec3(1.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f));

    body.applyForces(1.0f);

    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);

    body.addForce(std::make_shared<PlaneSpeedForce>
    (vec3(1.0f, 0.0f, 0.0f), 
    vec3(0.0f, 1.0f, 0.0f),
    2.0f));

    body.applyForces(2.0f);

    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);

    body.setSpeed(vec3(0.0f, 0.0f, 0.0f));

    body.addForce(std::make_shared<PlaneSpeedForce>
    (vec3(3.0f, 0.0f, 3.0f), 
    vec3(0.0f, 1.0f, 0.0f),
    2.0f));

    body.applyForces(1.0f);

    CHECK_CLOSE(body.getSpeed().x, 1.41421f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 1.41421f, 0.0001f);
}

TEST(SingleImpulse)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(1.0, 2.0, 3.0), 1.0));

    body.applyForces(0.5f);

    CHECK_CLOSE(body.getSpeed().x, 0.5f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 1.5f, 0.0001f);

    body.applyForces(1.5f);

    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 2.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 3.0f, 0.0001f);

    body.applyForces(1.0f);
    
    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 2.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 3.0f, 0.0001f);
}

TEST(OpposedSimpleSpeeds)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, 1.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(-1.0f, 0.0f, -1.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(11.5f);

    CHECK_CLOSE(body.getSpeed().x, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f); 

    //same with partially combined speed forces
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, -1.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, 1.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(11.9f);

    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f); 
}

TEST(OpposedImpulses)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(1.0, 2.0, 3.0), 1.0));
    body.addForce(std::make_shared<Impulse>(vec3(-1.0, 2.0, 3.0), 1.0));
    
    body.applyForces(0.5f);

    CHECK_CLOSE(body.getSpeed().x, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 2.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 3.0f, 0.0001f);

    body.applyForces(1.5f);

    CHECK_CLOSE(body.getSpeed().x, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 4.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 6.0f, 0.0001f);

    body.applyForces(1.0f);
    
    CHECK_CLOSE(body.getSpeed().x, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 4.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 6.0f, 0.0001f);
}

TEST(ResistiveSpeedForce)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(1.0, 0.0, 0.0), 1.0));
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(0.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(1.5f);

    CHECK_CLOSE(body.getSpeed().x, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);
}

TEST(PushingSpeedForceStoppingAcceleration)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(1.0, 0.0, 0.0), 1.0));
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(0.25f);

    CHECK_CLOSE(body.getSpeed().x, 0.5f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);

    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(0.4f);

    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);

}

TEST(RupturedSpeedForce)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(2.0, 0.0, 0.0), 1.0));
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(0.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(1.0f);

    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);
}

TEST(PushingRupturedSpeedForce)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(2.0, 0.0, 0.0), 3.0));
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.5f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(0.5f);

    CHECK_CLOSE(body.getSpeed().x, 1.5f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);

    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(0.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(1.0f);

    CHECK_CLOSE(body.getSpeed().x, 2.5f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);
}

TEST(PushingRupturedSpeedForce_ruptureInFrame)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(3.0, 0.0, 0.0), 3.0));
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(0.125f);

    CHECK_CLOSE(body.getSpeed().x, 0.5f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);

    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(0.625f);

    CHECK_CLOSE(body.getSpeed().x, 2.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 0.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);
}

TEST(NotInfluentSpeedForce)
{
    DynamicPhysicBody body(std::make_shared<PhysicLayer>(true, true), 1.0f);

    body.addForce(std::make_shared<Impulse>(vec3(0.0, 3.0, 0.0), 3.0));
    body.addForce(std::make_shared<PlaneSpeedForce>(
        vec3(1.0f, 0.0f, 0.0f), 
        vec3(0.0f, 1.0f, 0.0f),
        1.0f
    ));

    body.applyForces(2.0f);


    CHECK_CLOSE(body.getSpeed().x, 1.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().y, 6.0f, 0.0001f);
    CHECK_CLOSE(body.getSpeed().z, 0.0f, 0.0001f);
}

}

#ifdef TEST_PhysicBody_ROOT_INCLUDER
int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
#endif