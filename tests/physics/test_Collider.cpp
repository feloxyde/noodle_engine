/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_Collider_ROOT_INCLUDER
#endif

#include "../../physics/PhysicBody.hpp"
#include "../../core/CoreEventQueue.hpp"
#include "../../core/CoreEventListener.hpp"
#include "../../physics/CollisionEvent.hpp"
#include "../../physics/Force.hpp"
#include "../../physics/PhysicLayer.hpp"
#include "../../physics/Collider.hpp"
#include "../../core/GameObject.hpp"
#include <glm/glm.hpp>
#include <memory>
#include <iostream>
#include <vector>
#include "../../core/GameObject.hpp"

using namespace nde;
using namespace glm;

class CollisionReporter : public CoreEventListener<CollisionEvent>
{
public:
    CollisionReporter(std::shared_ptr<GameObject> listening,
    std::vector<std::shared_ptr<GameObject>> expectedColliding):
    m_listening(listening),
    m_expectedColliding(expectedColliding),
    m_collisionCount(expectedColliding.size())
    {}


    virtual  ~CollisionReporter(){}


    virtual void reaction(std::shared_ptr<CollisionEvent> event)
    {
        m_collisionCount--;

        if(event->getTargets()[0] == this->m_listening && event->getCollidingObject() == m_expectedColliding[0])
        {
            m_expectedColliding.erase(m_expectedColliding.begin());
        }
    }

    bool isReactionsOK()
    {
        return m_collisionCount == 0 && m_expectedColliding.size() == 0;
    }

private: 
    std::shared_ptr<GameObject> m_listening;
    std::vector<std::shared_ptr<GameObject>> m_expectedColliding;
    int m_collisionCount;
public: 
};

class TestGameObject : public GameObject
{

public:
    static std::shared_ptr<PhysicLayer> s_layer;
    static constexpr bool s_isLayerInit = false;
    static void initLayer()
    {
        if(!s_isLayerInit)
        {
            s_layer = std::make_shared<PhysicLayer>(true, true);
        }
    }
public:
    TestGameObject():
        GameObject::GameObject()
    {
        TestGameObject::initLayer();
        m_body = std::make_shared<DynamicPhysicBody>(
            TestGameObject::s_layer,
            1.0f);
        auto obb = new OrientedBoundingBox(1.0f, 1.0f, 1.0f);
        auto bodyPart = std::make_shared<BodyPart>(obb, m_body);
        bodyPartID_t id = m_body->addRootPart(bodyPart);
        //m_body->setOwner(this->weak_from_this());
        if(m_body == nullptr)
        {
            std::cout << "body is null omg" << std::endl;
        }
        
    }

    virtual ~TestGameObject(){}
};

std::shared_ptr<PhysicLayer> TestGameObject::s_layer;
constexpr bool TestGameObject::s_isLayerInit;

SUITE(Collider){

TEST(EventTriggeringSingleCollision)
{
    
    auto queue = std::make_shared<CoreEventQueue>();
    queue->addEventListener(std::make_shared<TargetedCoreEventBouncer>());

    std::cout << "queue init done" << std::endl;

    // A and B one single collision
    auto objectA = std::make_shared<TestGameObject>();
    auto objectB = std::make_shared<TestGameObject>();

    std::cout << "objects created" << std::endl;

    std::vector<std::shared_ptr<GameObject>> va;
    va.push_back(objectA);

    std::vector<std::shared_ptr<GameObject>> vb;
    vb.push_back(objectB);

    auto reporterA = std::make_shared<CollisionReporter>(objectA, vb);
    auto reporterB = std::make_shared<CollisionReporter>(objectB, va);

    auto bodyA = objectA->getPhysicBody();
    auto bodyB = objectB->getPhysicBody();
    bodyA->setOwner(objectA);
    bodyB->setOwner(objectB);

    objectA->addEventListener(reporterA);
    objectB->addEventListener(reporterB);


    Collision c(bodyA, bodyB, 0.1f, vec3(0.0f, 1.0f, 0.0f));
    
    Collider::collisionResponse(c, queue);

    queue->flushTo(100.0f);

    CHECK(reporterA->isReactionsOK());
    CHECK(reporterB->isReactionsOK());
}


TEST(CollisionResponse)
{

}






}

#ifdef TEST_Collider_ROOT_INCLUDER
int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
#endif