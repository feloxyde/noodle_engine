/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#include "../../core/CoreEventQueue.hpp"
#include "../../core/CoreEvent.hpp"
#include "../../core/CoreEventListener.hpp"
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_COREEVENTQUEUE_ROOT_INCLUDER
#endif



#include <vector>



using namespace nde;
using namespace std;



SUITE(CoreEventQueue)
{
class EventReporter : public CoreEventListener<CoreEvent>
{
public:
    EventReporter():
    CoreEventListener<CoreEvent>::CoreEventListener()
    {}
    virtual ~EventReporter(){}

    virtual void reaction(std::shared_ptr<CoreEvent> event)
    {
        m_timePoints.push_back(event->getTimePoint());
    }

public: 
    vector<timePoint_t> m_timePoints;
};

TEST(Order)
{
  
    CoreEventQueue queue;
    auto reporter = std::make_shared<EventReporter>();
    queue.addEventListener(reporter);

    queue.addEvent(std::make_shared<CoreEvent>(1.4f));
    queue.addEvent(std::make_shared<CoreEvent>(11.0f));
    queue.addEvent(std::make_shared<CoreEvent>(3.0f));
    queue.addEvent(std::make_shared<CoreEvent>(6.0f));
    queue.addEvent(std::make_shared<CoreEvent>(1.0f));
    queue.addEvent(std::make_shared<CoreEvent>(3.0f));
    
    queue.flushTo(12.0f);

    CHECK_CLOSE(reporter->m_timePoints[0], 1.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[1], 1.4f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[2], 3.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[3], 3.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[4], 6.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[5], 11.0f, 0.000001);
}




}

#ifdef TEST_COREEVENTQUEUE_ROOT_INCLUDER
int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
#endif