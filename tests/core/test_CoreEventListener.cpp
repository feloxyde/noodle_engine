/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#include "../../core/CoreEvent.hpp"
#include "../../core/CoreEventListener.hpp"
#include "../../utils/ndetypes.hpp"
#include <memory>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_COREEVENTLISTENER_ROOT_INCLUDER
#endif


/* write tests here */
/* dont forget to change FILENAME both over and under 
in the defines */

using namespace std;
using namespace nde;



SUITE(CoreEventListener)
{

class EventA : public CoreEvent
{
public:
    EventA(timePoint_t tp):
        CoreEvent::CoreEvent(tp)
        {}

    virtual ~EventA(){}
};


class EventB : public EventA
{
public:
    EventB(timePoint_t tp):
        EventA::EventA(tp)
        {}

    virtual ~EventB(){}
};

template <typename T>
class EventCounter : public CoreEventListener<T>
{
public:
    EventCounter():m_counter(0){}
    virtual  ~EventCounter(){}

    virtual void reaction(std::shared_ptr<T> event)
    {
        m_counter++;
    }

public: 
    unsigned int m_counter;

};

TEST(EventDetection)
{
    std::shared_ptr<EventCounter<CoreEvent>> counter = std::make_shared<EventCounter<CoreEvent>>();
    
    CHECK_EQUAL(counter->m_counter, 0);

    counter->reactTo(std::make_shared<CoreEvent>(0.0f));

    CHECK_EQUAL(counter->m_counter, 1);

    counter->reactTo(std::make_shared<CoreEvent>(0.0f));
    counter->reactTo(std::make_shared<CoreEvent>(0.0f));
    counter->reactTo(std::make_shared<CoreEvent>(0.0f));

    CHECK_EQUAL(counter->m_counter, 4);

    counter->reactTo(std::make_shared<EventA>(0.0f));
    
    CHECK_EQUAL(counter->m_counter, 5);

    counter->reactTo(std::make_shared<EventB>(0.0f));

    CHECK_EQUAL(counter->m_counter, 6);
}

TEST(EventRejection)
{
    //this one receives A and B
    std::shared_ptr<EventCounter<EventA>> counterA = std::make_shared<EventCounter<EventA>>();
    //this one receives only B
    std::shared_ptr<EventCounter<EventB>> counterB = std::make_shared<EventCounter<EventB>>();
    //none receives core
    std::shared_ptr<AbstractCoreEventListener> cA = counterA;
    std::shared_ptr<AbstractCoreEventListener> cB = counterB;

    CHECK_EQUAL(counterA->m_counter, 0);
    CHECK_EQUAL(counterB->m_counter, 0);

    cA->reactTo(std::make_shared<EventA>(0.0f));
    cB->reactTo(std::make_shared<EventA>(0.0f));

    CHECK_EQUAL(counterA->m_counter, 1);
    CHECK_EQUAL(counterB->m_counter, 0);

    cA->reactTo(std::make_shared<EventB>(0.0f));
    cB->reactTo(std::make_shared<EventB>(0.0f));

    CHECK_EQUAL(counterA->m_counter, 2);
    CHECK_EQUAL(counterB->m_counter, 1);

    cA->reactTo(std::make_shared<CoreEvent>(0.0f));
    cB->reactTo(std::make_shared<CoreEvent>(0.0f));

    CHECK_EQUAL(counterA->m_counter, 2);
    CHECK_EQUAL(counterB->m_counter, 1);
}

}

#ifdef TEST_COREEVENTLISTENER_ROOT_INCLUDER
int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
#endif