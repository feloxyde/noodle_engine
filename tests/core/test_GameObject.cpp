/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#include "../../core/GameObject.hpp"
#include "../../core/CoreEventListener.hpp"
#include "../../core/CoreEvent.hpp"
#include "../../physics/PhysicBody.hpp"
#include "../../core/CoreEventQueue.hpp"
#include "../../core/CoreEventListener.hpp"
#include "../../physics/CollisionEvent.hpp"
#include "../../physics/Force.hpp"
#include "../../physics/PhysicLayer.hpp"
#include "../../physics/Collider.hpp"
#include "../../core/GameObject.hpp"
#include <iostream>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_GAMEOBJECT_ROOT_INCLUDER
#endif



#include <vector>



using namespace nde;
using namespace std;



SUITE(GameObject)
{

class EventReporter : public CoreEventListener<CoreEvent>
{
public:
    EventReporter():
    CoreEventListener<CoreEvent>::CoreEventListener()
    {}
    virtual ~EventReporter(){}

    virtual void reaction(std::shared_ptr<CoreEvent> event)
    {
        m_timePoints.push_back(event->getTimePoint());
    }

public: 
    vector<timePoint_t> m_timePoints;
};

/*
class TestGameObject : public GameObject
{
public:
    TestGameObject():GameObject::GameObject()
    {}

    virtual ~TestGameObject(){}

};

*/

class TestGameObject : public GameObject
{

public:
    static std::shared_ptr<PhysicLayer> s_layer;
    static constexpr bool s_isLayerInit = false;
    static void initLayer()
    {
        if(!s_isLayerInit)
        {
            s_layer = std::make_shared<PhysicLayer>(true, true);
        }
    }
public:
    TestGameObject():
        GameObject::GameObject()
    {
        TestGameObject::initLayer();
        m_body = std::make_shared<DynamicPhysicBody>(
            TestGameObject::s_layer,
            1.0f);
        auto obb = new OrientedBoundingBox(1.0f, 1.0f, 1.0f);
        auto bodyPart = std::make_shared<BodyPart>(obb, m_body);
        bodyPartID_t id = m_body->addRootPart(bodyPart);
        m_body->setOwner(this->weak_from_this());
        if(m_body == nullptr)
        {
            std::cout << "body is null omg" << std::endl;
        }
        
    }

    virtual ~TestGameObject(){}
};

std::shared_ptr<PhysicLayer> TestGameObject::s_layer;
constexpr bool TestGameObject::s_isLayerInit;

TEST(Creation)
{
    auto go =  make_shared<GameObject>();
}

TEST(EventReaction)
{
    auto go = make_shared<GameObject>();
    auto reporter = std::make_shared<EventReporter>();
    go->addEventListener(reporter);

    go->onEvent(std::make_shared<CoreEvent>(1.4f));
    go->onEvent(std::make_shared<CoreEvent>(11.0f));
    go->onEvent(std::make_shared<CoreEvent>(3.0f));
    go->onEvent(std::make_shared<CoreEvent>(6.0f));
    go->onEvent(std::make_shared<CoreEvent>(1.0f));
    go->onEvent(std::make_shared<CoreEvent>(3.0f));


    CHECK_CLOSE(reporter->m_timePoints[0], 1.4f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[1], 11.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[2], 3.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[3], 6.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[4], 1.0f, 0.000001);
    CHECK_CLOSE(reporter->m_timePoints[5], 3.0f, 0.000001);
}




TEST(EventBouncing)
{
    auto go = make_shared<GameObject>();
    auto reporter = std::make_shared<EventReporter>();
    go->addEventListener(reporter);
    auto bouncer = std::make_shared<TargetedCoreEventBouncer>();
    auto event = std::make_shared<TargetedCoreEvent>(1.4f);
    event->addTarget(go);

    bouncer->reactTo(event);

    CHECK_CLOSE(reporter->m_timePoints[0], 1.4f, 0.000001);
}


TEST(EventBouncingWithInheritance)
{
     std::shared_ptr<GameObject> go = make_shared<TestGameObject>();
    auto reporter = std::make_shared<EventReporter>();
    go->addEventListener(reporter);
    auto bouncer = std::make_shared<TargetedCoreEventBouncer>();
    auto event = std::make_shared<TargetedCoreEvent>(1.8f);
    event->addTarget(go);

    bouncer->reactTo(event);

    CHECK_CLOSE(reporter->m_timePoints[0], 1.8f, 0.000001);
}

}



#ifdef TEST_GAMEOBJECT_ROOT_INCLUDER
int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
#endif