/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_GEOMETRY_ROOT_INCLUDER
#endif

#include "../../utils/geometry.hpp"
#include <glm/glm.hpp>

using namespace nde;

SUITE(Geometry){

TEST(proportion){
    glm::vec3 vec1(10.0f, 0.0f, 0.0f);
    glm::vec3 vec2(5.0f, 0.0f, 0.0f);
    
    CHECK_EQUAL(Geometry::proportion(vec1, vec2), 2.0f);
}

}


#ifdef TEST_GEOMETRY_ROOT_INCLUDER

int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}

#endif