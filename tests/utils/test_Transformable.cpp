/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_TRANSFORMABLE_ROOT_INCLUDER
#endif


#include "../../utils/geometry.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>



using namespace nde;
using namespace glm;

SUITE(Transformable){

TEST(base_transformations)
{
    Transformable t;
    CHECK(t.getPosition() == Transformable::DEFAULT_REFERENCE_FRAME_POSITION);
    CHECK(t.getOrientation() == Transformable::DEFAULT_REFERENCE_FRAME_ORIENTATION);
    CHECK(t.getRotCenter() == Transformable::DEFAULT_REFERENCE_FRAME_ROT_CENTER);


    vec3 trl(1.0, 0.0, 0.0); 
    t.translate(trl);
    CHECK(t.getPosition() == Transformable::DEFAULT_REFERENCE_FRAME_POSITION + trl );
    CHECK(t.getOrientation() == Transformable::DEFAULT_REFERENCE_FRAME_ORIENTATION);
    CHECK(t.getRotCenter() == Transformable::DEFAULT_REFERENCE_FRAME_ROT_CENTER + trl);
    
    quat rot(1.0, 1.0, 1.0, 0.5);
    t.rotate(rot);
    CHECK(t.getPosition() == Transformable::DEFAULT_REFERENCE_FRAME_POSITION + trl);
    CHECK(t.getOrientation() == Transformable::DEFAULT_REFERENCE_FRAME_ORIENTATION * rot);
    CHECK(t.getRotCenter() == Transformable::DEFAULT_REFERENCE_FRAME_ROT_CENTER + trl);

}
}

#ifdef TEST_TRANSFORMABLE_ROOT_INCLUDER

int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}

#endif