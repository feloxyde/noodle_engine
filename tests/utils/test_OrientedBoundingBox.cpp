/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_BOUNDINGBOX_ROOT_INCLUDER
#endif

#include "../../utils/geometry.hpp"
#include <glm/glm.hpp>
#include "../../utils/ndetypes.hpp"
#include <list>
#include <iostream>
#include "../../devtools.hpp"

using namespace nde;
using namespace glm;


SUITE(OrientedBoudingBox)
{

TEST(projection)
{
    OrientedBoundingBox obb(1.0, 3.0, 2.0);
    vec3 normal(0.0, 1.0, 0.0);      
    //lets first check the projection is done right with a non rotated OBB
    auto sp1 = obb.projection(normal);
    CHECK_CLOSE(sp1.begin(), -1.5, 0.00001);
    CHECK_CLOSE(sp1.end(), 1.5, 0.00001); 
    //now lets check with a rotated OBB.
    Transformable tbegin;
    Transformable tend;
    tbegin.setPosition(vec3(-0.5, -1.5, -1.0));
    tend.setPosition(vec3(0.5, 1.5, 1.0));
    vec3 rotaxis(0.0, 0.0, 1.0);
    angle_t angle = 29.0;

    obb.rotate(rotaxis, angle);
    tbegin.rotate(rotaxis, angle);
    tend.rotate(rotaxis, angle);

    auto sp2 = obb.projection(normal);
    CHECK_CLOSE(sp2.begin(), min(tbegin.getPosition().y, tend.getPosition().y), 0.00001);
    CHECK_CLOSE(sp2.end(), max(tbegin.getPosition().y, tend.getPosition().y), 0.00001);
    

}


TEST(normals)
{
    //manually check that normals are right set up
    //#FIXME please check all normals, here we only check for the normals count
    OrientedBoundingBox obb(2.0, 3.0, 4.0);
    auto normals = obb.normals();
    CHECK_EQUAL(normals.size(), 9); //OBB only has 9 normals since they are 18 paired 
    //checking every normals, 3*9 checks
    //1
    vec3 normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, 1.0, 0.000001);
    CHECK_CLOSE(normal.y, 0.0, 0.000001);
    CHECK_CLOSE(normal.z, 0.0, 0.000001);

    //2
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, 0.0, 0.000001);
    CHECK_CLOSE(normal.y, 1.0, 0.000001);
    CHECK_CLOSE(normal.z, 0.0, 0.000001);

    //3
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, 0.0, 0.000001);
    CHECK_CLOSE(normal.y, 0.0, 0.000001);
    CHECK_CLOSE(normal.z, 1.0, 0.000001);

    //4
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, 0.707107, 0.000001);
    CHECK_CLOSE(normal.y, 0.0, 0.000001);
    CHECK_CLOSE(normal.z, 0.707107, 0.000001);

    //5
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, -0.707107, 0.000001);
    CHECK_CLOSE(normal.y, 0.0, 0.000001);
    CHECK_CLOSE(normal.z, 0.707107, 0.000001);

    //6
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, 0.0, 0.000001);
    CHECK_CLOSE(normal.y, 0.707107, 0.000001);
    CHECK_CLOSE(normal.z, 0.707107, 0.000001);

    //7
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, 0.0, 0.000001);
    CHECK_CLOSE(normal.y, -0.707107, 0.000001);
    CHECK_CLOSE(normal.z, 0.707107, 0.000001);

    //8
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, 0.707107, 0.000001);
    CHECK_CLOSE(normal.y, 0.707107, 0.000001);
    CHECK_CLOSE(normal.z, 0.0, 0.000001);

    //9
    normal = normals.front();
    normals.pop_front();
    CHECK_CLOSE(normal.x, -0.707107, 0.000001);
    CHECK_CLOSE(normal.y, 0.707107, 0.000001);
    CHECK_CLOSE(normal.z, 0.0, 0.000001);
}



TEST(asAxisAlignedBoundingBox)
{
    OrientedBoundingBox obb(1.0, 3.0, 2.0);
    vec3 normal(0.0, 1.0, 0.0);      
    //we check that the AABB matches
    auto aabb1 = obb.asAxisAlignedBoundingBox();
    CHECK_CLOSE(aabb1.begin().x, -0.5, 0.0001);
    CHECK_CLOSE(aabb1.begin().y, -1.5, 0.0001);
    CHECK_CLOSE(aabb1.begin().z, -1.0, 0.0001);
    CHECK_CLOSE(aabb1.end().x, 0.5, 0.0001);
    CHECK_CLOSE(aabb1.end().y, 1.5, 0.0001);
    CHECK_CLOSE(aabb1.end().z, 1.0, 0.0001);
    
    //now lets check with a rotated OBB.
    Transformable tbegin;
    Transformable tend;
    tbegin.setPosition(vec3(-0.5, -1.5, -1.0));
    tend.setPosition(vec3(0.5, 1.5, 1.0));
    vec3 rotaxis(0.0, 0.0, 1.0);
    angle_t angle = 29.0;

    obb.rotate(rotaxis, angle);
    tbegin.rotate(rotaxis, angle);
    tend.rotate(rotaxis, angle);

    auto aabb2 = obb.asAxisAlignedBoundingBox();
    CHECK_CLOSE(aabb2.begin().y, min(tbegin.getPosition().y, tend.getPosition().y), 0.00001);
    CHECK_CLOSE(aabb2.end().y, max(tbegin.getPosition().y, tend.getPosition().y), 0.00001);
}

TEST(normalReturnCollision)
{
    std::cout << "begin test normal" << std::endl;
    OrientedBoundingBox obb1(1.0, 1.0, 1.0);
    obb1.translate(vec3(-1.5, 0.0, 0.0));
    obb1.rotate(vec3(0.0, 1.0, 0.0), 3.14159/4);
    OrientedBoundingBox obb2(1.0, 1.0, 1.0);
    obb2.translate(vec3(0.5, 0.0, 0.0));
    glm::vec3 move1(2.0, 0.0, 0.0);
    glm::vec3 move2(0.0, 0.0, 0.0);

    bool collision = false;
    collisionTime_t timeIn = 0;
    collisionTime_t timeOut = 0;
    vec3 normal(0.0);
    collision = BoundingBox::sweptCollisionTest(
                            obb1, move1, 
                            obb2, move2, 
                            timeIn, timeOut, normal);
    
    CHECK(collision);
    std::cout << "n1 = " << vec3ToString(normal) << std::endl;
    CHECK_CLOSE(timeIn, 0.396447, 0.00001);
    CHECK_CLOSE(timeOut, 1.60355, 0.00001);
    std::cout << "end test normal" << std::endl;
}

//there should be no need to check for collision usually,
//since it is supposed to be already working if normals
//and projection works. 
//since OBB collision detection tests are really hard to 
//implement (time consuming), Tests will be implemented later.

}


#ifdef TEST_BOUNDINGBOX_ROOT_INCLUDER
int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
#endif