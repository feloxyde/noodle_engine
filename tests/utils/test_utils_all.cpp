/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE
#define TEST_UTILS_ALL_INCLUDER_FILE
#endif

#include "test_AxisAlignedBoudingBox.cpp"
#include "test_Geometry.cpp"
#include "test_OrientedBoundingBox.cpp"
#include "test_SolidProjection.cpp"
#include "test_Transformable.cpp"

#ifdef TEST_UTILS_ALL_INCLUDER_FILE

int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}

#endif