/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE 
#define TEST_AXISALIGNEDBOUNDINGBOX_ROOT_INCLUDER
#endif

#include "../../utils/geometry.hpp"
#include <iostream>
#include <glm/glm.hpp>
#include "../../utils/ndetypes.hpp"
#include "../../devtools.hpp"
using namespace nde;
using namespace glm;

SUITE(AxisAlignedBoundingBox)
{

TEST(projection)
{
    vec3 begin(0.0, -3.0, 10.0);
    vec3 end(1.0, 20.0, 100.0);
    AxisAlignedBoundingBox aabb(begin, end);

    vec3 normal(1.0, 0.0, 0.0);

    auto proj = aabb.projection(normal);

    CHECK_CLOSE(proj.begin(), 0.0, 0.00001);
    CHECK_CLOSE(proj.end(), 1.0, 0.00001);
    CHECK_CLOSE(proj.normal().x, 1.0, 0.00001);
}
}

TEST(staticCollisionTestDetection)
{
    vec3 begin1(0.0, 0.0, 0.0);
    vec3 end1(1.0, 1.0, 1.0);
    vec3 begin2(0.6, 0.0, 0.0);
    vec3 end2(1.0, 1.0, 1.0);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);

    bool collision = false;
    vec3 extractor(0.0);
    collision = BoundingBox::staticCollisionTest(aabb1, aabb2, extractor);
    CHECK(collision);
    CHECK_CLOSE(extractor.x, -0.4, 0.00001);
}

TEST(staticCollisionTestRejection)
{
    vec3 begin1(0.0, 0.0, 0.0);
    vec3 end1(1.0, 1.0, 1.0);
    vec3 begin2(1.1, 0.0, 0.0);
    vec3 end2(2.0, 1.0, 1.0);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);

    bool collision = false;
    vec3 extractor(0.0);
    collision = BoundingBox::staticCollisionTest(aabb1, aabb2, extractor);
    CHECK(!collision);
}

TEST(sweptCollisionTestDetectionByFace)
{
    vec3 begin1(0.0, 0.0, 0.0);
    vec3 end1(1.0, 1.0, 1.0);
    vec3 begin2(2.0, 0.0, 0.0);
    vec3 end2(3.0, 1.0, 1.0);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);
    glm::vec3 move1(2.0, 0.0, 0.0);
    glm::vec3 move2(0.0, 0.0, 0.0);

    bool collision = false;
    collisionTime_t timeIn = 0;
    collisionTime_t timeOut = 0;
    vec3 normal(0.0);
    collision = BoundingBox::sweptCollisionTest(
                            aabb1, move1, 
                            aabb2, move2, 
                            timeIn, timeOut, normal);
    
    CHECK(collision);
    CHECK_CLOSE(timeIn, 0.5, 0.00001);
    CHECK_CLOSE(timeOut, 1.5, 0.00001);
}

TEST(sweptCollisionTestDetectionByEdge)
{
    vec3 begin1(0.0, 0.0, 0.0);
    vec3 end1(1.0, 1.0, 1.0);
    vec3 begin2(2.0, 2.0, 0.0);
    vec3 end2(3.0, 3.0, 1.0);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);
    glm::vec3 move1(2.0, 2.0, 0.0);
    glm::vec3 move2(0.0, 0.0, 0.0);

    bool collision = false;
    collisionTime_t timeIn = 0;
    collisionTime_t timeOut = 0;
    vec3 normal(0.0);
    collision = BoundingBox::sweptCollisionTest(
                            aabb1, move1, 
                            aabb2, move2, 
                            timeIn, timeOut, normal);
    
    CHECK(collision);
    CHECK_CLOSE(timeIn, 0.5, 0.00001);
    CHECK_CLOSE(timeOut, 1.5, 0.00001);
}

TEST(sweptCollisionTestRejectionNegativeMove)
{
    vec3 begin1(0.0, 0.0, 0.0);
    vec3 end1(1.0, 1.0, 1.0);
    vec3 begin2(2.0, 2.0, 0.0);
    vec3 end2(3.0, 3.0, 1.0);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);
    glm::vec3 move1(-2.0, -2.0, 0.0);
    glm::vec3 move2(0.0, 0.0, 0.0);

    bool collision = false;
    collisionTime_t timeIn = 0;
    collisionTime_t timeOut = 0;
    vec3 normal(0.0);
    collision = BoundingBox::sweptCollisionTest(
                            aabb1, move1, 
                            aabb2, move2, 
                            timeIn, timeOut, normal);
    
    CHECK(!collision);
}

TEST(sweptCollisionTestRejectionDodge)
{
    //the two objects crosses path but never encounter
    vec3 begin1(0.0, 0.0, 0.0);
    vec3 end1(1.0, 1.0, 1.0);
    vec3 begin2(2.0, 2.0, 0.0);
    vec3 end2(3.0, 3.0, 1.0);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);
    glm::vec3 move1(1.0, 0.0, 0.0);
    glm::vec3 move2(0.0, -10.0, 0.0);

    bool collision = false;
    collisionTime_t timeIn = 0;
    collisionTime_t timeOut = 0;
    vec3 normal(0.0);
    collision = BoundingBox::sweptCollisionTest(
                            aabb1, move1, 
                            aabb2, move2, 
                            timeIn, timeOut, normal);
    
    CHECK(!collision);
}

TEST(mergeExternal)
{
    //four cases : External, secant, included, mixed
    vec3 begin1(0.0, -0.1, -0.2);
    vec3 end1(1.0, 1.1, 1.2);
    vec3 begin2(2.0, 2.1, 2.3);
    vec3 end2(3.4, 3.5, 3.6);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);

    auto aabbm = AxisAlignedBoundingBox::merge(aabb1, aabb2);
    vec3 mBegin = aabbm.begin();
    vec3 mEnd = aabbm.end();
    CHECK_CLOSE(mBegin.x, begin1.x, 0.000001);
    CHECK_CLOSE(mBegin.y, begin1.y, 0.000001);
    CHECK_CLOSE(mBegin.z, begin1.z, 0.000001);
    CHECK_CLOSE(mEnd.x, end2.x, 0.000001);
    CHECK_CLOSE(mEnd.y, end2.y, 0.000001);
    CHECK_CLOSE(mEnd.z, end2.z, 0.000001);
}

TEST(mergeSecant)
{
    vec3 begin1(0.0, -0.1, -0.2);
    vec3 end1(1.0, 1.1, 1.2);
    vec3 begin2(0.5, 0.6, 0.7);
    vec3 end2(1.8, 1.9, 1.95);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);

    auto aabbm = AxisAlignedBoundingBox::merge(aabb1, aabb2);
    vec3 mBegin = aabbm.begin();
    vec3 mEnd = aabbm.end();
    CHECK_CLOSE(mBegin.x, begin1.x, 0.000001);
    CHECK_CLOSE(mBegin.y, begin1.y, 0.000001);
    CHECK_CLOSE(mBegin.z, begin1.z, 0.000001);
    CHECK_CLOSE(mEnd.x, end2.x, 0.000001);
    CHECK_CLOSE(mEnd.y, end2.y, 0.000001);
    CHECK_CLOSE(mEnd.z, end2.z, 0.000001);
}

TEST(mergeIncluded)
{
    vec3 begin1(0.0, -0.1, -0.2);
    vec3 end1(1.0, 1.1, 1.2);
    vec3 begin2(0.21, 0.22, 0.23);
    vec3 end2(0.44, 0.45, 0.46);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);

    auto aabbm = AxisAlignedBoundingBox::merge(aabb1, aabb2);
    vec3 mBegin = aabbm.begin();
    vec3 mEnd = aabbm.end();

    CHECK_CLOSE(mBegin.x, begin1.x, 0.000001);
    CHECK_CLOSE(mBegin.y, begin1.y, 0.000001);
    CHECK_CLOSE(mBegin.z, begin1.z, 0.000001);
    CHECK_CLOSE(mEnd.x, end1.x, 0.000001);
    CHECK_CLOSE(mEnd.y, end1.y, 0.000001);
    CHECK_CLOSE(mEnd.z, end1.z, 0.000001);
}

TEST(mergeMixed)
{
    vec3 begin1(0.0, -0.1, -0.2);
    vec3 end1(1.0, 1.1, 1.2);
    vec3 begin2(2.0, 0.6, 0.23);
    vec3 end2(3.4, 1.9, 0.46);

    AxisAlignedBoundingBox aabb1(begin1, end1);
    AxisAlignedBoundingBox aabb2(begin2, end2);

    auto aabbm = AxisAlignedBoundingBox::merge(aabb1, aabb2);
    vec3 mBegin = aabbm.begin();
    vec3 mEnd = aabbm.end();

    CHECK_CLOSE(mBegin.x, begin1.x, 0.000001);
    CHECK_CLOSE(mBegin.y, begin1.y, 0.000001);
    CHECK_CLOSE(mBegin.z, begin1.z, 0.000001);
    CHECK_CLOSE(mEnd.x, end2.x, 0.000001);
    CHECK_CLOSE(mEnd.y, end2.y, 0.000001);
    CHECK_CLOSE(mEnd.z, end1.z, 0.000001);
}

#ifdef TEST_AXISALIGNEDBOUNDINGBOX_ROOT_INCLUDER
int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}
#endif