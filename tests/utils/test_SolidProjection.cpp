/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <UnitTest++/UnitTest++.h>
#ifndef TEST_ROOT_INCLUDER_FILE
#define TEST_ROOT_INCLUDER_FILE
#define TEST_SOLIDPROJECTION_ROOT_INCLUDER
#endif


#include "../../utils/geometry.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>
#include "../../utils/ndetypes.hpp"

using namespace nde;
using namespace glm;

#define FLOAT_PRECISION 0.0000001

SUITE(SolidProjection){

TEST(staticOverlapTestDetection)
{
    vec3 normal(0.0, 1.0, 0.0);
    //four cases of overlaping : left, right, both, contained

    SolidProjection sp0(0.0, 1.0, normal);
    SolidProjection spright(0.6, 1.1, normal);
    SolidProjection spleft(-1.0, 0.4, normal);
    SolidProjection spboth(-1.0, 1.1, normal);
    SolidProjection spcont(0.2, 0.5, normal);

    vec3 extractor(0.0, 0.0, 0.0);
    bool overlap = false;

    overlap = SolidProjection::staticOverlapTest(sp0, spright, extractor);
    CHECK(overlap);
    CHECK_CLOSE(extractor.y, (-normal.y*0.4f), FLOAT_PRECISION);
    overlap = SolidProjection::staticOverlapTest(sp0, spleft, extractor);
    CHECK(overlap);
    CHECK_CLOSE(extractor.y, normal.y*0.4f, FLOAT_PRECISION);
    overlap = SolidProjection::staticOverlapTest(sp0, spboth, extractor);
    CHECK(overlap);
    CHECK_CLOSE(extractor.y, normal.y*1.1f, FLOAT_PRECISION);
    overlap = SolidProjection::staticOverlapTest(sp0, spcont, extractor);
    CHECK(overlap);
    CHECK_CLOSE(extractor.y, normal.y*0.5f, FLOAT_PRECISION);
    
}

TEST(staticOverlapTestRejection)
{
    vec3 normal(0.0, 1.0, 0.0);
    //two case of not overlaping : left and right

    SolidProjection sp0(0.0, 1.0, normal);
    SolidProjection spright(1.1, 1.5, normal);
    SolidProjection spleft(-1.0, -0.4, normal);

    vec3 extractor(0.0, 0.0, 0.0);
    bool overlap = true;

    overlap = SolidProjection::staticOverlapTest(sp0, spright, extractor);
    CHECK(!overlap);
    overlap = SolidProjection::staticOverlapTest(sp0, spleft, extractor);
    CHECK(!overlap);
}

TEST(sweptOverlapTestDetection)
{

    vec3 normal(0.0, 1.0, 0.0);
    //two cases : reach by left and reach by righ
    SolidProjection sp0(0.0, 1.0, normal);
    SolidProjection spright(1.1, 2.1, normal);
    SolidProjection spleft(-3.0, -2.0, normal);
    
    collisionTime_t timeIn = 0.0;
    collisionTime_t timeOut = 0.0;
    vec3 colNormal(0.0);
    bool overlap = false;

    overlap = SolidProjection::sweptOverlapTest(sp0, spright, -0.2, timeIn, timeOut, colNormal);
    CHECK(overlap);
    CHECK_CLOSE(timeIn, 0.5, 0.00001);
    CHECK_CLOSE(timeOut, 10.5, 0.00001);
    CHECK_CLOSE(colNormal.y, normal.y, FLOAT_PRECISION);

    overlap = SolidProjection::sweptOverlapTest(sp0, spleft, 8.0, timeIn, timeOut, colNormal);
    CHECK(overlap);
    CHECK_CLOSE(timeIn, 0.25, 0.00001);
    CHECK_CLOSE(timeOut, 0.5, 0.00001);
    CHECK_CLOSE(colNormal.y, normal.y, FLOAT_PRECISION);
}

TEST(sweptOverlapTestRejection)
{
     vec3 normal(0.0, 1.0, 0.0);
    //four cases : left and right too slow, left and right going away
    SolidProjection sp0(0.0, 1.0, normal);
    SolidProjection spright(1.1, 2.1, normal);
    SolidProjection spleft(-3.0, -2.0, normal);
    
    collisionTime_t timeIn = 1.0;
    collisionTime_t timeOut = 1.0;
    vec3 colNormal(0.0);
    bool overlap = false;

    //right away
    overlap = SolidProjection::sweptOverlapTest(sp0, spright, 0.3, timeIn, timeOut, colNormal);
    CHECK(!overlap);
    
    //left away
    overlap = SolidProjection::sweptOverlapTest(sp0, spleft, -3.0, timeIn, timeOut, colNormal);
    CHECK(!overlap);

    //right too slow
    overlap = SolidProjection::sweptOverlapTest(sp0, spright, -0.05, timeIn, timeOut, colNormal);
    CHECK(!overlap);
    
    //left too slow
    overlap = SolidProjection::sweptOverlapTest(sp0, spleft, 1.0, timeIn, timeOut, colNormal);
    CHECK(!overlap);

}

}

#ifdef TEST_SOLIDPROJECTION_ROOT_INCLUDER

int main(int, const char *[])
{
   return UnitTest::RunAllTests();
}

#endif