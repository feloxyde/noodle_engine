/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_GEOMETRY_H
#define NDE_GEOMETRY_H
#include <glm/glm.hpp>
//#include <glm/gtx/rotate_vector.hpp>
//#include <glm/gtx/vector_angle.hpp>
//#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
//#include <glm/gtx/transform.hpp>
#include "ndetypes.hpp"
#include <list>

#include <vector>
#include <memory>

namespace nde {

class Geometry {

public:
  /** returns the transfer rotation to orient form source to destination
  this function can be considered as "diff"*/
  static glm::quat reorient(glm::quat source, glm::quat destination);
  /** gives proportion of v in col, v is colinear to col */
  static length_t proportion(glm::vec3 v, glm::vec3 col);
  /** gives the reflexion of the vector v alongside vector normal */
  static glm::vec3 reflection(glm::vec3 v, glm::vec3 normal);
  /** gives the part of v that is colinear to normal */
  static glm::vec3 colinearPart(glm::vec3 v, glm::vec3 normal);
  /** makes a referential change over the quat orientation. */
  static glm::vec3 forwardAlong(glm::vec3 v, glm::quat orient);
  /** makes a referential change of v toward orient using up as up vector */
  static glm::vec3 forwardAlong(glm::vec3 v, glm::vec3 orient, glm::vec3 up);
};

class Transformable : public std::enable_shared_from_this<Transformable> {

public:

  static const glm::vec3 DEFAULT_REFERENCE_FRAME_POSITION;
  static const glm::quat DEFAULT_REFERENCE_FRAME_ORIENTATION;
  static const glm::vec3 DEFAULT_REFERENCE_FRAME_ROT_CENTER;

public:

  /** Builds a default reference frame Transformable */
  Transformable();
  /** Builds a custom reference frame Transformable */
  Transformable(std::weak_ptr<Transformable> referenceFrame);
  
  /** copy constructor */
  Transformable(Transformable const& origin);

  virtual ~Transformable();

  void setReferenceFrame(std::weak_ptr<Transformable> referenceFrame);
  void setReferenceFrameKeepRelative(std::weak_ptr<Transformable> referenceFrame);


  /** make selfAnchor (given in "this" reference frame) and refAnchor (given in referenceFrame reference frame)
  *  correspond in real referencial by a translation, and then set it to the rotCenter of this.
  *  referenceFrame becomes the new Reference frame of this
  */
  void attachToReferenceFrame(glm::vec3 const& selfAnchor, std::weak_ptr<Transformable> referenceFrame, glm::vec3 const& refAnchor);

  /** make selfAnchor (given in "this" referenceFrame) and refAnchor (given in referenceFrame referenceFrame)
  *   correspond in real referencial by a translation, and then set it to the rotCenter of this.
  *   if no reference frame is set, it will take default one.
  */
  void attachToReferenceFrame(glm::vec3 const& selfAnchor, glm::vec3 const& refAnchor);

  virtual glm::vec3 getPosition() const;
  virtual glm::quat getOrientation() const;
  virtual glm::vec3 getRotCenter() const;
  virtual std::weak_ptr<Transformable> getReferenceFrame() const;
  /* raw change of position */
  virtual void setPosition(glm::vec3 const& position);
  /** raw change of orientation */
  virtual void setOrientation(glm::quat const& orientation);
  /** raw change of rotcenter */
  virtual void setRotCenter(glm::vec3 const& rotCenter);

  /** returns relative position (in reference frame) */
  glm::vec3 getRelativePosition();
  /** returns relative orientation (in reference frame) */
  glm::quat getRelativeOrientation();
  /** relative raw< change of position */
  void setRelativePosition(glm::vec3 const& position);
  /** relative raw change of orientation */
  void setRelativeOrientation(glm::quat const& orientation);

  /** changes position while keeping others attributes */
  virtual void reposition(glm::vec3 const& position);
  /** changes orientation while keeping others attributes coherent with it */
  virtual void reorient(glm::quat  const& orientation);

  /** reposition the transformable in its reference frame */
  virtual void relativeReposition(glm::vec3 const& position);
  /** reposition the transformable in its reference frame */
  virtual void relativeReorient(glm::quat const& orientation);




  void transform(glm::mat4 const& transformation);
  void translate(glm::vec3 const& translation);

  void rotate(glm::vec3 const& axis, angle_t angle);
  void rotate(glm::vec3 const& axis, angle_t angle, glm::vec3 const& rotCenter);
  void rotate(glm::quat const& rotation);
  void rotate(glm::quat const& rotation, glm::vec3 const& rotCenter);

  /** returns the transformation matrix for an object in world to have coordinates in 'this' referential */
  virtual glm::mat4 asReferenceFrameTransformation();
  /** returns the transformation matrix for an object in 'this' referential o have coordinates in world */
  virtual glm::mat4 asReferenceFrameTransformationBack();

  /** returns a copy of this */
//  virtual Transformable& copy();

protected:
  /** transforms the body of the transformable if he has one */
  virtual void bodyTransform(glm::mat4 const& transformation);
  /** translate the body of the transformable if he has one */
  virtual void bodyTranslate(glm::vec3 const& translation);

private:

  std::weak_ptr<Transformable> m_referenceFrame; //used to get/set relatives orientations
  glm::vec3 m_position; //absolute position
  glm::quat m_orientation; //absolute orientation
  glm::vec3 m_rotCenter; //rotation center
};

class SolidProjection {

public:
  static bool staticOverlapTest(SolidProjection const& proj1, SolidProjection const& proj2, glm::vec3& extractor);

  static bool sweptOverlapTest(SolidProjection const& proj1, SolidProjection const& proj2, length_t relMove2,
                               collisionTime_t& timeIn, collisionTime_t& timeOut,
                               glm::vec3& normal1);



public:
  /** this class is a projection alongside an axis 
  */
  SolidProjection(length_t begin, length_t end, glm::vec3 normal);


  length_t begin() const;
  length_t end() const;
  glm::vec3 const& normal() const;

private:
//  glm::vec3 m_begin;colTime_t
//  glm::vec3 m_end;
  length_t m_begin;
  length_t m_end;
  glm::vec3 m_normal;
};

class AxisAlignedBoundingBox; //forward Declaration of AABB

class BoundingBox : public Transformable
{

public:
  /** if box1 overlaps with box2, returns true, and extractor is value of translation
  * to extract box1 from box2
  */
  static bool staticCollisionTest(BoundingBox const& box1, BoundingBox const& box2, glm::vec3& extractor);

  /** computes a sewpt collision test */
  static bool sweptCollisionTest(BoundingBox& box1, glm::vec3& move1,
                             BoundingBox& box2, glm::vec3& move2,
                             collisionTime_t& timeIn, collisionTime_t& timeOut,
                             glm::vec3& normalIn);
public:

  /** builds a default bounding box */
  BoundingBox();

  BoundingBox(std::weak_ptr<Transformable> referenceFrame);

  virtual ~BoundingBox();

  /** returns all normals of the box in order to detect collisions */
  virtual std::list<glm::vec3> const& normals() const = 0;

  /** returns the projection object of the Box by a normal vector 
  !!! DOES NOT WORKS IF THE NORMAL IS NOT AN AXIS !!! NEED A FIX !  
  */
  virtual SolidProjection projection(glm::vec3 normal) const = 0;

  /** returns the slightest AABB englobing the box */
  virtual AxisAlignedBoundingBox asAxisAlignedBoundingBox() = 0;

};

class AxisAlignedBoundingBox : public BoundingBox
{

public:
  /** returns the slightest AABB containing these two AABB */
  static AxisAlignedBoundingBox merge(AxisAlignedBoundingBox const& box1, AxisAlignedBoundingBox const& box2);

private:
  class NormalList{
    public:
      NormalList();
      std::list<glm::vec3> list; //the normal list, since it is always the same. DO NOT CHANGE IT !
  };

  static NormalList NORMAL_LIST;

public:
  /** creates an AABB */
  AxisAlignedBoundingBox(glm::vec3 const& begin, glm::vec3 const& end);

  virtual ~AxisAlignedBoundingBox();

  /** returns all normals of the box in order to detect collisions */
  virtual std::list<glm::vec3> const& normals() const;

  /** returns the projection object if the Box by a normal vector */
  virtual SolidProjection projection(glm::vec3 normal) const;

  glm::vec3 const& begin() const;

  glm::vec3 const& end() const;

  glm::vec3 const center() const;

  /** returns the slightest AABB englobing the box */
  virtual AxisAlignedBoundingBox asAxisAlignedBoundingBox();

protected:

  /** transforms the body of the transformable if he has one */
  virtual void bodyTransform(glm::mat4 const& ransformation);
  /** translate the body of the transformable if he has one */
  virtual void bodyTranslate(glm::vec3 const& translation);

private:

  glm::vec3 m_begin; //origin of the box
  glm::vec3 m_end; //end of the box

};


class OrientedBoundingBox : public BoundingBox
{
public:

  /** creates a Oriented Bounding box as an AABB with each of its halflengths over axis
  * witdh = x , height = y, depth = z */
   OrientedBoundingBox(length_t width, length_t height, length_t depth);

   virtual ~OrientedBoundingBox();

   /** returns all normals of the box in order to detect collisions */
   virtual std::list<glm::vec3> const& normals() const;

   /** returns the projection object of the Box by a normal vector */
   virtual SolidProjection projection(glm::vec3 normal) const;

   /** returns the slightest AABB englobing the box */
   virtual AxisAlignedBoundingBox asAxisAlignedBoundingBox();

protected:
   /** transforms the body of the transformable if he has one */
   virtual void bodyTransform(glm::mat4 const& ransformation);
   /** translate the body of the transformable if he has one */
   virtual void bodyTranslate(glm::vec3 const& translation);



private:
    std::vector<glm::vec3> m_body;
    std::list<glm::vec3> m_normals;
};


}


#endif //GEOMETRY_H
