/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bmp.hpp"
#include <fstream>
#include <iostream>
//loader inspired from opengl-tutorial.org

namespace nde {

BMPImage::BMPImage(const char* const buffer, const size_t width, const size_t height):
m_buffer(buffer),
m_width(width),
m_height(height),
m_bufferSize(width*height*3)
{
}
    
BMPImage::~BMPImage()
{
    if(m_buffer != nullptr)
        delete m_buffer;
}

std::unique_ptr<BMPImage> loadBMPImage (std::string filename)
{
    char header[54];
    unsigned int bufferPos;    
    unsigned int width, height;
    unsigned int bufferSize;   // = width*height*3
    //opening stream of the file
    std::ifstream inputFile (filename, std::ifstream::binary);

    inputFile.read(header, 54);


    if(inputFile.gcount() != 54) {
        std::cout << "Impossible to load bmp image " << filename << " : impossible to read header" << std::endl;
        return nullptr;
    }

    if ( header[0]!='B' || header[1]!='M' ){
       std::cout << "Impossible to load bmp image " << filename << " : wrong header start characters" << std::endl;
        return nullptr;
    }

    bufferPos = *(int*)&(header[0x0A]);
    bufferSize = *(int*)&(header[0x22]);
    width = *(int*)&(header[0x12]);
    height = *(int*)&(header[0x16]);

    if (bufferSize==0)    bufferSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
    if (bufferPos==0)      bufferPos=54; // The BMP header is done that way

    // Create a buffer
    char* buffer = new char [bufferSize];
    
    inputFile.seekg(bufferPos);

    // Read the actual data from the file into the buffer
    inputFile.read(buffer, bufferSize);


    inputFile.close();

    return std::make_unique<BMPImage>(buffer, width, height);
}

}