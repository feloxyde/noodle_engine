/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ndetypes.hpp"
#include "../physics/Body.hpp"
#include "geometry.hpp"
#include "../physics/Animation.hpp"
#include <memory>

namespace nde {

class Human {

public:

  //main part
  static bodyPartID_t thoraxID;

  static bodyPartID_t headID;

  //left
  static bodyPartID_t leftLegID;
  static bodyPartID_t leftShinID;
  static bodyPartID_t leftArmID;
  static bodyPartID_t leftForearmID;
  static bodyPartID_t leftHandID;
  static bodyPartID_t leftFootID;

  //right
  static bodyPartID_t rightLegID;
  static bodyPartID_t rightShinID;
  static bodyPartID_t rightArmID;
  static bodyPartID_t rightForearmID;
  static bodyPartID_t rightHandID;
  static bodyPartID_t rightFootID;

  //measurments
  static length_t headWidth;
  static length_t headHeight;
  static length_t headDepth;

  static length_t thoraxWidth;
  static length_t thoraxHeight;
  static length_t thoraxDepth;

  static length_t legWidth;
  static length_t legHeight;
  static length_t legDepth;

  static length_t shinWidth;
  static length_t shinHeight;
  static length_t shinDepth;

  static length_t footWidth;
  static length_t footHeight;
  static length_t footDepth;

  static length_t armWidth;
  static length_t armHeight;
  static length_t armDepth;

  static length_t forearmWidth;
  static length_t forearmHeight;
  static length_t forearmDepth;

  static length_t handWidth;
  static length_t handHeight;
  static length_t handDepth;

  static length_t jointOffset;


public:

  /** Creates an human body
  *
  * @return the skeleton of an human
  */
  static Body* body();

  /** Creates a walking animation
  *
  * @return a walking animation
  */
  static Animation* walk();

};

}
