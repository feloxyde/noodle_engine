/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Human.hpp"

namespace nde {

/* ##################################################################### */
/* ########## HUMAN CLASS IMPLEMENTATION ############################### */
/* ##################################################################### */


/* PUBLIC STATIC METHODS */
Body* Human::body()
{
  /* legs */
  std::weak_ptr<Transformable> nullweak;
  /* left foot */
  auto leftFoot = std::make_shared<BodyPart>(new OrientedBoundingBox(footWidth, footHeight, footDepth), nullweak);
//  leftFoot->translate(glm::vec3(0.0, -((shinHeight + footHeight)/2 + jointOffset), legWidth/2));
  //leftFoot->setRotCenter(glm::vec3(0.0, -0.6, 0.0));
  /* left shin */
  std::vector<std::shared_ptr<BodyPart>> subsLeftShin;
  subsLeftShin.push_back(leftFoot);

  auto leftShin = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(shinWidth, shinHeight, shinWidth), nullweak);
  leftShin->setExtensions(subsLeftShin);
  /* left leg */
  std::vector<std::shared_ptr<BodyPart>> subsLeftLeg;
  subsLeftLeg.push_back(leftShin);

  auto leftLeg = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(legWidth, legHeight, legDepth), nullweak);
  leftLeg->setExtensions(subsLeftLeg);
  /* right foot */
  auto rightFoot = std::make_shared<BodyPart>(new OrientedBoundingBox(footWidth, footHeight, footDepth), nullweak);

  /* right shin */
  std::vector<std::shared_ptr<BodyPart>> subsRightShin;
  subsRightShin.push_back(rightFoot);

  auto rightShin = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(shinWidth, shinHeight, shinWidth), nullweak);
  rightShin->setExtensions(subsRightShin);
  /* right leg */
  std::vector<std::shared_ptr<BodyPart>> subsRightLeg;
  subsRightLeg.push_back(rightShin);

  auto rightLeg = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(legWidth, legHeight, legDepth), nullweak);
  rightLeg->setExtensions(subsRightLeg);
 
  /* arms */

  /* right hand */
  auto rightHand = std::make_shared<BodyPart>(new OrientedBoundingBox(handWidth, handHeight, handDepth), nullweak);
  /* right forearm */
  std::vector<std::shared_ptr<BodyPart>> subsRightForearm;
  subsRightForearm.push_back(rightHand);

  auto rightForearm = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(forearmWidth, forearmHeight, forearmDepth), nullweak);
  rightForearm->setExtensions(subsRightForearm);
  /* right arm */
  std::vector<std::shared_ptr<BodyPart>> subsRightArm;
  subsRightArm.push_back(rightForearm);

  auto rightArm = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(armWidth, armHeight, armDepth), nullweak);
  rightArm->setExtensions(subsRightArm);

  /* left hand */
  auto leftHand = std::make_shared<BodyPart>(new OrientedBoundingBox(handWidth, handHeight, handDepth), nullweak);

  /* left forearm */
  std::vector<std::shared_ptr<BodyPart>> subsLeftForearm;
  subsLeftForearm.push_back(leftHand);

  auto leftForearm = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(forearmWidth, forearmHeight, forearmDepth), nullweak);
  leftForearm->setExtensions(subsLeftForearm);
  /* left arm */
  std::vector<std::shared_ptr<BodyPart>> subsLeftArm;
  subsLeftArm.push_back(leftForearm);

  auto leftArm = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(armWidth, armHeight, armDepth), nullweak);
  leftArm->setExtensions(subsLeftArm);

  /* principal */

  /* head */
  auto head = std::make_shared<BodyPart>(new OrientedBoundingBox(headWidth, headHeight, headDepth), nullweak);

  /* thorax */
  std::vector<std::shared_ptr<BodyPart>> subsThorax;
  subsThorax.push_back(head);
  subsThorax.push_back(leftArm);
  subsThorax.push_back(rightArm);
  subsThorax.push_back(rightLeg);
  subsThorax.push_back(leftLeg);

  auto thorax = std::make_shared<ExtendedBodyPart>(new OrientedBoundingBox(Human::thoraxWidth, Human::thoraxHeight, Human::thoraxDepth), nullweak);
  thorax->setExtensions(subsThorax);


  leftFoot->attachToReferenceFrame(
    glm::vec3(0.0f, (footHeight+jointOffset)/2, -shinDepth/2),
    glm::vec3(0.0f, -(shinHeight+jointOffset)/2, 0.0f));
  leftShin->attachToReferenceFrame(
    glm::vec3(0.0f, (shinHeight+jointOffset)/2, 0.0f),
    glm::vec3(0.0f, -(legHeight+jointOffset)/2, 0.0f));
  leftLeg->attachToReferenceFrame(
    glm::vec3(0.0f, (legHeight+jointOffset)/2, 0.0f),
    glm::vec3((thoraxWidth - legWidth)/2,-(thoraxHeight+jointOffset)/2, 0.0f));

  rightFoot->attachToReferenceFrame(
    glm::vec3(0.0f, (footHeight+jointOffset)/2, -shinDepth/2),
    glm::vec3(0.0f, -(shinHeight+jointOffset)/2, 0.0f));
  rightShin->attachToReferenceFrame(
    glm::vec3(0.0f, (shinHeight+jointOffset)/2, 0.0f),
    glm::vec3(0.0f, -(legHeight+jointOffset)/2, 0.0f));
  rightLeg->attachToReferenceFrame(
    glm::vec3(0.0f, (legHeight+jointOffset)/2, 0.0f),
    glm::vec3(-(thoraxWidth - legWidth)/2,-(thoraxHeight+jointOffset)/2, 0.0f));



  leftHand->attachToReferenceFrame(
    glm::vec3(0.0f, (handHeight+jointOffset)/2, 0.0f),
    glm::vec3(0.0f, -(forearmHeight+jointOffset)/2, 0.0f));
  leftForearm->attachToReferenceFrame(
    glm::vec3(0.0f, (forearmHeight+jointOffset)/2, 0.0f),
    glm::vec3(0.0f, -(forearmHeight+jointOffset)/2, 0.0f));
  leftArm->attachToReferenceFrame(
    glm::vec3(-(armWidth+jointOffset)/2, (armHeight-jointOffset)/2, 0.0f),
    glm::vec3((thoraxWidth+jointOffset)/2, (thoraxHeight-jointOffset)/2, 0.0f));

  rightHand->attachToReferenceFrame(
    glm::vec3(0.0f, (handHeight+jointOffset)/2, 0.0f),
    glm::vec3(0.0f, -(forearmHeight+jointOffset)/2, 0.0f));
  rightForearm->attachToReferenceFrame(
    glm::vec3(0.0f, (forearmHeight+jointOffset)/2, 0.0f),
    glm::vec3(0.0f, -(forearmHeight+jointOffset)/2, 0.0f));
  rightArm->attachToReferenceFrame(
    glm::vec3((armWidth+jointOffset)/2, (armHeight-jointOffset)/2, 0.0f),
    glm::vec3(-(thoraxWidth+jointOffset)/2, (thoraxHeight-jointOffset)/2, 0.0f));

  head->attachToReferenceFrame(
    glm::vec3(0.0f, -(headHeight+jointOffset)/2, 0.0f),
    glm::vec3(0.0f, (thoraxHeight+jointOffset)/2, 0.0f));


  Body* body = new Body();
  Human::thoraxID = body->addRootPart(thorax);
  Human::headID = body->addPart(head);

  Human::leftArmID = body->addPart(leftArm);
  Human::leftForearmID = body->addPart(leftForearm);
  Human::leftHandID = body->addPart(leftHand);

  Human::rightArmID = body->addPart(rightArm);
  Human::rightForearmID = body->addPart(rightForearm);
  Human::rightHandID = body->addPart(rightHand);

  Human::rightLegID = body->addPart(rightLeg);
  Human::rightShinID = body->addPart(rightShin);
  Human::rightFootID = body->addPart(rightFoot);

  Human::leftLegID = body->addPart(leftLeg);
  Human::leftShinID = body->addPart(leftShin);
  Human::leftFootID = body->addPart(leftFoot);


  return body;

}


Animation* Human::walk()
{
  Animation* anim = new Animation();

//frame 1
  KeyFrame* frame1 = new KeyFrame();
  Pose* leftLeg1 = new RelativeOrientation(leftLegID, glm::angleAxis(0.1f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightLeg1 = new RelativeOrientation(rightLegID, glm::angleAxis(-0.6f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightShin1 = new RelativeOrientation(rightShinID, glm::angleAxis(0.6f, glm::vec3(1.0f, 0.0f, 0.0f)));
  frame1->addPose(leftLeg1);
  frame1->addPose(rightLeg1);
  frame1->addPose(rightShin1);
  anim->addFrame(frame1, 1, 0.5f);

//frame 2
  KeyFrame* frame2 = new KeyFrame();
  Pose* leftLeg2 = new RelativeOrientation(leftLegID, glm::angleAxis(0.3f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* leftShin2 = new RelativeOrientation(leftShinID, glm::angleAxis(0.25f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightLeg2 = new RelativeOrientation(rightLegID, glm::angleAxis(-0.5f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightShin2 = new RelativeOrientation(rightShinID, glm::angleAxis(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)));
  frame2->addPose(leftLeg2);
  frame2->addPose(leftShin2);
  frame2->addPose(rightLeg2);
  frame2->addPose(rightShin2);
  anim->addFrame(frame2, 2, 0.5f);

  //frame 3
  KeyFrame* frame3 = new KeyFrame();
  Pose* leftLeg3 = new RelativeOrientation(leftLegID, glm::angleAxis(-0.2f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* leftShin3 = new RelativeOrientation(leftShinID, glm::angleAxis(0.8f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightLeg3 = new RelativeOrientation(rightLegID, glm::angleAxis(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)));
//  Pose* rightShin2 = new RelativeOrientation(rightShinID, glm::angleAxis(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)));
  frame3->addPose(leftLeg3);
  frame3->addPose(leftShin3);
  frame3->addPose(rightLeg3);

//  frame2->addPose(rightShin2);
  anim->addFrame(frame3, 3, 0.5f);

  KeyFrame* frame4 = new KeyFrame();
//  Pose* leftLeg4 = new RelativeOrientation(leftLegID, glm::angleAxis(-0.6f, glm::vec3(1.0f, 0.0f, 0.0f)));
//  Pose* leftShin4 = new RelativeOrientation(leftShinID, glm::angleAxis(0.6f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* leftLeg4 = new RelativeOrientation(leftLegID, glm::angleAxis(-0.5f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* leftShin4 = new RelativeOrientation(leftShinID, glm::angleAxis(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightLeg4 = new RelativeOrientation(rightLegID, glm::angleAxis(0.3f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightShin4 = new RelativeOrientation(rightShinID, glm::angleAxis(0.25f, glm::vec3(1.0f, 0.0f, 0.0f)));
  frame4->addPose(leftLeg4);
  frame4->addPose(leftShin4);
  frame4->addPose(rightLeg4);
  frame4->addPose(rightShin4);
  anim->addFrame(frame4, 4, 0.5f);


  //frame 5
  KeyFrame* frame5 = new KeyFrame();
  Pose* rightLeg5 = new RelativeOrientation(rightLegID, glm::angleAxis(-0.2f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* rightShin5 = new RelativeOrientation(rightShinID, glm::angleAxis(0.8f, glm::vec3(1.0f, 0.0f, 0.0f)));
  Pose* leftLeg5 = new RelativeOrientation(leftLegID, glm::angleAxis(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)));
//  Pose* rightShin2 = new RelativeOrientation(rightShinID, glm::angleAxis(0.0f, glm::vec3(1.0f, 0.0f, 0.0f)));
  frame5->addPose(rightLeg5);
  frame5->addPose(rightShin5);
  frame5->addPose(leftLeg5);
  anim->addFrame(frame5, 1, 0.5f);


  return anim;
}


/* PUBLIC STATIC MEMBERS */

//main part
 bodyPartID_t Human::thoraxID = 0;

 bodyPartID_t Human::headID = 0;

//left
bodyPartID_t Human::leftLegID = 0;
bodyPartID_t Human::leftShinID = 0;
bodyPartID_t Human::leftArmID = 0;
bodyPartID_t Human::leftForearmID = 0;
bodyPartID_t Human::leftHandID = 0;
bodyPartID_t Human::leftFootID = 0;

//right
bodyPartID_t Human::rightLegID = 0;
bodyPartID_t Human::rightShinID = 0;
bodyPartID_t Human::rightArmID = 0;
bodyPartID_t Human::rightForearmID = 0;
bodyPartID_t Human::rightHandID = 0;
bodyPartID_t Human::rightFootID = 0;


//measurments
length_t Human::headWidth = 0.3;
length_t Human::headHeight = 0.3;
length_t Human::headDepth = 0.3;

length_t Human::thoraxWidth = 0.5;
length_t Human::thoraxHeight = 1.0;
length_t Human::thoraxDepth = 0.2;

length_t Human::legWidth = 0.2;
length_t Human::legHeight = 0.6;
length_t Human::legDepth = 0.2;

length_t Human::shinWidth = Human::legWidth;
length_t Human::shinHeight = 0.5;
length_t Human::shinDepth = Human::legDepth;

length_t Human::footWidth = Human::legWidth;
length_t Human::footHeight = 0.2;
length_t Human::footDepth = 2*Human::legDepth;

length_t Human::armWidth = 0.2;
length_t Human::armHeight = 0.6;
length_t Human::armDepth = 0.2;

length_t Human::forearmWidth = Human::armWidth;
length_t Human::forearmHeight = 0.5;
length_t Human::forearmDepth = Human::armDepth;

length_t Human::handWidth = Human::armWidth;
length_t Human::handHeight = Human::armDepth;
length_t Human::handDepth = Human::armDepth;

length_t Human::jointOffset = 0.1;


}
