/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDETYPES_H
#define NDETYPES_H
/** this file only defines types used in this program
*
*
*/
#include <inttypes.h>
#include <glm/glm.hpp>

namespace nde {

  //nde identifiers types
  typedef uint16_t shaderID_t;
  typedef uint16_t modelID_t;
  typedef uint16_t textureID_t;

  //nde geometry types
  typedef float angle_t;
  typedef float length_t;
  typedef float collisionTime_t;
  typedef u_char axis_t;

  //nde GL types
  typedef uint64_t vboSize_t;

  //nde body types
  typedef unsigned bodyPartID_t;

  //nde time types
  typedef double timeInterval_t;
  typedef double timePoint_t;

  //nde view and screen types
  typedef float screenRatio_t;
  typedef uint32_t screenID_t;
  typedef uint16_t screenSize_t;

  //nde texture sizes
  typedef uint32_t textureSize_t;

  //animation modes
  typedef int32_t frameRank_t;
  typedef uint16_t interpolationMode_t;
  
  //game objects
  typedef uint16_t GameObjectType_t;

  //physics
  typedef float mass_t;
  typedef glm::vec3 speed_t;
  typedef glm::vec3 momentum_t;

  //controller 
  typedef int16_t inputIdentifier_t;
  typedef glm::vec2 mousePos_t;
  typedef glm::vec2 mouseMove_t;
  typedef float axisValue_t;
  typedef int keyboardKey_t;
  typedef int keyAction_t;
}


#endif //NDETYPES_H
