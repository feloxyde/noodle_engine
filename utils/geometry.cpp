/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "geometry.hpp"
#include <limits>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/projection.hpp>

#include <cmath>
namespace nde {
static std::string vec3ToString(glm::vec3 const& vec)
{
  std::string a;
  a += "(";
  a += std::to_string(vec.x);
  a += ";";
  a += std::to_string(vec.y);
  a += ";";
  a += std::to_string(vec.z);
  a += ")";
  return a;
}
/* ##################################################################### */
/* ########## GEOMETRY CLASS IMPLEMENTATION ###~~~~~#################### */
/* ##################################################################### */

/* PUBLIC STATIC METHODS */

glm::quat Geometry::reorient(glm::quat source, glm::quat destination)
{
  return destination * glm::inverse(source);
}

length_t Geometry::proportion(glm::vec3 v, glm::vec3 col)
{
  //only one coordinate is necessary
  if (col.x != 0.0){
    return v.x / col.x;
  }
  //same for y
  if (col.y != 0.0){
    return v.y / col.y;
  }
  //same for z
  if (col.z != 0.0){
    return v.z / col.z;
  }
  // past here col is 0,0,0
  return 0.0; //#FIXME add better exception gestion
}


glm::vec3 Geometry::reflection(glm::vec3 v, glm::vec3 normal)
{
  //reflexion of v is two time the projection of v on normal minus v
  return 2.0f*glm::proj(v, normal) - v; 
}

glm::vec3 Geometry::colinearPart(glm::vec3 v, glm::vec3 normal)
{
  if(glm::dot(v, normal) < 0.0f)
  {
    return glm::proj(v, -normal);
  }
  else {
    return glm::proj(v, normal);
  }

}


glm::vec3 Geometry::forwardAlong(glm::vec3 v, glm::quat orient)
{
  glm::mat4 rot = glm::mat4_cast(orient);
  return glm::vec3(  rot * glm::vec4(v, 1.0f)  );
}


glm::vec3 Geometry::forwardAlong(glm::vec3 v, glm::vec3 ort, glm::vec3 up)
{
    //compute rotation angle
  glm::vec3 orient = glm::normalize(ort);
  //we dont take care of the [y] coordinate
  orient.y = 0.0f;
  angle_t alignAngle = glm::orientedAngle(glm::normalize(orient), glm::normalize(glm::vec3(0,0,1)), glm::normalize(up))/* + M_PI */;

  glm::vec3 refmove = glm::rotate(v, -alignAngle, glm::normalize(up));
  //apply movement to camera
  return refmove;
}

/* ##################################################################### */
/* ########## TRANSFORMABLE CLASS IMPLEMENTATION ####################### */
/* ##################################################################### */

/* STATIC CONSTANTS */

const glm::vec3 Transformable::DEFAULT_REFERENCE_FRAME_POSITION = glm::vec3(0,0,0);
const glm::quat Transformable::DEFAULT_REFERENCE_FRAME_ORIENTATION = glm::quat(glm::vec3(0,0,0));
const glm::vec3 Transformable::DEFAULT_REFERENCE_FRAME_ROT_CENTER = glm::vec3(0,0,0);
/* PUBLIC OBJECT METHODS */

Transformable::Transformable():
m_referenceFrame(),
m_position(DEFAULT_REFERENCE_FRAME_POSITION),
m_orientation(DEFAULT_REFERENCE_FRAME_ORIENTATION),
m_rotCenter(DEFAULT_REFERENCE_FRAME_ROT_CENTER)
{}

Transformable::Transformable(std::weak_ptr<Transformable> referenceFrame):
m_referenceFrame(referenceFrame),
m_position(DEFAULT_REFERENCE_FRAME_POSITION),
m_orientation(DEFAULT_REFERENCE_FRAME_ORIENTATION),
m_rotCenter(DEFAULT_REFERENCE_FRAME_ROT_CENTER)
{}

Transformable::Transformable(Transformable const& origin):
m_referenceFrame(origin.getReferenceFrame()),
m_position(origin.getPosition()),
m_orientation(origin.getOrientation()),
m_rotCenter(origin.getRotCenter())
{}
  

Transformable::~Transformable()
{}

void Transformable::setReferenceFrame(std::weak_ptr<Transformable> referenceFrame)
{
  m_referenceFrame = referenceFrame;
  auto rf = m_referenceFrame.lock();
}

void Transformable::setReferenceFrameKeepRelative(std::weak_ptr<Transformable> referenceFrame)
{
  //#FIXME implement this please
}


void Transformable::attachToReferenceFrame(glm::vec3 const& selfAnchor, std::weak_ptr<Transformable> referenceFrame, glm::vec3 const& refAnchor)
{
  this->setReferenceFrame(referenceFrame);
  this->attachToReferenceFrame(selfAnchor, refAnchor);
}

void Transformable::attachToReferenceFrame(glm::vec3 const& selfAnchor, glm::vec3 const& refAnchor)
{


  //first, compute anchor in world
  auto rf = m_referenceFrame.lock();
  glm::vec3 selfAnchorWorld = selfAnchor + this->getPosition();
  glm::vec3 refAnchorWorld;
  if(rf){
    refAnchorWorld = refAnchor + rf->getPosition();
    }
  else refAnchorWorld = refAnchor + DEFAULT_REFERENCE_FRAME_POSITION;

  //now translation
  this->translate(refAnchorWorld - selfAnchorWorld);

  //set the rotcenter
  this->setRotCenter(refAnchorWorld);

}


glm::vec3 Transformable::getPosition() const
{
return m_position;
}

glm::quat Transformable::getOrientation() const
{
  return m_orientation;
}

glm::vec3 Transformable::getRotCenter() const
{
  return m_rotCenter;
}

std::weak_ptr<Transformable> Transformable::getReferenceFrame() const
{
  return m_referenceFrame;
}

void Transformable::setPosition(glm::vec3 const& position)
{
  m_position = position;
}

void Transformable::setOrientation(glm::quat const& orientation)
{
  m_orientation = orientation;
}

void Transformable::setRotCenter(glm::vec3 const& rotCenter)
{
  m_rotCenter = rotCenter;
}

glm::vec3 Transformable::getRelativePosition()
{
  glm::vec3 framePos = DEFAULT_REFERENCE_FRAME_POSITION;
  auto rf = m_referenceFrame.lock();
  if (rf) {
    framePos = rf->getPosition();
  }
  return this->getPosition() - framePos;
}

glm::quat Transformable::getRelativeOrientation()
{
  glm::quat frameOrient = DEFAULT_REFERENCE_FRAME_ORIENTATION;
  auto rf = m_referenceFrame.lock();
  if(rf){
    frameOrient = rf->getOrientation();
    }

  return glm::conjugate(frameOrient)*this->getOrientation();

}

void Transformable::setRelativePosition(glm::vec3 const& position)
{
  this->setPosition(position + this->getRelativePosition());
}
/** relative raw change of orientation */
void Transformable::setRelativeOrientation(glm::quat const& orientation)
{
  this->setOrientation(this->getRelativeOrientation() * orientation);
}


void Transformable::reposition(glm::vec3 const& position)
{
  glm::vec3 posDiff = position - m_position;
  this->translate(posDiff);
}


void Transformable::reorient(glm::quat const& orientation)
{
  glm::quat toGO = Geometry::reorient(m_orientation, orientation);
  glm::vec3 rotCenter = getRotCenter();
  translate(-rotCenter);
  transform(glm::mat4_cast(toGO));
  translate(rotCenter);
}

void Transformable::relativeReposition(glm::vec3 const& position)
{

  glm::vec3 framePos = DEFAULT_REFERENCE_FRAME_POSITION;
  auto rf = m_referenceFrame.lock();
  if (rf) framePos = rf->getPosition();

  this->reposition(this->getPosition() + framePos);
}

void Transformable::relativeReorient(glm::quat const& orientation)
{
  glm::quat frameOrient = DEFAULT_REFERENCE_FRAME_ORIENTATION;
  auto rf = m_referenceFrame.lock();
  if(rf) frameOrient = rf->getOrientation();

  this->reorient(frameOrient * orientation);
}

void Transformable::transform(glm::mat4 const& transformation)
{
  //transforming position as a pure matrix transformation
  //std::cout << "transformation in progress " << m_rotCenter.x <<" " << m_rotCenter.y <<" "<< m_rotCenter.z<< std::endl;
  m_position = glm::vec3(transformation*glm::vec4(m_position, 1.0));
  //same goes for rot center
  m_rotCenter = glm::vec3(transformation*glm::vec4(m_rotCenter, 1.0));
  //and finally we want only to rotate the orientation
  m_orientation = glm::quat_cast(transformation)*m_orientation;

  this->bodyTransform(transformation);
//    std::cout << "transformation ended " << m_rotCenter.x <<" " << m_rotCenter.y <<" "<< m_rotCenter.z<< std::endl;
}

void Transformable::translate(glm::vec3 const& translation)
{
  m_position += translation;
  m_rotCenter += translation;
  this->bodyTranslate(translation);
}

void Transformable::rotate(glm::vec3 const& axis, angle_t angle)
{
  this->rotate(axis, angle, m_rotCenter);
}

void Transformable::rotate(glm::vec3 const& axis, angle_t angle, glm::vec3 const& rotCenter)
{
  this->rotate(glm::angleAxis(angle, axis), rotCenter);
}

void Transformable::rotate(glm::quat const& rotation)
{

  this->rotate(rotation, m_rotCenter);
}

void Transformable::rotate(glm::quat const& rotation, glm::vec3 const& rotCenter)
{

  glm::vec3 rc = rotCenter;
  //referential change to center rotCenter
  this->translate(-rc);
  //rotation
  this->transform(glm::mat4_cast(rotation));
  //referencial change to come back
  this->translate(rc);

}


/** returns the transformation matrix for an object in world to have coordinates in 'this' referential */
glm::mat4 Transformable::asReferenceFrameTransformation()
{
  glm::mat4 rotation = glm::mat4_cast(m_orientation);
  glm::mat4 translation = glm::translate(glm::mat4(1.0), m_position);
  return translation*rotation;
}
/** returns the transformation matrix for an object in 'this' referential o have coordinates in world */
glm::mat4 Transformable::asReferenceFrameTransformationBack()
{
  return glm::inverse(this->asReferenceFrameTransformation());
}


/* PROTECTED OBJECT METHODS */

void Transformable::bodyTransform(glm::mat4 const& transformation)
{
  (void)transformation;
}

void Transformable::bodyTranslate(glm::vec3 const& translation)
{
  (void)translation;
}
/* ##################################################################### */
/* ########## PROJECTION CLASS IMPLEMENTATION ########################## */
/* ##################################################################### */

/* PUBLIC STATIC METHODS */

/** returns true if two projection on the same normal overlaps
*   if yes, extractor is the tinyest move needed to extract "proj1" from proj2
*/
bool SolidProjection::staticOverlapTest(SolidProjection const&  proj1, SolidProjection const& proj2, glm::vec3& extractor)
{
//return immediately if not colliding
  if(proj1.begin() > proj2.end() || proj1.end() < proj2.begin())
  return false;

  //lets compute extractor
  length_t distTop = proj2.end() - proj1.begin();
  length_t distBottom = proj2.begin() - proj1.end();
  if(std::abs(distTop) < std::abs(distBottom))//Select the shortest
    {extractor = proj1.normal()*distTop;}
  else
    {extractor = proj1.normal()*distBottom;}

 return true;
}
/** return true if proj1 overlaps proj2 while proj2 travels of relMove2 alongside normal of projs */
bool SolidProjection::sweptOverlapTest(SolidProjection const& proj1, SolidProjection const& proj2, length_t relMove2,
                             collisionTime_t& timeIn, collisionTime_t& timeOut,
                             glm::vec3& normal)
{
  
  //two cases, depending on the relative speed
  if (relMove2 > 0.0){
      //case it cant reach
      if(proj1.begin() >= proj2.end() + relMove2 || proj1.end() <= proj2.begin()){
        return false;
      }

      //Else compute entry and out time
      timeIn = (proj1.begin() - proj2.end())/relMove2;
      timeOut = (proj1.end() - proj2.begin())/relMove2;
  }
  else if(relMove2 < 0.0){
    //case it cant reach
    if(proj1.end() <= proj2.begin() + relMove2 || proj1.begin() >= proj2.end()){
      return false;
    }
    //else compute entry and out time
    timeIn = (proj2.begin() - proj1.end())/-relMove2;
    timeOut = (proj2.end() - proj1.begin())/-relMove2;
  }
  else {
    if(proj1.begin() >= proj2.end() || proj1.end() <= proj2.begin())
      {return false;}
    else {
      timeIn = -std::numeric_limits<collisionTime_t>::infinity();
      timeOut = std::numeric_limits<collisionTime_t>::infinity();
    }
  }
/*
  if(timeIn < 0.0)
  {
    timeIn = 0.0;
  }
*/
  normal = proj1.normal();
  return true;

}
/* PUBLIC OBJECTS METHODS */

//SolidProjection(glm::vec3 begin, glm::vec3 end);
SolidProjection::SolidProjection(length_t begin, length_t end, glm::vec3 normal):
m_begin(begin), m_end(end), m_normal(normal)
{}


length_t SolidProjection::begin() const
{
  return m_begin;
}

length_t SolidProjection::end() const
{
  return m_end;
}

glm::vec3 const& SolidProjection::normal() const
{
  return m_normal;
}

/* ##################################################################### */
/* ########## BOUNDINGBOX CLASS IMPLEMENTATION ######################### */
/* ##################################################################### */

/* PUBLIC STATIC METHODS */

bool BoundingBox::staticCollisionTest(BoundingBox const& box1, BoundingBox const& box2, glm::vec3& extractor)
{
  //first lets check in "this" referential

  //each parts of the code will be
  std::list<glm::vec3> const& normals1 = box1.normals();

  //maybe check if normals is not empty ?

  auto normal = normals1.begin();
  bool overlap = true;
  extractor = glm::vec3(std::numeric_limits<float>::infinity());
  glm::vec3 ext; //temporaty extractor
  while(overlap && normal != normals1.end()){
    overlap = SolidProjection::staticOverlapTest(box1.projection(*normal), box2.projection(*normal), ext);
    if(overlap && glm::length2(extractor) > glm::length2(ext))
      { extractor = ext; }//selecting tinyest extraction
    ++normal; //try the next
  }

  if(!overlap){return false;} //return if a gap is found

  //lets check the box referential

  std::list<glm::vec3> const& normals2 = box2.normals();
  normal = normals2.begin();

  while(overlap && normal != normals2.end()){
    overlap = SolidProjection::staticOverlapTest(box1.projection(*normal), box2.projection(*normal), ext);
    if(overlap && glm::length2(extractor) > glm::length2(ext))
      { extractor = ext; }//selecting tinyest extraction
    ++normal; //try the next
  }

  if(!overlap){return false;} //return if a gap is found

  return true; //always overlap, we collided
}

/** computes a sewpt collision test */
bool BoundingBox::sweptCollisionTest(BoundingBox& box1, glm::vec3& move1,
                               BoundingBox& box2, glm::vec3& move2,
                               collisionTime_t& timeIn, collisionTime_t& timeOut,
                               glm::vec3& normalIn) //normal1 unused for the moment
{
  //#FIXME add some mechanism for the normals of the collision, and optimize code (we can do it i think)
  //first compute relative move of box2 referenced by box1:
  glm::vec3 relMove2 = move2 - move1;
  //next the algorithm is quite similar to SAT
  timeIn = -(std::numeric_limits<collisionTime_t>::infinity());
  timeOut = std::numeric_limits<collisionTime_t>::infinity();

  std::list<glm::vec3> const& normals1 = box1.normals();
  auto normal = normals1.begin();
  collisionTime_t tin; //temporary time in
  collisionTime_t tout; //temporary time out
  bool collision = true;
  glm::vec3 normalProj(0);

  while (collision && normal != normals1.end())
  {
    collision = SolidProjection::sweptOverlapTest(box1.projection(*normal), box2.projection(*normal),
                                                    Geometry::proportion(glm::proj(relMove2, *normal), *normal),
                                                    tin, tout, normalProj);
    //finding the max time in and min time out
    if(collision){
      if(tin > timeIn){
        timeIn = tin;
        normalIn = normalProj;
      }
      if(tout < timeOut) 
        {timeOut = tout;}
      //return if impossible collision situation
      if(timeOut < timeIn) 
        {return false;}
    }
    ++normal;
  }

  if(!collision) {return false;}

  //same with box2 normals
  std::list<glm::vec3> const& normals2 = box2.normals();
  normal = normals2.begin();

  while (collision && normal != normals2.end())
  {
    collision = SolidProjection::sweptOverlapTest(box1.projection(*normal), box2.projection(*normal),
                                                    Geometry::proportion(glm::proj(relMove2, *normal), *normal),
                                                    tin, tout, normalProj);
    //finding the max time in and min time out
    if(collision){
      if(tin > timeIn){
        timeIn = tin;
        normalIn = normalProj;
      }
      //return if impossible situation
      if(timeOut < timeIn) {return false;}
    }
    ++normal;
  }

  if(!collision) {return false;}

  return true;
}
/* PUBLIC OBJECT METHODS */

BoundingBox::BoundingBox():
Transformable::Transformable()
{}

BoundingBox::BoundingBox(std::weak_ptr<Transformable> referenceFrame):
Transformable::Transformable(referenceFrame)
{}

BoundingBox::~BoundingBox()
{}



/* ##################################################################### */
/* ########## AXISALIGNEDBOUNDINGBOX CLASS IMPLEMENTATION ############## */
/* ##################################################################### */


/* PUBLIC STATIC METHODS */

/** returns the slightest AABB containing these two AABB */
AxisAlignedBoundingBox AxisAlignedBoundingBox::merge(AxisAlignedBoundingBox const& box1, AxisAlignedBoundingBox const& box2)
{
  glm::vec3 begin;
  glm::vec3 end;

  if(box1.begin().x < box2.begin().x) begin.x = box1.begin().x;
  else begin.x = box2.begin().x;
  if(box1.begin().y < box2.begin().y) begin.y = box1.begin().y;
  else begin.y = box2.begin().y;
  if(box1.begin().z < box2.begin().z) begin.z = box1.begin().z;
  else begin.z = box2.begin().z;

  if(box1.end().x > box2.end().x) end.x = box1.end().x;
  else end.x = box2.end().x;
  if(box1.end().y > box2.end().y) end.y = box1.end().y;
  else end.y = box2.end().y;
  if(box1.end().z > box2.end().z) end.z = box1.end().z;
  else end.z = box2.end().z;

  return AxisAlignedBoundingBox(begin, end);
}


/* PRIVATE STATIC METHODS */

AxisAlignedBoundingBox::NormalList::NormalList():
list()
{
  list.push_back(glm::vec3(1.0, 0.0, 0.0));

  list.push_back(glm::vec3(0.0, 1.0, 0.0));

  list.push_back(glm::vec3(0.0, 0.0, 1.0));

}

/* PRIVATE STATIC MEMBERS */

AxisAlignedBoundingBox::NormalList AxisAlignedBoundingBox::NORMAL_LIST; //the normal list, since it is always the same. DO NOT CHANGE IT !

/* PUBLIC OBJECT METHODS */

AxisAlignedBoundingBox::AxisAlignedBoundingBox(glm::vec3 const& begin, glm::vec3 const& end):
m_begin(begin), m_end(end)
{}

AxisAlignedBoundingBox::~AxisAlignedBoundingBox()
{}

/** returns all normals of the box in order to detect collisions */
std::list<glm::vec3> const& AxisAlignedBoundingBox::normals() const
{
  return AxisAlignedBoundingBox::NORMAL_LIST.list;
}

/** returns the projection object if the Box by a normal vector */
SolidProjection AxisAlignedBoundingBox::projection(glm::vec3 normal) const
{
  //if the normal is an axis :
  if ((normal.x == 0.0 && normal.y == 0.0) || (normal.y == 0.0 && normal.z == 0.0) || (normal.z == 0.0 && normal.x == 0.0))
  {
    length_t bottom = Geometry::proportion(glm::proj(m_begin, normal), normal); //bottom of the projection
    length_t top = Geometry::proportion(glm::proj(m_end, normal), normal); //bottom of the projection

    if(bottom > top)
    {
      length_t tmp = bottom;
      bottom = top;
      top = tmp;
    }
    SolidProjection projection(bottom, top, normal);
    return projection;
  }
 //#FIXME here please add case where normal not an axis, this else is bullshit
 else return SolidProjection(0.0, 1.0, normal);
}



glm::vec3 const& AxisAlignedBoundingBox::begin() const
{
  return m_begin;
}

glm::vec3 const& AxisAlignedBoundingBox::end() const
{
  return m_end;
}

glm::vec3 const AxisAlignedBoundingBox::center() const
{
  return this->end() - this->begin();
}

/** returns the slightest AABB englobing the box */
AxisAlignedBoundingBox AxisAlignedBoundingBox::asAxisAlignedBoundingBox()
{
  return *this;
}


/** PROTECTED OBJECT METHODS */

/** transforms the body of the transformable if he has one */
void AxisAlignedBoundingBox::bodyTransform(glm::mat4 const& transformation)
{
      //#FIXME this is quite incorrect, we need to override more methods into Transformable
      //no rotation but we want to keep translation, OK ?
      glm::vec3 oldCenter = this->center();
      glm::vec3 newCenter = glm::vec3(transformation*glm::vec4(oldCenter, 1.0));
      glm::vec3 translation = newCenter - oldCenter;
      this->bodyTranslate(translation);
}

/** translate the body of the transformable if he has one */
void AxisAlignedBoundingBox::bodyTranslate(glm::vec3 const& translation)
{
  m_begin = m_begin + translation;
  m_end = m_end + translation;
}


/* ##################################################################### */
/* ########## ORIENTEDBOUNDINGBOX CLASS IMPLEMENTATION ################# */
/* ##################################################################### */


/* PUBLIC OBJECT METHODS */

/** creates a Oriented Bounding box as an AABB with each of its lengths over axis
* witdh = x , height = y, depth = z */
OrientedBoundingBox::OrientedBoundingBox(length_t width, length_t height, length_t depth):
BoundingBox::BoundingBox(), m_body(8), m_normals()
{
  //#FIXME add all the computations into static constants for more perfs when creating items ?
  //setting the points

  length_t w = width/2.0;
  length_t h = height/2.0;
  length_t d = depth/2.0;

  m_body[0] = glm::vec3(-w, -h, -d);
  m_body[1] = glm::vec3(-w, -h,  d);
  m_body[2] = glm::vec3(-w,  h, -d);
  m_body[3] = glm::vec3( w, -h, -d);
  m_body[4] = glm::vec3(-w,  h,  d);
  m_body[5] = glm::vec3( w, -h,  d);
  m_body[6] = glm::vec3( w,  h, -d);
  m_body[7] = glm::vec3( w,  h,  d);

  //setting the norm of faces (3 norms are added, orthers are for calculation)
  glm::vec3 normXp(1.0, 0.0, 0.0); //norm X plus
  glm::vec3 normXm(-1.0, 0.0, 0.0); //norm X minus (for calculation)
  glm::vec3 normYp(0.0, 1.0, 0.0);
  glm::vec3 normYm(0.0, -1.0, 0.0); //for calculation
  glm::vec3 normZp(0.0, 0.0, 1.0);

  //setting the norms of the edges
  //the round of a face (face Z)
  glm::vec3 normXpZp = glm::normalize(normXp + normZp);
  glm::vec3 normXmZp = glm::normalize(normXm + normZp);
  glm::vec3 normYpZp = glm::normalize(normYp + normZp);
  glm::vec3 normYmZp = glm::normalize(normYm + normZp);
  //and complete with the two adjacent of same face (face Y)
  glm::vec3 normXpYp = glm::normalize(normXp + normYp);
  glm::vec3 normXmYp = glm::normalize(normXm + normYp);

  //adding all of the norms but the computationnals two
  m_normals.push_back(normXp);
  m_normals.push_back(normYp);
  m_normals.push_back(normZp);
  m_normals.push_back(normXpZp);
  m_normals.push_back(normXmZp);
  m_normals.push_back(normYpZp);
  m_normals.push_back(normYmZp);
  m_normals.push_back(normXpYp);
  m_normals.push_back(normXmYp);

  //#FIXME the normals can be created in a static constant and then copied
}

OrientedBoundingBox::~OrientedBoundingBox()
{}

/** returns all normals of the box in order to detect collisions */
std::list<glm::vec3> const& OrientedBoundingBox::normals() const
{
  return m_normals;
}

/** returns the projection object if the Box by a normal vector */
SolidProjection OrientedBoundingBox::projection(glm::vec3 normal) const
{

  //initializing parse
  length_t bottom = Geometry::proportion(glm::proj(m_body[0], normal), normal); //bottom of the projection
  length_t top = bottom; //top part of the projection
  length_t current; //temp var to project only once
  for (size_t vertexIndex = 1;  vertexIndex < m_body.size(); ++vertexIndex){ //check if better projections of body points
    current = Geometry::proportion(glm::proj(m_body[vertexIndex], normal), normal); //#FIXME add normalisation of normal ?
  //  std::cout << "actual coordinate is " << m_body[vertexIndex].x << " " << m_body[vertexIndex].y << " "<< m_body[vertexIndex].z << std::endl;
    if(bottom > current)
    //if(bottom.x > projec.x && bottom.y > projec.y && bottom.z > projec.z)
      {bottom = current;}

    if(top < current)
    //if(top.x < projec.x || top.y < projec.y || top.z < projec.z)
      {top = current;}
  }

  //now we have top and bottom
  SolidProjection projection(bottom, top, normal);
  return projection;
}

/** returns the slightest AABB englobing the box */
AxisAlignedBoundingBox OrientedBoundingBox::asAxisAlignedBoundingBox()
{
  float inf = std::numeric_limits<float>::infinity();
  glm::vec3 begin(inf, inf, inf);
  glm::vec3 end(-inf, -inf, -inf);

  for (size_t vertexIndex = 0; vertexIndex < m_body.size(); ++vertexIndex){
   if(begin.x > m_body[vertexIndex].x) begin.x = m_body[vertexIndex].x;
   if(begin.y > m_body[vertexIndex].y) begin.y = m_body[vertexIndex].y;
   if(begin.z > m_body[vertexIndex].z) begin.z = m_body[vertexIndex].z;

   if(end.x < m_body[vertexIndex].x) end.x = m_body[vertexIndex].x;
   if(end.y < m_body[vertexIndex].y) end.y = m_body[vertexIndex].y;
   if(end.z < m_body[vertexIndex].z) end.z = m_body[vertexIndex].z;
 }

 AxisAlignedBoundingBox envelop(begin, end);
 return envelop;

}

/* PROTECTED OBJECT METHODS */


/** transforms the body of the transformable if he has one */
void OrientedBoundingBox::bodyTransform(glm::mat4 const& transformation)
{
  //first transform body
  for (size_t vertexIndex = 0; vertexIndex < m_body.size(); ++vertexIndex){
   m_body[vertexIndex] = glm::vec3(transformation*glm::vec4(m_body[vertexIndex], 1.0));
  }
  //then rotate normals
  glm::quat rotation = glm::quat_cast(transformation);

  for (auto normal = m_normals.begin(); normal != m_normals.end(); ++normal){
    *normal = rotation*(*normal);
  }

}
/** translate the body of the transformable if he has one */
void OrientedBoundingBox::bodyTranslate(glm::vec3 const& translation)
{
  for (size_t vertexIndex = 0; vertexIndex < m_body.size(); ++vertexIndex)
    m_body[vertexIndex] += translation;
}

}
