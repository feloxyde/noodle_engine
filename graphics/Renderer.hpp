/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_RENDERER_H
#define NDE_RENDERER_H

#include "ndeGL.hpp"
#include <glm/glm.hpp>
#include "../utils/ndetypes.hpp"
#include <map>
#include <memory>
//#include "GraphicObject.hpp" /* put it into .cpp to avoir circular includes */

namespace nde {

class GraphicObject; //forward declaration, we know a graphic object exists
class View;
class Model;
class View;

class Renderer {

public:

  Renderer();
  virtual ~Renderer();

  /** adds a graphic object to the renderer to be drawn */
  virtual void referGraphicObject(std::weak_ptr<GraphicObject> object) = 0;

  /** removes the graphic object from the renderer */
  void removeGraphicObject(std::weak_ptr<GraphicObject> object);

  /** renders all enlisted objects into the view */
  virtual void render(View& view, timeInterval_t elapsed) = 0;

};

/** contains a pointer to a GraphicObject and is linked doubly to
others renderCells in a Listed Renderer (ModelRenderer) */
class RendererCell {

public:
  RendererCell(std::weak_ptr<GraphicObject> object, RendererCell* prev, RendererCell* next);
  virtual  ~RendererCell(); //automatically links the prev and next to each other before being deleted;

  void setPrev(RendererCell* prev);
  void setNext(RendererCell* next);
  RendererCell* getPrev();
  RendererCell* getNext();

  std::weak_ptr<GraphicObject> getObject();

private:
  std::weak_ptr<GraphicObject> m_object;
  RendererCell* m_prev;
  RendererCell* m_next;
};

/** A tool to pack objects by model and render them easy and fast
*
*
*/
class ModelRenderer : public Renderer {

public:
  /* creates an empty renderer */
  ModelRenderer(std::shared_ptr<Model> model);

  virtual ~ModelRenderer();

  /** adds a graphic object to the renderer to be drawn */
  virtual void referGraphicObject(std::weak_ptr<GraphicObject> object);

  /** renders all enlisted objects into the view */
  virtual void render(View& view, timeInterval_t elapsed);

private:
  std::unique_ptr<RendererCell> m_first;//delete
  std::shared_ptr<Model> m_model; //do not delete
  //RendererCell* m_last; npt useful for the moment, insertions are made in the head
};

/** A tool to pack object by texture and model and render them easy and fast */
class TextureRenderer : public Renderer {

public:
  /** creates an empty renderer */
  TextureRenderer(std::shared_ptr<Texture> texture);

  /** deletes the renderer and all his subs renderer */
  virtual  ~TextureRenderer();

  /** adds a graphic object to the renderer to be drawn */
  virtual void referGraphicObject(std::weak_ptr<GraphicObject> object);

  /** renders all enlisted objects into the view */
  virtual void render(View& view, timeInterval_t elapsed);

private:
  std::map<std::shared_ptr<Model>, std::shared_ptr<ModelRenderer>> m_modelRenderers; //delete subrenderers, not model*
  std::shared_ptr<Texture> m_texture; //do not delete
};


/** atool to pack objects by shader and texture and model and render them easy and fast */
class ShaderRenderer : public Renderer {

public:
  /** creates an empty renderer */
  ShaderRenderer(std::shared_ptr<Shader> shader);

  /** deletes the renderer and all his subs renderer */
  virtual ~ShaderRenderer();

  /** adds a graphic object to the renderer to be drawn */
  virtual void referGraphicObject(std::weak_ptr<GraphicObject> object);

  /** renders all enlisted objects into the view */
  virtual void render(View& view, timeInterval_t elapsed);

private:
  std::map<std::shared_ptr<Texture>, std::shared_ptr<TextureRenderer>> m_textureRenderers; //delete only texrenderers
  std::shared_ptr<Shader> m_shader; //do not delete
  GLuint m_uniformViewLocation;
  GLuint m_uniformProjectionLocation;

};


/** a tool to pack objects by shader and texture and model and render them easy and fast */
class GlobalRenderer : public Renderer {

public:
  /** creates an empty renderer */
  GlobalRenderer();

  /** deletes the renderer and all his subs renderer */
  virtual ~GlobalRenderer();

  /** adds a graphic object to the renderer to be drawn */
  virtual void referGraphicObject(std::weak_ptr<GraphicObject> object);

  /** renders all enlisted objects into the view */
  virtual void render(View& view, timeInterval_t elapsed);

private:
  std::map<std::shared_ptr<Shader>, std::shared_ptr<ShaderRenderer>> m_shaderRenderers;
};


} //namespace nde
#endif //NDE_RENDERER_H
