/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Model.hpp"
#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "../utils/ndetypes.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

#include "ndeGL.hpp"


namespace nde {


/* ##################################################################### */
/* ########## MODEL CLASS IMPLEMENTATION ############################### */
/* ##################################################################### */

/* PUBLIC CONSTANTS */
const GLuint Model::VERTEX_COORDINATES_SHADER_ATTRIB = 0;
const GLuint Model::VERTEX_TEXTURE_COORDINATES_SHADER_ATTRIB = 1;
const GLuint Model::VERTEX_NORMAL_SHADER_ATTRIB = 2;

/* PUBLIC STATIC METHODS */

std::shared_ptr<Model> Model::createTexturedIndexedModel(const void* vertices, vboSize_t size,  const void* textureCoords, vboSize_t sizeTex, const void* indexes, vboSize_t indexSize)
{
  auto m = std::make_shared<IndexedModel>(vertices, size, indexes, indexSize);
  std::shared_ptr<GLBufferObject>bo(new VertexBuffer<GLfloat, 2, GL_FLOAT>(textureCoords, sizeTex, Model::VERTEX_TEXTURE_COORDINATES_SHADER_ATTRIB));
  m->addVerticeData(bo);
  return m;
}

//This is not Optimized !!!!
std::shared_ptr<Model> Model::createMonochromeIndexedModel(const void* vertices, vboSize_t size, const void* indexes, vboSize_t indexSize, glm::vec3& color)
{
  auto m = std::make_shared<MonochromeIndexedModel>(vertices, size, indexes, indexSize, color);
  return m;
}

std::shared_ptr<Model> Model::createTexturedNormalizedIndexedModel(
    const void* vertices, 
    vboSize_t size,  
    const void* textureCoords, 
    vboSize_t sizeTex,
    const void* normals,
    vboSize_t sizeNorm,
    const void* indexes, 
    vboSize_t indexSize)
{
  auto m = std::make_shared<IndexedModel>(vertices, size, indexes, indexSize);
  std::shared_ptr<GLBufferObject> bo(new VertexBuffer<GLfloat, 2, GL_FLOAT>(textureCoords, sizeTex, Model::VERTEX_TEXTURE_COORDINATES_SHADER_ATTRIB));
  m->addVerticeData(bo);
  std::shared_ptr<GLBufferObject> no(new VertexBuffer<GLfloat, 3, GL_FLOAT>(normals, sizeNorm, Model::VERTEX_NORMAL_SHADER_ATTRIB));
  m->addVerticeData(no);
  return m;

}


/** loads a model by its name */
std::shared_ptr<Model> Model::loadModel(std::string modelName)
{
  return nullptr;
}
/** deletes all loaded models */
void Model::deleteModels()
{}

modelID_t Model::getNextModelID()
{
  return ++s_nextID;
}

/* PRIVATE STATIC MEMBERS */

std::map<std::string, std::shared_ptr<Model>> Model::s_models; //a list of all shaders loaded using loadModel()
modelID_t Model::s_nextID; //shader ids


/* PUBLIC OBJECTS METHODS */

/** creating a model from a vertexArray and a shader */
Model::Model(const void* vertices, vboSize_t size):
m_ID(getNextModelID()), m_verticesData()
{
  std::shared_ptr<GLBufferObject> bo(new VertexBuffer<GLfloat, 3, GL_FLOAT>(vertices, size, Model::VERTEX_COORDINATES_SHADER_ATTRIB));
  this->addVerticeData(bo);
}

/** creating a model from a filename */
Model::Model(std::string modelName)
{}
/** deletes a model and frees its ressources */
Model::~Model()
{}


void Model::addVerticeData(std::shared_ptr<GLBufferObject> data)
{
  m_verticesData.push_back(data); //object is now owning data ! 
}

/** prepare model for use */
void Model::enable()
{
  //enabling data
  for (auto vd = m_verticesData.begin(); vd != m_verticesData.end(); ++vd)
  {
    (*vd)->bind();
    (*vd)->enable();
  }

}

void Model::disable()
{  
  for (auto vd = m_verticesData.begin(); vd != m_verticesData.end(); ++vd)
  {
    (*vd)->disable();
  }
}

/** draws the model using modelView matrix */
void Model::draw()
{
  glDrawArrays(GL_TRIANGLES, 0, m_verticesData[0]->elementCount());
}
/** draws the model using m v  p matrixs */
//virtual void draw(glm::mat4& model, glm::mat4& view, glm::mat4& projection);


/** returns model ID */
modelID_t Model::ID()
{
  return m_ID;
}

void Model::bindToShader(std::shared_ptr<Shader> shader)
{
   //nothing to do
}

/* ##################################################################### */
/* ##########  INDEXEDMODEL CLASS IMPLEMENTATION ####################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

/** creating a model from vertices, indexes and shader */
IndexedModel::IndexedModel(const void* vertices, vboSize_t size, const void* indexes, vboSize_t indexSize):
Model(vertices, size), m_indexes(indexes, indexSize)
{

}

/** creating a model from a filename */
IndexedModel::IndexedModel(std::string modelName):
Model(modelName)
{}

/** deletes a model and frees its ressources */
IndexedModel::~IndexedModel()
{}


void IndexedModel::enable() {
  this->Model::enable();

  m_indexes.bind();

}

/** prepare model for use */
void IndexedModel::draw()
{
  glDrawElements(
  GL_TRIANGLES,      // mode
  m_indexes.elementCount(),    // count
  GL_UNSIGNED_INT,   // type
  (void*)0           // element array buffer offset
  );
}


/* ##################################################################### */
/* ##########  MONOCHROMEINDEXEDMODEL CLASS IMPLEMENTATION ############# */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

/** creating a model from vertices, indexes and shader and a color */
MonochromeIndexedModel::MonochromeIndexedModel(const void* vertices, vboSize_t size, const void* indexes, vboSize_t indexSize, glm::vec3& color):
IndexedModel(vertices, size, indexes, indexSize),
m_color(color),
m_uniformColorLocation(0)
{}

/** creating a model from a filename */
MonochromeIndexedModel::MonochromeIndexedModel(std::string modelName):
IndexedModel(modelName)
{}
/** deletes a model and frees its ressources */
MonochromeIndexedModel::~MonochromeIndexedModel()
{}

/** prepare model for use */
void MonochromeIndexedModel::enable()
{
  this->IndexedModel::enable();

}

void MonochromeIndexedModel::bindToShader(std::shared_ptr<Shader> shader)
{
   this->Model::bindToShader(shader);
   if (shader!= nullptr){
   m_uniformColorLocation = shader->getUniformLocation(Shader::UNIFORM_COLOR_VECTOR3_NAME);
   }
   shader->uniformVector3f(m_uniformColorLocation, m_color);
}


}
