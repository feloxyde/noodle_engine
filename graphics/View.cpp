/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "View.hpp"
#include "Renderer.hpp"
#include "ndeGL.hpp"

namespace nde {

/* ##################################################################### */
/* ########## VIEW CLASS IMPLEMENTATION ################################ */
/* ##################################################################### */

/* STATIC CONSTANTS */

glm::vec3 const View::DEFAULT_CAMERA_POSITION(0.0, 0.0, 0.0);
glm::vec3 const View::DEFAULT_CAMERA_LOOKAT(1.0, 0.0, 0.0);
glm::vec3 const View::DEFAULT_CAMERA_UP(0.0, 1.0, 0.0);

angle_t const View::DEFAULT_PROJECTION_WIDTH = 70.0;
screenRatio_t const View::DEFAULT_PROJECTION_RATIO = 16.0/9.0; //yeah, i have a extrawide screen
length_t const View::DEFAULT_PROJECTION_NEAR = 0.1f;
length_t const View::DEFAULT_PROJECTION_FAR = 100.f;

/* PUBLIC OBJECT METHODS */

View::View(textureSize_t width, textureSize_t height):
m_camera(DEFAULT_CAMERA_POSITION, DEFAULT_CAMERA_LOOKAT, DEFAULT_CAMERA_UP),
m_projection(DEFAULT_PROJECTION_WIDTH, DEFAULT_PROJECTION_RATIO, DEFAULT_PROJECTION_NEAR, DEFAULT_PROJECTION_FAR),
m_frame(width, height),
m_renderer(nullptr)
{
  
}

View::~View()
{}

void View::update(timeInterval_t elapsed)
{
  m_frame.bind();
    glClearColor(0.0f, 0.4f, 0.4f, 1.0f); //oh god i love this color
//  glClearColor(0.0f, 0.0f, 0.0f, 0.0f); //transparent black is good, but not used for now
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //#FIXME here add the render calls
  if(m_renderer != nullptr) m_renderer->render(*this, elapsed);
}

Camera& View::camera()
{
  return m_camera;
}

Projection& View::projection()
{
  return m_projection;
}

void View::watch(Renderer* renderer)
{
  m_renderer = renderer;
}

void View::use()
{
  m_frame.renderTarget()->bind();
}

} //nde namespace
