/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

//Template, meant to be included at the end of ndgGL.hpp
#define NDE_GL_VERTEX_BUFFER_TEMPLATE
#include "ndeGL.hpp"


namespace nde {
/* ##################################################################### */
/* ########## VERTEX BUFFER CLASS IMPLEMENTATION ####################### */
/* ##################################################################### */

/* PUBLIC OBJECTS METHODS */
template <typename T, size_t D, GLenum E>
VertexBuffer<T,D,E>::VertexBuffer(GLuint shaderLocation):
GLBufferObject(), m_shaderLocation(shaderLocation)
{}

/** creates a buffer with data of size size */
template <typename T, size_t D, GLenum E>
VertexBuffer<T,D,E>::VertexBuffer(const void* data, vboSize_t size, GLuint shaderLocation):
GLBufferObject(data, size), 
m_shaderLocation(shaderLocation)
{
    this->bind();
    this->data(size, data);
}

/** destroys the VBO, frees the ressources */
template <typename T, size_t D, GLenum E>
VertexBuffer<T,D,E>::~VertexBuffer()
{
  //nothing to do
}

/** binds the vertexbuffer in order to allow use */
template <typename T, size_t D, GLenum E>
void VertexBuffer<T,D,E>::bind()
{
  GLBufferObject::bind(GL_ARRAY_BUFFER);


}

template <typename T, size_t D, GLenum E>
void VertexBuffer<T,D,E>::enable()
{
  glEnableVertexAttribArray(m_shaderLocation);//not sure, maybe should be moved to shaders ??

  glVertexAttribPointer(
  m_shaderLocation,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
  D,                  // size
  E,                  // type
  GL_FALSE,           // normalized?
  0,                  // stride
  (void*)0            // array buffer offset
  );

}

template <typename T, size_t D, GLenum E>
void VertexBuffer<T,D,E>::disable()
{
  glDisableVertexAttribArray(m_shaderLocation); //not sure, maybe should be moved to shaders ??

}


/** send datas to the buffer*/
template <typename T, size_t D, GLenum E>
void VertexBuffer<T,D,E>::data(vboSize_t size, const void* data)
{
  m_size = size;
  GLBufferObject::data(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}


/** sends subdatas to the buffer */
template <typename T, size_t D, GLenum E>
void VertexBuffer<T,D,E>::subData(vboSize_t offset, vboSize_t size, const void* data)
{
  GLBufferObject::subData(GL_ARRAY_BUFFER, offset, size, data);
}

/** returns the size of the VertexBuffer */
template <typename T, size_t D, GLenum E>
 vboSize_t VertexBuffer<T,D,E>::getSize()
 {
   return GLBufferObject::m_size;
 }

/** returns number of elements in the VertexBuffer */
template <typename T, size_t D, GLenum E>
vboSize_t VertexBuffer<T,D,E>::elementCount()
{
  return GLBufferObject::m_size/sizeof(T)/D;
}

}