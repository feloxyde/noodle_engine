/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Renderer.hpp"
#include "GraphicObject.hpp" //for GO manipulation
#include "Model.hpp"
#include "View.hpp"

namespace nde {

/* ##################################################################### */
/* ########## RENDERER CLASS IMPLEMENTATION ############################ */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

Renderer::Renderer()
{}

Renderer::~Renderer()
{}

void Renderer::removeGraphicObject(std::weak_ptr<GraphicObject> object)
{
  auto ob = object.lock();
  ob->unsetRenderCell();
}

/* ##################################################################### */
/* ########## RENDERERCELL CLASS IMPLEMENTATION ######################## */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

RendererCell::RendererCell(std::weak_ptr<GraphicObject> object, RendererCell* prev, RendererCell* next):
m_object(object),
m_prev(prev), m_next(next)
{
  //auto insert
  auto ob = object.lock();
  if(ob) ob->setRendererCell(this);
  if(m_prev != nullptr) m_prev->setNext(this);
  if(m_next != nullptr) m_next->setPrev(this);
}

RendererCell::~RendererCell()
{
  //link prev and next together
  if(m_prev != nullptr) m_prev->setNext(m_next);
  if(m_next != nullptr) m_next->setPrev(m_prev);
}

void RendererCell::setPrev(RendererCell* prev)
{
  m_prev = prev;
}

void RendererCell::setNext(RendererCell* next)
{
  m_next = next;
}

RendererCell* RendererCell::getPrev()
{
  return m_prev;
}

RendererCell* RendererCell::getNext()
{
  return m_next;
}

std::weak_ptr<GraphicObject> RendererCell::getObject()
{
  return m_object;
}

/* ##################################################################### */
/* ########## MODELRENDERER CLASS IMPLEMENTATION ####################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

ModelRenderer::ModelRenderer(std::shared_ptr<Model> model):
Renderer::Renderer(),
m_first(std::make_unique<RendererCell>(std::weak_ptr<GraphicObject>(), nullptr, nullptr)), //the head is special
m_model(model)
{}

ModelRenderer::~ModelRenderer()
{

  while(m_first->getNext() != nullptr)
  {
    auto ob = m_first->getNext()->getObject().lock(); 
    ob->unsetRenderCell();
  }

}

/** adds a graphic object to the renderer to be drawn */
void ModelRenderer::referGraphicObject(std::weak_ptr<GraphicObject> object)
{
  new RendererCell(object, m_first.get(), m_first->getNext());
}

/** renders all enlisted objects into the view */
void ModelRenderer::render(View& view, timeInterval_t elapsed)
{
  //for all cells
  RendererCell* current = m_first->getNext();

  if(current!=nullptr)
  {
    //apply the first shader of the list (OK since all have same shader)
    if(m_model != nullptr){
    auto ob = current->getObject().lock();
    m_model->bindToShader(ob->getShader());
    m_model->enable();}
  //draw until reaching the end
  while(current != nullptr)
  {
    auto ob = current->getObject().lock();
    ob->draw(view, elapsed);
    current = current->getNext();
  }
    if(m_model != nullptr){
    m_model->disable();}
  }
}

/* ##################################################################### */
/* ########## TEXTURERENDERER CLASS IMPLEMENTATION ##################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

/** creates an empty renderer */
TextureRenderer::TextureRenderer(std::shared_ptr<Texture> texture):
Renderer::Renderer(),
m_modelRenderers(),
m_texture(texture)
{}

/** deletes the renderer and all his subs renderer */
TextureRenderer::~TextureRenderer()
{

}

/** adds a graphic object to the renderer to be drawn */
void TextureRenderer::referGraphicObject(std::weak_ptr<GraphicObject> object)
{
  auto ob = object.lock();
  try {
    m_modelRenderers.at(ob->getModel())->referGraphicObject(object);
  }
  catch (std::out_of_range e){
    auto newRenderer = std::make_shared<ModelRenderer>(ob->getModel());
    m_modelRenderers.emplace(ob->getModel(), newRenderer);
    newRenderer->referGraphicObject(object);
  }
}

/** renders all enlisted objects into the view */
void TextureRenderer::render(View& view, timeInterval_t elapsed)
{

  if(m_texture != nullptr){m_texture->bind();}
  for(auto mr = m_modelRenderers.begin(); mr != m_modelRenderers.end(); ++mr)
  {
    mr->second->render(view, elapsed);
  }

}


/* ##################################################################### */
/* ########## SHADERRENDERER CLASS IMPLEMENTATION ###################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

/** creates an empty renderer */
ShaderRenderer::ShaderRenderer(std::shared_ptr<Shader> shader):
Renderer::Renderer(),
m_textureRenderers(),
m_shader(shader),
m_uniformViewLocation(0),
m_uniformProjectionLocation(0)
{
  if(m_shader != nullptr)
  {
    m_uniformViewLocation = m_shader->getUniformLocation(Shader::UNIFORM_VIEW_MATRIX_NAME);
    m_uniformProjectionLocation = m_shader->getUniformLocation(Shader::UNIFORM_PROJECTION_MATRIX_NAME);
  }
}

/** deletes the renderer and all his subs renderer */
ShaderRenderer::~ShaderRenderer()
{

}

/** adds a graphic object to the renderer to be drawn */
void ShaderRenderer::referGraphicObject(std::weak_ptr<GraphicObject> object)
{
  auto ob = object.lock();
  try {
    m_textureRenderers.at(ob->getTexture())->referGraphicObject(object);
  }
  catch (std::out_of_range e){
    auto newRenderer = std::make_shared<TextureRenderer>(ob->getTexture());
    m_textureRenderers.emplace(ob->getTexture(), newRenderer);
    newRenderer->referGraphicObject(object);
  }
}

/** renders all enlisted objects into the view */
void ShaderRenderer::render(View& view, timeInterval_t elapsed)
{
  if(m_shader != nullptr)
  {
    m_shader->use();
    //extract view and projection and send to shader
    m_shader->uniformMatrix4fv(m_uniformViewLocation, view.camera().lookAt());
    m_shader->uniformMatrix4fv(m_uniformProjectionLocation, view.projection().perspective());
  }

  for(auto tr = m_textureRenderers.begin(); tr != m_textureRenderers.end(); ++tr)
  {
    tr->second->render(view, elapsed);
  }
}

/* ##################################################################### */
/* ########## GLOBALRENDERER CLASS IMPLEMENTATION ###################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

/** creates an empty renderer */
GlobalRenderer::GlobalRenderer():
Renderer::Renderer(),
m_shaderRenderers()
{}

/** deletes the renderer and all his subs renderer */
GlobalRenderer::~GlobalRenderer()
{
}

/** adds a graphic object to the renderer to be drawn */
void GlobalRenderer::referGraphicObject(std::weak_ptr<GraphicObject> object)
{
  auto ob = object.lock();
  try {
    m_shaderRenderers.at(ob->getShader())->referGraphicObject(object);
  }
  catch (std::out_of_range e){
    auto newRenderer = std::make_shared<ShaderRenderer>(ob->getShader());
    m_shaderRenderers.emplace(ob->getShader(), newRenderer);
    newRenderer->referGraphicObject(object);
  }
}

/** renders all enlisted objects into the view */
void GlobalRenderer::render(View& view, timeInterval_t elapsed)
{
  for(auto sr = m_shaderRenderers.begin(); sr != m_shaderRenderers.end(); ++sr)
  {
    sr->second->render(view, elapsed);
  }

}


}//namespace nde
