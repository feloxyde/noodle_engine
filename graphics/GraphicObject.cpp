/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "GraphicObject.hpp"
#include "Renderer.hpp"

#include "Model.hpp"

namespace nde  {
/* ##################################################################### */
/* ########## GRAPHICOBJECT CLASS IMPLEMENTATION ####################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

GraphicObject::GraphicObject(std::shared_ptr<Shader> shader, std::shared_ptr<Model> model, std::shared_ptr<Texture> texture):
m_shader(shader), m_model(model), m_rendererCell(nullptr), m_texture(texture)
{
  if(shader !=  nullptr)
  {
    m_uniformModelLocation = shader->getUniformLocation(Shader::UNIFORM_MODEL_MATRIX_NAME);
  }
}

GraphicObject::~GraphicObject()
{
  unsetRenderCell();
}


void GraphicObject::setRendererCell(RendererCell* cell)
{
  unsetRenderCell();
  m_rendererCell = cell;
}

void GraphicObject::unsetRenderCell()
{
  if(m_rendererCell != nullptr)
  {
    delete m_rendererCell;
    m_rendererCell = nullptr;
  }
}

std::shared_ptr<Shader> GraphicObject::getShader()
{
  return m_shader;
}

std::shared_ptr<Model> GraphicObject::getModel()
{
  return m_model;
}

std::shared_ptr<Texture> GraphicObject::getTexture()
{
  return m_texture;
}


/* ##################################################################### */
/* ########## ATTACHEDGRAPHICOBJECT CLASS IMPLEMENTATION ############### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

AttachedGraphicObject::AttachedGraphicObject(std::shared_ptr<Shader> shader, std::shared_ptr<Model> model, std::shared_ptr<Texture> texture, std::weak_ptr<Transformable> attachPoint):
GraphicObject::GraphicObject(shader, model, texture), m_attachPoint(attachPoint)
{}

AttachedGraphicObject::~AttachedGraphicObject()
{}

void AttachedGraphicObject::draw(View& view, timeInterval_t elapsed)
{
  (void) elapsed; //we dont use time in this kind of object
  (void) view;
  //draw the model at the location of attachPoint
  auto ap = m_attachPoint.lock();

  m_shader->uniformMatrix4fv(
    m_uniformModelLocation,
    ap->asReferenceFrameTransformation());
  m_model->draw();
}

} //nde namespace
