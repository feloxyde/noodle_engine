/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_GL_H
#define NDE_GL_H


#include <string> //used for names
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <map> //used to keep resources unique
#include <memory>
#include "../utils/ndetypes.hpp"

/* glm includes; geometry */
#include <glm/glm.hpp>






namespace nde {


  /** this class represents a camera in opengl; and permit to the View transformation matrix
   *
   *  not that the target - position cant be collinear with up if you want to use rotateVertical
   *  if rotateVertically does an angle superior to +90 or -90 in reference to horizontal,
   *  comportment or rotateVertically is undetermined
   */
  class Camera {
  public:
  static angle_t const DEFAULT_MIN_VERTICAL_ANGLE;
  static angle_t const DEFAULT_MAX_VERTICAL_ANGLE;


  public:
  /** creates a Camera with a position, a target and a up vector */
  Camera(glm::vec3 const& position, glm::vec3 const& target, glm::vec3 const& up);
  /** deletes the camera */
  virtual ~Camera();

  /** defines the target of the camera, this target is absolute */
  void setTarget(glm::vec3 const& target);
  /** defines the orientation of the camera, this orientation is absolute*/
  void setOrientation(glm::vec3 const& orientation);
  /** defines the position of camera, this position is absolute*/
  void setPosition(glm::vec3 const& position);
  /** replaces the camera, keeping its orientation */
  void replace(glm::vec3 const& position);

  /** translates the camera, orientation stays the same*/
  void translate(glm::vec3 const& translation);
  /** rotates the camera around its position */
  void rotate(glm::vec3 const& axis, angle_t angle);
  /** rotates the camera horizontally (around the "up" axis) */
  void rotateHorizontally(angle_t angle);
  /** rotates the camera vertically, meaning it will only change the "up" chosen coordinate of orientation */
  void rotateVertically(angle_t angle);
  /** tilts the camera, meaning it will rotate around the orientation axis, changing the "up" and orientation vectors */
  void tilt(angle_t angle);
  /** translate only position */
  void translatePosition(glm::vec3 const& translation);
  /** rotates position around a point */
  void rotatePosition(glm::vec3 const& axis, glm::vec3 const& center, angle_t angle);

  /** moves the camera, but aligns the vector on horizontal orientation, keeps orientation */
  void moveForward(glm::vec3 const& movement);

  /** @return the position of the camera */
  glm::vec3 getPosition();
  /** @return the target of the camera */
  glm::vec3 getTarget();
  /** @return the up vector of the camera */
  glm::vec3 getUp();
  /** @return the look direction (target - position) of the camera */
  glm::vec3 getLookDirection();

  /** returns the view matrix for OpenGL rendering */
  glm::mat4 lookAt();

  private:

  glm::vec3 m_position; //position
  glm::vec3 m_target; //target
  glm::vec3 m_up; //the up vector
  angle_t m_minVertical; //angle not to go under for vertical orient
  angle_t m_maxVertical; //angle not to go over for vertical orient
};

class Projection {
public:

    /** creates a view */
    Projection(angle_t width, screenRatio_t ratio, length_t near, length_t far);
    /** deletes the view */
    virtual ~Projection();

    /** returns the projection matrix for OpenGL rendering */
    glm::mat4 perspective();

private:
  angle_t m_width;
  float m_ratio;
  float m_near;
  float m_far;
};


/** This class defines a wrapping for OpenGL vbos
*   this class is a templated buffer object
*
*/

class GLBufferObject {
public:
    /** unbind all buffers */
    static void bindZero();

public:

  /** creates a buffer empty */
  GLBufferObject();
  /** creates a buffer with data of size size */
  GLBufferObject(const void* data, vboSize_t size);
  /** destroys the VBO, frees the ressources */
  virtual ~GLBufferObject();

  /** binds the vertexbuffer in order to allow use */
  virtual void bind() = 0;

  virtual void enable() = 0;

  virtual void disable() = 0;

  /** raw OpenGL bind of the buffer */
  void bind(GLenum target);

  /** send datas to the buffer*/
  virtual void data(vboSize_t size, const void* data) = 0;
  /** raw Opengl data send to buffer */
  void data(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage);
  /** sends subdatas to the buffer */
  virtual void subData(vboSize_t offset, vboSize_t size, const void* data) = 0;
  /** raw OpenGL GLuint shaderID = 0subData send to buffer */
  void subData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid* data);

  /** returns the size of the vbo */
  virtual vboSize_t getSize();
  /** returns number of elements in the vbo */
  virtual vboSize_t elementCount();

protected:
    GLuint m_vboID;
    vboSize_t m_size;
};

/** this class is a wrapper for the OpenGL VertexBuffer0bject as VertexBuffer
*   it is a template taking two parameters : the type of the buffer, and the number of dimensions D of the buffer
*   number of dimensions is how much elements should be grouped by
*   E is the enum corresponding to the type
*/
template <typename T, size_t D, GLenum E>
class VertexBuffer : public GLBufferObject{

public:

  /** creates an empty buffer */
  VertexBuffer(GLuint shaderLocation = 0);
  /** creates a buffer with data of size size */
  VertexBuffer(const void* data, vboSize_t size, GLuint shaderLocation);
  /** destroys the VBO, frees the ressources */
  virtual ~VertexBuffer();

  /** binds the vertexbuffer in order to allow use */
  virtual void bind();
  
  virtual void enable();

  virtual void disable();
  /** send datas to the buffer*/
  virtual void data(vboSize_t size, const void* data);
  /** sends subdatas to the buffer */
  virtual void subData(vboSize_t offset, vboSize_t size, const void* data);

  /** returns the size of the vbo */
  virtual vboSize_t getSize();
  /** returns number of elements in the vbo */
  virtual vboSize_t elementCount();

private: 
    GLuint m_shaderLocation; //to bind with shader

};


/** this class is a wrapper for the OpenGL VertexBuffer0bject as ElementBuffer
*
*/
class ElementBuffer : public GLBufferObject{

public:

  /** creates an empty buffer */
  ElementBuffer();
  /** creates a buffer with data of size size */
  ElementBuffer(const void* data, vboSize_t size);
  /** destroys the VBO, frees the ressources */
  virtual ~ElementBuffer();

  /** binds the vertexbuffer in order to allow use */
  virtual void bind();

  virtual void enable(){};

  virtual void disable(){};


  /** send datas to the buffer*/
  virtual void data(vboSize_t size, const void* data);
  /** sends subdatas to the buffer */
  virtual void subData(vboSize_t offset, vboSize_t size, const void* data);

  /** returns the size of the vbo */
  virtual vboSize_t getSize();
  /** returns number of elements in the vbo */
  virtual vboSize_t elementCount();

};




/** this class is a wrapper for the OpenGL VertexArrayObject
*
*
*
*/
class VertexArrayObject {
public:
  /** unbinds all VAOs */
  static void bindZero();

public:

  /** builds a VAO */
  VertexArrayObject();

  /** releases ressources of the VAO */
  virtual ~VertexArrayObject();

  /** binds the VAO for use */
  void bind();

protected:
  GLuint m_vaoID;
};


/** This class defines a wrapping for OpenGL shader entity
*
*
*/
class Shader {

public:
  static std::string const EXTENSION_VERTEX; //the extension done for a shader vertex
  static std::string const EXTENSION_FRAGMENT; //the extension for a shader
  static std::string const NAME_SEPARATOR_SYMBOL; //the separator for a shader
  static std::string const SHADER_DIR_PATH; //path to shader folder

  static std::string const UNIFORM_MODEL_MATRIX_NAME; //shader global model matrix name
  static std::string const UNIFORM_VIEW_MATRIX_NAME; //shader global view matrix name
  static std::string const UNIFORM_PROJECTION_MATRIX_NAME; //shader global projection matrix name
  static std::string const UNIFORM_COLOR_VECTOR3_NAME; //shader global color vector name

  /** loads a shader by its name */
  static std::shared_ptr<Shader> loadShader(std::string shaderName);
  /** loads a shader by specifying a name different for the vertex and fragment shader */
  static std::shared_ptr<Shader> loadShader(std::string vertexName, std::string fragmentName);
  /** release all shaders stored */
  static void deleteShaders();
  /** release one shader, seems useless */
  static void deleteShader(std::shared_ptr<Shader> shader);
  /** gluseprogram(0) */
  static void useZero();
  /** returns a shaderID unused */
  static shaderID_t getNextShaderID();
  /** returns the "name" attribute of a shader */
  static std::string shaderGenericName(std::string vertexName, std::string fragmentName);

private:
static std::map<std::string, std::shared_ptr<Shader>> s_shaders; //a list of all shaders loaded using loadShader()
static shaderID_t s_nextID; //shader ids


public:
  /** Creating a shader, extensions are auto added in the name */
  Shader(std::string vertexName, std::string fragmentName);
  /** deletes a shader, and frees its gl ressources used */
  virtual ~Shader();
  /** returns the name of the Shader */
  std::string const getName() const;
  /** set shader to be used */
  virtual void use();
  /** returns the ID (nde) of the shader */
  shaderID_t getID();
  /** returns location of a shader param */
  virtual GLuint getUniformLocation(std::string uniformName);

  /** sends a uniform single 4fv matrix nto transposition*/
  virtual void uniformMatrix4fv(GLuint location, const glm::mat4& value);
  /** sends a uniform single 3f vector no transposition */
  virtual void uniformVector3f(GLuint location, const glm::vec3& value);

private:
  shaderID_t m_ID; //ID of the program
  GLuint m_programID; //GL id of the program
  std::string m_name; //name of the shader, concat of namevert and namefrag
};

/** This class defines a wrapper for OpenGL Texture entity,
*
* by texture is meant a simple texture, not mipmaped nor renderable
*
*/
class Texture {
public:

  static const std::string TEXTURE_DIR_PATH;

  /** loads a textures, ensures this texture is uniquely loaded */
  static std::shared_ptr<Texture> loadTexture(std::string textureName);
  /** delete all textures loaded and known */
  static void deleteTextures();
  /** delete one texture */
  static void deleteTexture(std::shared_ptr<Texture> texture);
  /** unbinds textures */
  static void bindZero();
  /** returns the next texture ID */
  static textureID_t getNextTextureID();


private:
  static std::map<std::string, std::shared_ptr<Texture>> s_textures; //to ensure only one instance of each texture is loaded
  static textureID_t s_nextID; //to give each texture an unique ID, which will be used for faster indexing, as for examples in renderers

public:
  /** creates an empty texture */
  Texture(textureSize_t width, textureSize_t height);
  /** create a Texture, loading it from a TGA file */
  Texture(std::string textureName);
  /** destroys the texture */
  virtual ~Texture();

  virtual GLuint glTextureID();

  /** binds a texture for use */
  virtual void bind();
  /** unbinds a texture after use */
  virtual void unbind();

private:
  textureID_t m_ID; // NDE ID of the texture (used mostly in shaders)
  GLuint m_textureID; //GL ID of the texture
  std::string m_name; //name of the texture
  textureSize_t m_width;
  textureSize_t m_height;
};


/** this class defines a wrapper for opengl framebuffer */
class FrameBuffer {

public:

  static void bindZero();

public:
  /** creates a FrameBuffer of the wanted dimensions */
  FrameBuffer(textureSize_t width, textureSize_t height);

  /* deletes the frame buffer */
  virtual ~FrameBuffer();

  /* bind it to render into */
  virtual void bind();

  /* return the texture rendered into */
  virtual Texture* renderTarget();

private:
  GLuint m_bufferID;
  GLuint m_renderBufferID;
  Texture* m_renderTarget;
  textureSize_t m_width;
  textureSize_t m_height;
};


} //nde namespace


#endif //NDG_GL_H

#ifndef NDE_GL_VERTEX_BUFFER_TEMPLATE

#include "ndeGLVertexBuffer.cpp"

#endif //NDG_GL_TEMPLATE_IMPLEM