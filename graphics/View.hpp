/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_VIEW_H
#define NDE_VIEW_H


#include <glm/glm.hpp>
#include "../utils/ndetypes.hpp"
#include "ndeGL.hpp"

namespace nde {

class Renderer;
/** represents a view, the combination of a Camera, a Projection and a FrameBuffer
* its objective is to render into a scene from a point of view into a frame buffer
*
*
*/
class View {
public:
  static glm::vec3 const DEFAULT_CAMERA_POSITION;
  static glm::vec3 const DEFAULT_CAMERA_LOOKAT;
  static glm::vec3 const DEFAULT_CAMERA_UP;

  static angle_t const DEFAULT_PROJECTION_WIDTH;
  static screenRatio_t const DEFAULT_PROJECTION_RATIO;
  static length_t const DEFAULT_PROJECTION_NEAR;
  static length_t const DEFAULT_PROJECTION_FAR;

public:

  /** creates a view 
  *   
  *   @param width, in pixels, of the view
  *   @param height, in pixels, of the view
  */
  View(textureSize_t width, textureSize_t height);

  /** deletes the view */
  virtual ~View();
  
  /** updates the view, will be called by the screen usually 
  *
  *   @param elapsed length of the current frame
  */
  virtual void update(timeInterval_t elapsed);

  /** @return camera of the view */
  Camera& camera();

  /** @return projection of the view */
  Projection& projection();

  /** set up the camera to watch a renderer
  *
  * @param renderer the renderer to watch
  */
  void watch(Renderer* renderer);

  /** sets up the rendertexture of the view to be used to render, on a screen for example */
  void use();

public: //#FIXME make this private later !
  Projection m_projection;
  Camera m_camera;
  FrameBuffer m_frame;
  Renderer* m_renderer;
};


}
#endif //NDE_VIEW_H
