/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "ndeGL.hpp"
#include "../utils/bmp.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <iostream>
#include <fstream>
#include <vector>

#define _USE_MATH_DEFINES
#include <math.h>

namespace nde {

/* ##################################################################### */
/* ########## CAMERA CLASS IMPLEMENTATION ############################## */
/* ##################################################################### */

/* PUBLIC STATIC MEMBERS */

angle_t const Camera::DEFAULT_MIN_VERTICAL_ANGLE = -M_PI/2 + 0.15;
angle_t const Camera::DEFAULT_MAX_VERTICAL_ANGLE = M_PI/2 - 0.15;

/* PUBLIC OBJECTS METHODS */

/** creates a Camera with a position, a target and a up vector */
Camera::Camera(glm::vec3 const& position, glm::vec3 const& target, glm::vec3 const& up):
m_position(position), m_target(target), m_up(up),
m_minVertical(DEFAULT_MIN_VERTICAL_ANGLE), m_maxVertical(DEFAULT_MAX_VERTICAL_ANGLE)
{}
/** deletes the camera */
Camera::~Camera()
{} //nothing to do

/** defines the target of the camera, this target is absolute */
void Camera::setTarget(glm::vec3 const& target)
{
  m_target = target;
}

/** defines the orientation of the camera, this orientation is absolute*/
void Camera::setOrientation(glm::vec3 const& orientation)
{
  m_target = orientation + m_position;
}

/** defines the position of camera, this position is absolute*/
void Camera::setPosition(glm::vec3 const& position)
{
  m_position = position;
}

/** replaces the camera, keeping its orientation */
void Camera::replace(glm::vec3 const& position)
{
  m_target += position - m_position;
  m_position = position;
}

/** translates the camera */
void Camera::translate(glm::vec3 const& translation)
{
  m_target += translation;
  m_position += translation;
}

/** rotates the camera around its center */
void Camera::rotate(glm::vec3 const& axis, angle_t angle)
{
  m_target -= m_position; //set rot around 0
  m_target = glm::rotate(m_target, angle, axis);
  m_target += m_position;
}

/** rotates the camera horizontally (around the "up" axis) */
void Camera::rotateHorizontally(angle_t angle)
{

  m_target -= m_position; //set rot around 0
  m_target = glm::rotate(m_target, angle, m_up);
  m_target += m_position;
  glm::vec3 orient =glm::normalize(m_target-m_position);
}

/** rotates the camera vertically, meaning it will only change the "up" chosen coordinate of orientation */
void Camera::rotateVertically(angle_t angle)
{

  //we need to prevent camera to go over 90° angle positive or negative
  glm::vec3 rotaxis = glm::cross(m_up, m_target-m_position);

  //glm::vec3 horizontal = glm::cross(m_up, rotaxis);
  angle_t angleNow = glm::orientedAngle(glm::normalize(m_target-m_position), glm::normalize(m_up), glm::normalize(-rotaxis)) -M_PI/2;
  angle_t finalAngle = angle;
if (angleNow  + angle > m_maxVertical) {
    finalAngle = m_maxVertical - angleNow;
  }

  else if (angleNow + angle < m_minVertical) {
    finalAngle = m_minVertical - angleNow;
  }

    m_target -= m_position; //set rot around 0
    m_target = glm::rotate(m_target, finalAngle, rotaxis);
    m_target += m_position;
}

/** tilts the camera, meaning it will rotate arond the orientation axis, changing the "up" and orientation vectors */
void Camera::tilt(angle_t angle) //basically rotates the up vector around orientation axis
{}
/** translate only position */
void Camera::translatePosition(glm::vec3 const& translation)
{}
/** rotates position around a point */
void Camera::rotatePosition(glm::vec3 const& axis, glm::vec3 const& center, angle_t angle)
{}

/** moves the camera, but aligns the vector on horizontal orientation, keeps orientation */
void Camera::moveForward(glm::vec3 const& movement)
{

  //compute rotation angle
  glm::vec3 orient = glm::normalize(m_target-m_position);
  //we dont take care of the [y] coordinate
  orient.y = 0.0f;
  angle_t alignAngle = glm::orientedAngle(glm::normalize(orient), glm::normalize(glm::vec3(0,0,1)), glm::normalize(m_up))/* + M_PI */;

  glm::vec3 refmove = glm::rotate(movement, -alignAngle, glm::normalize(m_up));
  //apply movement to camera
  this->translate(refmove);
}


glm::vec3 Camera::getPosition()
{
  return this->m_position;
}

glm::vec3 Camera::getTarget()
{
  return this->m_target;
}

glm::vec3 Camera::getUp()
{
  return this->m_up;
}

glm::vec3 Camera::getLookDirection()
{
  return this->getTarget() - this->getPosition();
}

/** returns the view matrix for OpenGL rendering */
glm::mat4 Camera::lookAt()
{
  return glm::lookAt(m_position,m_target,m_up);
}

/* ##################################################################### */
/* ########## PROJECTION CLASS IMPLEMENTATION ########################## */
/* ##################################################################### */



/** creates a view */
Projection::Projection(angle_t width, screenRatio_t ratio, length_t near, length_t far):
m_width(width), m_ratio(ratio), m_near(near), m_far(far)
{}

/** deletes the view */
Projection::~Projection()
{}

/** returns the projection matrix for OpenGL rendering */
glm::mat4 Projection::perspective()
{
  return glm::perspective(m_width, m_ratio, m_near, m_far);
}


/* ##################################################################### */
/* ########## GL BUFFER OBJECT CLASS IMPLEMENTATION #################### */
/* ##################################################################### */

/* PUBLIC STATIC METHODS */

/** unbind all buffers */
void GLBufferObject::bindZero()
{}

/* PUBLIC OBJECTS METHODS */

GLBufferObject::GLBufferObject():
m_vboID(0), m_size(0)
{
  glGenBuffers(1, &m_vboID);
}

/** creates a buffer with data of size size */
GLBufferObject::GLBufferObject(const void* data, vboSize_t size):
m_vboID(0), m_size(size)
{
    glGenBuffers(1, &m_vboID);
}

/** destroys the VBO, frees the ressources */
GLBufferObject::~GLBufferObject()
{
  glDeleteBuffers(1, &m_vboID);
}

/** raw OpenGL bind of the buffer */
void GLBufferObject::bind(GLenum target)
{
  glBindBuffer(target, m_vboID);
}

/** raw Opengl data send to buffer */
void GLBufferObject::data(GLenum target, GLsizeiptr size, const GLvoid* data, GLenum usage)
{
  glBufferData(target, size, data, usage);
}

/** raw OpenGL subData send to buffer */
void GLBufferObject::subData(GLenum target, GLintptr offset, GLsizeiptr size, const GLvoid* data)
{
  glBufferSubData(target, offset, size, data);
}


/** returns the size of the vbo */
 vboSize_t GLBufferObject::getSize()
 {
   return m_size;
 }

/** returns number of elements in the vbo */
vboSize_t GLBufferObject::elementCount()
{
  return m_size/sizeof(GL_FLOAT)/3;
}


/* ##################################################################### */
/* ########## ELEMENT BUFFER CLASS IMPLEMENTATION ###################### */
/* ##################################################################### */

/* PUBLIC OBJECTS METHODS */

ElementBuffer::ElementBuffer():
GLBufferObject()
{}

/** creates a buffer with data of size size */
ElementBuffer::ElementBuffer(const void* data, vboSize_t size):
GLBufferObject(data, size)
{
    this->bind();
    this->data(size, data);
}

/** destroys the VBO, frees the ressources */
ElementBuffer::~ElementBuffer()
{
  //nothing to do
}

/** binds the vertexbuffer in order to allow use */
void ElementBuffer::bind()
{
  GLBufferObject::bind(GL_ELEMENT_ARRAY_BUFFER);
}


/** send datas to the buffer*/
void ElementBuffer::data(vboSize_t size, const void* data)
{
  m_size = size;
  GLBufferObject::data(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
}


/** sends subdatas to the buffer */
void ElementBuffer::subData(vboSize_t offset, vboSize_t size, const void* data)
{
  GLBufferObject::subData(GL_ELEMENT_ARRAY_BUFFER, offset, size, data);
}

/** returns the size of the ElementBuffer*/
 vboSize_t ElementBuffer::getSize()
 {
   return m_size;
 }

/** returns number of elements in the ElementBuffer */
vboSize_t ElementBuffer::elementCount()
{
  return m_size/sizeof(GLuint);
}

/* ##################################################################### */
/* ########## VERTEX ARRAY OBJECT CLASS IMPLEMENTATION ################# */
/* ##################################################################### */


/* PUBLIC STATIC METHODS */

/** unbinds all VertexArrayObject */
void VertexArrayObject::bindZero()
{
  glBindVertexArray(0);
}

/* PUBLIC OBJECTS METHODS */

/** builds a VAO */
VertexArrayObject::VertexArrayObject()
{
  glGenVertexArrays(1, &m_vaoID);
}

/** releases ressources of the VAO */
VertexArrayObject::~VertexArrayObject()
{
  glDeleteVertexArrays(1, &m_vaoID);
}
#include "View.hpp"

  /** binds the VAO for use */
void VertexArrayObject::bind()
{
  glBindVertexArray(m_vaoID);
}


/* ##################################################################### */
/* ########## SHADER CLASS IMPLEMENTATION ############################## */
/* ##################################################################### */

/* PUBLIC STATIC MEMBERS */

std::string const Shader::EXTENSION_VERTEX = ".vert"; //the extension done for a shader vertex
std::string const Shader::EXTENSION_FRAGMENT = ".frag"; //the extension for a shader
std::string const Shader::NAME_SEPARATOR_SYMBOL = "|"; //the separator for a shader
std::string const Shader::SHADER_DIR_PATH = "resources/Shaders/";

std::string const Shader::UNIFORM_MODEL_MATRIX_NAME = "model";
std::string const Shader::UNIFORM_VIEW_MATRIX_NAME = "view";
std::string const Shader::UNIFORM_PROJECTION_MATRIX_NAME = "projection";
std::string const Shader::UNIFORM_COLOR_VECTOR3_NAME = "col";

/* PUBLIC STATIC METHODS */

/** loads a shader by its name */
std::shared_ptr<Shader> Shader::loadShader(std::string shaderName)
{
  return loadShader(shaderName, shaderName);
}

/** loads a shader by specifying a name different for the vertex and fragment shader */
std::shared_ptr<Shader> Shader::loadShader(std::string vertexName, std::string fragmentName)
{
  try { //if already existing we return it
    return s_shaders.at(shaderGenericName(vertexName, fragmentName));
  } catch (std::out_of_range e){ //else we create it and add it at map before returning
    auto s = std::make_shared<Shader>(vertexName, fragmentName);
    s_shaders.emplace(shaderGenericName(vertexName, fragmentName), s);
    return s;
  }
}

/** release all shaders stored */
void Shader::deleteShaders(){}

/** release one shader, seems useless */
void Shader::deleteShader(std::shared_ptr<Shader> shader){}

/** gluseprogram(0) */
void Shader::useZero(){}

/** returns a shaderID unused */
shaderID_t Shader::getNextShaderID()
{return s_nextID++;}

/** returns the "name" attribute of a shader */
std::string Shader::shaderGenericName(std::string vertexName, std::string fragmentName)
{
  return vertexName + NAME_SEPARATOR_SYMBOL + fragmentName;
#include "View.hpp"
}

/* PRIVATE STATIC MEMBERS */

std::map<std::string, std::shared_ptr<Shader>> Shader::s_shaders; //a list of all shaders loaded using loadShader()
shaderID_t Shader::s_nextID = 0; //shader ids

/* PUBLIC OBJECTS METHODS */

/** Creating a shader, extensions are auto added in the name */
Shader::Shader(std::string vertexName, std::string fragmentName):
m_ID(Shader::getNextShaderID()), m_name(shaderGenericName(vertexName, fragmentName))
{
  /* following code from opengl-tutorial.org */
  //creates the shaders
  GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
  GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

  // Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(SHADER_DIR_PATH+vertexName+EXTENSION_VERTEX, std::ios::in);
	if(VertexShaderStream.is_open()){
		std::string Line = "";
		while(getline(VertexShaderStream, Line))
			VertexShaderCode += "\n" + Line;
		VertexShaderStream.close();
	}else{
		std::cout << "problem while opening vertex shader " << vertexName << std::endl;
    exit(EXIT_FAILURE);
    getchar();
  }

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(SHADER_DIR_PATH+fragmentName+EXTENSION_FRAGMENT, std::ios::in);
	if(FragmentShaderStream.is_open()){
		std::string Line = "";
		while(getline(FragmentShaderStream, Line))
			FragmentShaderCode += "\n" + Line;
		FragmentShaderStream.close();
	}	else{
  		std::cout << "problem while opening fragment shader " << fragmentName << std::endl;
      getchar();
    }


  GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	char const * VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}

  // Compile Fragment Shader
	char const * FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

  // Link the program
  m_programID = glCreateProgram();
	glAttachShader(m_programID, VertexShaderID);
	glAttachShader(m_programID, FragmentShaderID);
	glLinkProgram(m_programID);

	// Check the program
	glGetProgramiv(m_programID, GL_LINK_STATUS, &Result);
	glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if ( InfoLogLength > 0 ){
		std::vector<char> ProgramErrorMessage(InfoLogLength+1);
		glGetProgramInfoLog(m_programID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	glDetachShader(m_programID, VertexShaderID);
	glDetachShader(m_programID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);
}

/** deletes a shader, and frees its gl ressources used */
Shader::~Shader()
{}

/** returns the name of the Shader */
std::string const Shader::getName() const
{return m_name;}

/** set shader to be used */
void Shader::use()
{glUseProgram(m_programID);}
/** returns the ID (nde) of the shader */
shaderID_t Shader::getID()
{return m_ID;}

GLuint Shader::getUniformLocation(std::string uniformName)
{
  return glGetUniformLocation(m_programID, uniformName.c_str());
}

/** sends a uniform single 4fv matrix nto transposition*/
void Shader::uniformMatrix4fv(GLuint location, const glm::mat4& value)
{
  glUniformMatrix4fv(location, 1, GL_FALSE, &value[0][0]);
}

void Shader::uniformVector3f(GLuint location, const glm::vec3& value)
{
  glUniform3fv(location, 1, glm::value_ptr(value));
}

/* ##################################################################### */
/* ########## TEXTURE CLASS IMPLEMENTATION ############################# */
/* ##################################################################### */


const std::string Texture::TEXTURE_DIR_PATH = "resources/Textures/";

/* PUBLIC STATIC METHODS */

/** loads a textures, ensures this texture is uniquely loaded */
std::shared_ptr<Texture> Texture::loadTexture(std::string textureName)
{
  try { //if already existing we return it
    return s_textures.at(textureName);
  } catch (std::out_of_range e){ //else we create it and add it at map before returning
    auto t = std::make_shared<Texture>(textureName);
    s_textures.emplace(textureName, t);
    return t;
  }
}

/** delete all textures loaded and known */
void Texture::deleteTextures()
{
  //#FIXME add release all here
}

/** delete one texture */
void Texture::deleteTexture(std::shared_ptr<Texture> texture)
{
  //FIXME add release one here
}

/** returns the next texture ID */
textureID_t Texture::getNextTextureID()
{
  return s_nextID++;
}

/* PRIVATE STATIC MEMBERS */

std::map<std::string, std::shared_ptr<Texture>> Texture::s_textures; //to ensure only one instance of each texture is loaded
textureID_t Texture::s_nextID = 0; //to give each texture an unique ID, which will be used for faster indexing, as for examples in renderers

/* PUBLIC OBJECT METHODS */

/** creates an empty texture */
Texture::Texture(textureSize_t width, textureSize_t height):
m_ID(getNextTextureID()), m_textureID(0),
m_name(""),
m_width(width), m_height(height)
{
  //generating
  glGenTextures(1, &m_textureID);
  //bind this
  glBindTexture(GL_TEXTURE_2D, m_textureID);

  //empty image
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

  //poor base filtering
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

}

/** create a Texture, loading it from a file */
Texture::Texture(std::string textureName):
m_ID(getNextTextureID()), m_textureID(0),
m_name(textureName),
m_width(0), m_height(0)
{
  std::unique_ptr<BMPImage> teximage = loadBMPImage(Texture::TEXTURE_DIR_PATH+textureName+".bmp");
  if (!teximage)
  {
    std::cout << "failed to load texture " + textureName + ", exiting." << std::endl; 
    exit(EXIT_FAILURE);
  }
  
  m_width = teximage->m_width;
  m_height = teximage->m_height;

  //generating
  glGenTextures(1, &m_textureID);
  //bind this
  glBindTexture(GL_TEXTURE_2D, m_textureID);

  GLint a;
  glGetIntegerv(GL_UNPACK_SKIP_ROWS, &a);

  //poor base filtering
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, m_width, m_height, 0, GL_BGR, GL_UNSIGNED_BYTE, teximage.get()->m_buffer);


  //mipmapping
  glGenerateMipmap(GL_TEXTURE_2D);

}

Texture::~Texture()
{
//#FIXME properly destroy the texture
}

/** binds a texture for use */
void Texture::bind()
{
  glBindTexture(GL_TEXTURE_2D, m_textureID);
}
/** unbinds a texture after use */
void Texture::unbind()
{
  glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint Texture::glTextureID()
{
  return m_textureID;
}


/* ##################################################################### */
/* ########## FRAMEBUFFER CLASS IMPLEMENTATION ######################### */
/* ##################################################################### */

/* PUBLIC STATIC METHODS */

void FrameBuffer::bindZero()
{
  glBindFramebuffer(GL_FRAMEBUFFER,0);
}

/* PUBLIC OBJECT METHODS */

/** creates a FrameBuffer of the wanted dimensions */
FrameBuffer::FrameBuffer(textureSize_t width, textureSize_t height):
m_bufferID(0), m_renderBufferID(0),
m_renderTarget(nullptr),
m_width(width), m_height(height)
{
  //creating frame buffer
  glGenFramebuffers(1, &m_bufferID);
  glBindFramebuffer(GL_FRAMEBUFFER, m_bufferID);

  //setting up the texture
  m_renderTarget = new Texture(m_width, m_height);
  //remember here texture is already binded

  //depth buffer
  glGenRenderbuffers(1, &m_renderBufferID);
  glBindRenderbuffer(GL_RENDERBUFFER, m_renderBufferID);
  glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, m_width, m_height);
  glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_renderBufferID);

  //configuration, render to color attachment 0
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_renderTarget->glTextureID() ,0);

  GLenum drawBuffer[1] = {GL_COLOR_ATTACHMENT0};

  glDrawBuffers(1, drawBuffer);

  //check if OK
  if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
  {
    std::cout << "an error occured while creating framebuffer" << std::endl;
    exit(EXIT_FAILURE);
  }

}

/* deletes the frame buffer */
FrameBuffer::~FrameBuffer()
{
  //#FIXME add ressources liberation here
}

/* bind it to render into */
void FrameBuffer::bind()
{
  glBindFramebuffer(GL_FRAMEBUFFER, m_bufferID);
  glViewport(0, 0, m_width, m_height);

}

/* return the texture rendered into */
Texture* FrameBuffer::renderTarget()
{
  return m_renderTarget;
}

GLuint m_bufferID;
GLuint m_renderBufferID;
Texture* m_renderTarget;
textureSize_t m_width;
textureSize_t m_height;

} //nde namespace
