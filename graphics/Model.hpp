/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_MODEL_H
#define NDE_MODEL_H


#include <string>
#include <map>
#include "../utils/ndetypes.hpp"
#include "ndeGL.hpp"
#include <glm/glm.hpp>
#include <vector>
#include <memory>

namespace nde {

typedef std::vector<std::shared_ptr<GLBufferObject>>GLBufferObjectVector_t;

//for the vbo
#ifndef BUFFER_OFFSET
#define BUFFER_OFFSET(offset) ((char*)NULL + (offset))
#endif

/** this class defines a really simple model, with only a buffer of vertices and a shader
*   which is basically the tiniest drawable entity after a "full shader" model
*
*
**/
class Model {
public: 
  static const GLuint VERTEX_COORDINATES_SHADER_ATTRIB;
  static const GLuint VERTEX_TEXTURE_COORDINATES_SHADER_ATTRIB;
  static const GLuint VERTEX_NORMAL_SHADER_ATTRIB;
public:
//here addm a mechanism to load models uniquely  
  
  /** loads a model by its name NOT YET IMPLEMENTED*/
  static std::shared_ptr<Model> loadModel(std::string modelName);
  /** deletes all loaded models */
  static void deleteModels();
  /** returns the next modelID */
  static modelID_t getNextModelID();

  /** creates an indexed model with textures coords
  *   @param vertices raw buffer of vertices, 3-GLFLOAT per vertex
  *   @param size size (in bytes) of the vertices buffer
  *   @param textureCoords raw buffer of texture coordds, 2-GLFLoats per coord
  *   @param sizeTex size (in bytes) of the coord buffer
  *   @param indexes raw buffer of indexes, 1 GLINT per index
  *   @param indexSize size (in bytes) of the indexes buffer
  */
  static std::shared_ptr<Model> createTexturedIndexedModel(
    const void* vertices, 
    vboSize_t size,  
    const void* textureCoords, 
    vboSize_t sizeTex, 
    const void* indexes, 
    vboSize_t indexSize);

  /** creates an indexed model with a color
  *   @param vertices raw buffer of vertices, 3-GLFLOAT per vertex
  *   @param size size (in bytes) of the vertices buffer
  *   @param indexes raw buffer of indexes, 1 GLINT per index
  *   @param indexSize size (in bytes) of the indexes buffer
  *   @param color the color of the model
  */
  static std::shared_ptr<Model> createMonochromeIndexedModel(
    const void* vertices, 
    vboSize_t size, 
    const void* indexes, 
    vboSize_t indexSize, 
    glm::vec3& color);

    /** creates an indexed model with textures coords and vertices normals
  *   @param vertices raw buffer of vertices, 3-GLFLOAT per vertex
  *   @param size size (in bytes) of the vertices buffer
  *   @param textureCoords raw buffer of texture coordds, 2-GLFLoats per coord
  *   @param sizeTex size (in bytes) of the coord buffer
  *   @param normals raw buffer of normals, 3-GLfloats per normal
  *   @param sizeNorm sizr (in bytes) of the normals buffer
  *   @param indexes raw buffer of indexes, 1 GLINT per index
  *   @param indexSize size (in bytes) of the indexes buffer
  */
  static std::shared_ptr<Model> createTexturedNormalizedIndexedModel(
    const void* vertices, 
    vboSize_t size,  
    const void* textureCoords, 
    vboSize_t sizeTex,
    const void* normals,
    vboSize_t sizeNorm,
    const void* indexes, 
    vboSize_t indexSize);

protected:
static std::map<std::string, std::shared_ptr<Model>> s_models; //a list of all shaders loaded using loadModel()

private:
static modelID_t s_nextID; //shader ids

public:
  /** creating a model from a vertices array and a shader */
  Model(const void* vertices, vboSize_t size);
  /** creating a model from a filename */
  Model(std::string modelName);
  /** deletes a model and frees its ressources */
  virtual ~Model();

  void addVerticeData(std::shared_ptr<GLBufferObject> data);

  /** prepare model for use */
  virtual void enable();

  /** discard model from use */
  virtual void disable();

  /** draws the model using model matrix */
  virtual void draw();
  /** draws the model using m v  p matrixs */
  //virtual void draw(glm::mat4& model, glm::mat4& view, glm::mat4& projection);

  /** returns model ID */
  modelID_t ID();

  virtual void bindToShader(std::shared_ptr<Shader> shader);

protected:
  GLBufferObjectVector_t m_verticesData;
  //GLuint m_uniformModelLocation;

private:
  modelID_t m_ID;
};


/** This class defines a really simple model, with only a buffer of vertices and a shader, which is
* basicallu the tiniest drawable entity after a "full shader" model. It also uses indexing for drawing
*
*
*/
class IndexedModel : public Model{

public:
  /** creating a model from vertices, indexes and shader */
  IndexedModel(const void* vertices, vboSize_t size, const void* indexes, vboSize_t indexCount);
  /** creating a model from a filename */
  IndexedModel(std::string modelName);
  /** deletes a model and frees its ressources */
  virtual ~IndexedModel();
  /** prepare model for use */
  virtual void enable();

  /** draws the model using model matrix , have to be "enabled" before this */
  virtual void draw();

protected:
    ElementBuffer m_indexes;

};

/** this class represents a model defined by a color and a vertex buffer, all indexed */
class MonochromeIndexedModel : public IndexedModel {

public:
  /** creating a model from vertices, indexes and shader and a color */
  MonochromeIndexedModel(const void* vertices, vboSize_t size, const void* indexes, vboSize_t indexSize, glm::vec3& color);
  /** creating a model from a filename */
  MonochromeIndexedModel(std::string modelName);
  /** deletes a model and frees its ressources */
  virtual ~MonochromeIndexedModel();
  /** prepare model for use */
  virtual void enable();

  /** refresh uniform location and set new shader */
  virtual void bindToShader(std::shared_ptr<Shader> shader);
  
private:
  glm::vec3 m_color;
  GLuint m_uniformColorLocation;
};


}//nde namespace


#endif //NDG_MODEL_H
