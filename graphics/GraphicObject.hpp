/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_GRAPHIC_OBJECT_H
#define NDE_GRAPHIC_OBJECT_H
#include "ndeGL.hpp"
#include "../utils/geometry.hpp"
#include "View.hpp"
#include <memory>

namespace nde {

class RendererCell; //forward declaration for the RendererCell element to use with renderers
class Model;

/** A graphic object is an association of a shader, a model and a texture */
class GraphicObject {

public:
  /** Creates a graphic object
  *   
  *   @param Shader the shader of the object, can not be null
  *   @param the model (mesh) of the object, can not be null
  *   @param the texture of the object, can be null
  */
  GraphicObject(std::shared_ptr<Shader> Shader, std::shared_ptr<Model> model, std::shared_ptr<Texture> texture);
  
  /** destroys the graphic object */
  virtual ~GraphicObject();
  
  /** draws the graphic object, is called by the renderer, DO NOT CALL directly
  *   this method is meant to be overriden when defining a graphic object
  *
  *   @param view (passed by renderer) the view where the object is drawn
  *   @param elapsed (passed by renderer) the current frame duration
  **/
  virtual void draw(View& view, timeInterval_t elapsed) = 0;

  /** setup renderer cell of the graphic object, DO NOT CALL */
  void setRendererCell(RendererCell* cell);

  /** removes the renderer cell from the object, DO NOT CALL */
  void unsetRenderCell();

  /** @return the shader of the object */
  virtual std::shared_ptr<Shader> getShader();
 
  /** @return the model (mesh) of the object */
  virtual std::shared_ptr<Model> getModel();

  /** @return the texture of the object */
  virtual std::shared_ptr<Texture> getTexture();

protected:
  std::shared_ptr<Shader> m_shader; //do not delete
  GLuint m_uniformModelLocation;

  std::shared_ptr<Model> m_model; //do not delete
  std::shared_ptr<Texture> m_texture; //do not delete

private:
  RendererCell* m_rendererCell; //delete
};

/** a graphic object attached to a transformable */
class AttachedGraphicObject : public GraphicObject {

public:
  /** Creates a graphic object
  *   
  *   @param Shader the shader of the object, can not be null
  *   @param the model (mesh) of the object, can not be null
  *   @param the texture of the object, can be null
  *   @param attachPoint anchor of the object, the object will automatically setup his world position matrix 
  *          to match attachPoint world position when rendering itself 
  */
  AttachedGraphicObject(std::shared_ptr<Shader> shader, std::shared_ptr<Model> model, std::shared_ptr<Texture> texture, std::weak_ptr<Transformable> attachPoint);
  
  /** deletes the graphic object */
  virtual ~AttachedGraphicObject();

  /** draws the attached graphic object, automatically setting up its world position to match the attatchPoint position. DO NOT CALL*/
  virtual void draw(View& view, timeInterval_t elapsed);

public:
  std::weak_ptr<Transformable> m_attachPoint;
};

} //nde namespace

#endif //NDE_GRAPHIC_OBJECT_H
