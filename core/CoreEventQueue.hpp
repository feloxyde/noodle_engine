/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_COREEVENTQUEUE_HPP
#define NDE_COREEVENTQUEUE_HPP
#include <memory>
#include <queue>
#include <vector>
#include "../utils/ndetypes.hpp"
#include "CoreEvent.hpp"


namespace nde {

class AbstractCoreEventListener;

/** Comparison tool used for pointer ordered storage purpose */
class CoreEventPTRComparison
{
public:
  CoreEventPTRComparison() {}

  bool operator() (const std::shared_ptr<CoreEvent>& lhs, const std::shared_ptr<CoreEvent>& rhs) const
  {
    return (lhs->getTimePoint() > rhs->getTimePoint());
  }
};

/** EventQueue used to process CoreEvent in chronological order.*/
class CoreEventQueue 
{
public:
    /** creates a CoreEventQueue */
    CoreEventQueue();
    /** deletes a CoreEventQueue */
    virtual ~CoreEventQueue();

    /** Add an event listener to the queue
    *
    * @param listener the listener to add
    */
    void addEventListener(std::shared_ptr<AbstractCoreEventListener> listener);

    /** Removes an event listener from the queue
    *
    * @param listener the listener to remove 
    *
    */
    void removeEventListener(std::shared_ptr<AbstractCoreEventListener> listener);

    /** Process event up to the specified timepoint 
    *
    * @param timePoint the fence timePoint, all events with timepoint =< to this timepoint will be processed.
    */
    void flushTo(timePoint_t timePoint);

    /** Add an event to the queue. 
    *
    * @param the event to add 
    */
    void addEvent(std::shared_ptr<CoreEvent> event);
private:
    std::priority_queue <
               std::shared_ptr<CoreEvent>, 
               std::vector<std::shared_ptr<CoreEvent>>,
               CoreEventPTRComparison
               > m_events;
    std::vector<std::shared_ptr<AbstractCoreEventListener>> m_listeners;
};



} //namespace nde




#endif //NDE_COREEVENTQUEUE_HPP