/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_COREEVENTLISTENER_HPP
#define NDE_COREEVENTLISTENER_HPP
#include "../utils/ndetypes.hpp"
#include <memory>
#include "CoreEvent.hpp"

namespace nde 
{

/** Overall abstraction of a CoreEvent listener. Used for storage and polymophism purpose over the template 
*   Do not instanciate or derive from, do it from CoreEventListener instead.
*
**/
class AbstractCoreEventListener
{
public: 
    AbstractCoreEventListener();
    virtual ~AbstractCoreEventListener();

    virtual void reactTo(std::shared_ptr<CoreEvent> event) = 0;
};

/** Core event listener. It will listen on events of type T, or 
*   inheriting from T
*/
template <typename T>
class CoreEventListener: public AbstractCoreEventListener
{
public:
    /** Creates a core event listener */
    CoreEventListener();

    /* deletes the object */
    virtual ~CoreEventListener();

    /** General reaction method, do not override!*/
    virtual void reactTo(std::shared_ptr<CoreEvent> event);

    /** This method will be called when an event is sent to the EventListener.
    *   You have to implement it when deriving from the CoreEventListener
    *   
    *   @param event the event to react to
    */
    virtual void reaction(std::shared_ptr<T> event) = 0;
};

/** A special listener meant to be added to the EventQueue when needing to propagate 
* TargetedCoreEvents to their targets.
*/
class TargetedCoreEventBouncer: public CoreEventListener<TargetedCoreEvent>
{
public:
    /** Creates a TargetedCoreEventBouncer() */
    TargetedCoreEventBouncer();
    virtual ~TargetedCoreEventBouncer();

    virtual void reaction(std::shared_ptr<TargetedCoreEvent> event);
};

} //namespace nde

#endif //NDE_COREEVENTLISTENER_HPP



#ifndef NDE_COREEVENTLISTENER_TEMPLATE

#include "CoreEventListenerTemplate.cpp"

#endif //NDE_COREEVENTLISTENER_TEMPLATE