/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "GameObject.hpp"
#include "CoreEventListener.hpp"
#include <algorithm>
#include "Game.hpp"
#include "../physics/Collider.hpp"
#include "../physics/PhysicBody.hpp"
#include "../graphics/Renderer.hpp"
#include <iostream>

namespace nde {


GameObject::GameObject():
m_body(),
m_isAlive(true),
m_eventListeners(),
m_graphicObjects()
{

}

GameObject::~GameObject()
{
}

void GameObject::onFrameBegin(timeInterval_t elapsed)
{
    //nothing to do here
}

void GameObject::onFrameEnd(timeInterval_t elapsed)
{
    //nothing to do here
}


void GameObject::onEvent(std::shared_ptr<CoreEvent> event)
{
 
    for(auto listn = m_eventListeners.begin(); listn != m_eventListeners.end(); ++listn)
    {
        
        (*listn)->reactTo(event);
    }    
}

bool GameObject::isAlive()
{
    return m_isAlive;
}

void GameObject::kill()
{
    m_isAlive = false;
}

void GameObject::addEventListener(std::shared_ptr<AbstractCoreEventListener> listener)
{
    m_eventListeners.push_back(listener);
}

void GameObject::removeEventListener(std::shared_ptr<AbstractCoreEventListener> listener)
{
    auto listn = std::find(m_eventListeners.begin(), m_eventListeners.end(), listener);
    if (listn != m_eventListeners.end())
    {
        m_eventListeners.erase(listn);
    }
}

std::shared_ptr<PhysicBody> GameObject::getPhysicBody()
{
    return m_body;
}

std::vector<std::shared_ptr<GraphicObject>>& GameObject::getGraphicObjects()
{
    return m_graphicObjects;
}

void GameObject::registerToGame() {
    this->m_body->setOwner(this->weak_from_this());
    Game::registerGameObject(this->shared_from_this()); 
}

void GameObject::setPhysicBody(std::shared_ptr<PhysicBody> body)
{
    m_body  = body;
    m_body->setOwner(this->shared_from_this());
}
    
} //namespace nde