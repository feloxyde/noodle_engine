/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CoreEventQueue.hpp"
#include <algorithm>
#include "CoreEventListener.hpp"

namespace nde {

CoreEventQueue::CoreEventQueue():
m_events()
{}

CoreEventQueue::~CoreEventQueue()
{
    //nothing to do
}

void CoreEventQueue::addEventListener(std::shared_ptr<AbstractCoreEventListener> listener)
{
    m_listeners.push_back(listener);
}

void CoreEventQueue::removeEventListener(std::shared_ptr<AbstractCoreEventListener> listener)
{
    auto pos = std::find(m_listeners.begin(), m_listeners.end(), listener);
    if(pos != m_listeners.end())
    {
        m_listeners.erase(pos);
    }
}

void CoreEventQueue::flushTo(timePoint_t timePoint)
{
    while(m_events.size() != 0 && m_events.top()->getTimePoint() <= timePoint)
    {
        auto event = m_events.top();
        for(auto listn = m_listeners.begin(); listn != m_listeners.end(); ++listn)
        {
            (*listn)->reactTo(event);
        }

        m_events.pop();
    }
}

void CoreEventQueue::addEvent(std::shared_ptr<CoreEvent> event)
{
    m_events.push(event);
}

} //namespace NDE