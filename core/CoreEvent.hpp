/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_COREEVENT_HPP
#define NDE_COREEVENT_HPP
#include "../utils/ndetypes.hpp"
#include <string>
#include <memory>
#include <vector>


namespace nde 
{

class GameObject;

/** this class is the top class of game engine event, any event shall
* inherit from it, directly or indirectly.
*
*/
class CoreEvent : public std::enable_shared_from_this<CoreEvent>
{
public:
    /** Create an event a the set time, events are handled into chronological order
    *   Please note that events shall be created using make_shared.
    *
    *   @param timePoint the time point of the event is wanted to occur.
    */
    CoreEvent(timePoint_t timePoint);

    /** deletes an event. should not be called. */
    virtual ~CoreEvent();

    /** Returns the timepoint of the event
    *
    *   @return the timepoint of the event.
    */
    timePoint_t getTimePoint() const;
    
    /** returns true if the event is, or inherits from T class type
    *
    *   @return true if the event is, or inherits from T class type
    */
    template <typename T>
    bool isA();

    /** returns a pointer to the event on which an attempt of dynamic_cast has been done.
    *
    *   @return a pointer to this if this->isA<T>() is true, a null pointer otherwise.
    */
    template <typename T>
    std::shared_ptr<T> as();
    
    bool operator <(CoreEvent const& B)
    {
        return this->getTimePoint() < B.getTimePoint();
    }

    bool operator >(CoreEvent const& B)
    {
        return this->getTimePoint() > B.getTimePoint();
    }

    bool operator ==(CoreEvent const& B)
    {
        return this->getTimePoint() == B.getTimePoint();
    }

private: 
    timePoint_t m_timePoint;
};

/** A core event with targets, meant to be passed to GameObjects when processed.
*
*
*/
class TargetedCoreEvent : public CoreEvent
{
public:
    /** Creates a TargetedCoreEvent at the set timepoint. Please note that events 
    * shall be created using make_shared
    *
    *   @param timePoint the time point to set the event at.
    */
    TargetedCoreEvent(timePoint_t timePoint);

    /** the event, should not be called*/
    virtual ~TargetedCoreEvent();

    /** adds a target to the event. Upon processing the event, every target will be notified by the event.
    *
    *   @param target the target to add
    */
    void addTarget(std::shared_ptr<GameObject> target);

    /** @return the list of targets of the event */
    std::vector<std::shared_ptr<GameObject>> const& getTargets();

private: 

    std::vector<std::shared_ptr<GameObject>> m_targets;

};


} //namespace nde

#endif //NDE_COREEVENT_HPP

#ifndef NDE_COREEVENTCASTS_TEMPLATE

#include "CoreEventCasts.cpp"

#endif //NDE_COREEVENTCASTS_TEMPLATE