/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CoreEvent.hpp"

namespace nde 
{

CoreEvent::CoreEvent(timePoint_t timePoint):
m_timePoint(timePoint)
{

}

CoreEvent::~CoreEvent()
{

}

timePoint_t CoreEvent::getTimePoint() const
{
    return m_timePoint;
}


TargetedCoreEvent::TargetedCoreEvent(timePoint_t timePoint):
CoreEvent::CoreEvent(timePoint),
m_targets()
{
    //nothing to do
}

TargetedCoreEvent::~TargetedCoreEvent()
{
    //nothing to do
}


void TargetedCoreEvent::addTarget(std::shared_ptr<GameObject> target)
{
    m_targets.push_back(target);
}

std::vector<std::shared_ptr<GameObject>> const& TargetedCoreEvent::getTargets()
{
    return m_targets;
}


} //namespace nde