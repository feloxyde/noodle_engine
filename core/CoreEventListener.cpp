/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CoreEventListener.hpp"
#include "GameObject.hpp"
namespace nde 
{


AbstractCoreEventListener::AbstractCoreEventListener()
{}

AbstractCoreEventListener::~AbstractCoreEventListener()
{}


TargetedCoreEventBouncer::TargetedCoreEventBouncer():
CoreEventListener<TargetedCoreEvent>::CoreEventListener()
{
    //nothing to do
}


TargetedCoreEventBouncer::~TargetedCoreEventBouncer()
{
    //nothing to do
}

void  TargetedCoreEventBouncer::reaction(std::shared_ptr<TargetedCoreEvent> event)
{
    auto targets = event->getTargets();

    for(auto tg = targets.begin(); tg != targets.end(); ++tg)
    {
        (*tg)->onEvent(event);
    }

}


} //namespace nde