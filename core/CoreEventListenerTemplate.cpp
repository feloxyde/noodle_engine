/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#define NDE_COREEVENTLISTENER_TEMPLATE
#include "CoreEventListener.hpp"


namespace nde 
{

template <typename T>
CoreEventListener<T>::CoreEventListener():
AbstractCoreEventListener::AbstractCoreEventListener()
{
    //nothing to do
}

template <typename T>
CoreEventListener<T>::~CoreEventListener()
{
    //nothing to do
}

template <typename T>
void CoreEventListener<T>::reactTo(std::shared_ptr<CoreEvent> event)
{
    std::shared_ptr<T> ptr = event->as<T>();
    if(ptr != nullptr)
    {
        this->reaction(ptr);
    }
}



} //namespace nde