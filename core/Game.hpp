/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_GAME_HPP
#define NDE_GAME_HPP

#include <memory>
#include "../utils/ndetypes.hpp"
#include <list>

namespace nde {

class GameObject;
class Collider;
class Renderer;
class SimpleScreen;
class View;
class Controller;
class CoreEventQueue;
class CoreEvent;

/** The instance of the engine. Holds most of resources and the main loop
* Only static methods should be called from a game developement perspective.
*/
class Game
{

public: 

    /** Initializes the engine, by instanciating a game and setting it up for running.
    *   This method should be the first one you call sequentially when using the engine,
    *   usually at the top of your main() function.
    *
    * @param exitKeyCode GLFW keycode of the input key for exiting the game
    * @param toggleSimulationKeyCode GLFW keycode of the input key to start/stop simulation of the game 
    */
    static void init(int exitKeyCode, int toggleSimulationKeyCode);

    /** Runs the main loop of the game. Call this to start the game, once all your additional setup is done.
    * This method returns when the game loop exits, after pressing the exit key set in init, or calling requireExit()
    */
    static void runMainLoop();

    /** @return the end of current the simulation frame */
    static timePoint_t currentFrameEnd();

    /** @return the beginning of the current simulation frame */
    static timePoint_t currentFrameBegin();

    /** Adds a game object to the game. Please do not use and call GameObject::registerToGame() instead */
    static void registerGameObject(std::shared_ptr<GameObject> object);

    /** adds an event to the game event queue 
    *   
    *   @param event the event to add to event queue
    */
    static void registerCoreEvent(std::shared_ptr<CoreEvent> event);
    
    /** @return the game collider */
    static std::shared_ptr<Collider> getGameCollider();

    /** @return the game top level renderer */
    static std::shared_ptr<Renderer> getGameRenderer();

    /** @return the game screen */
    static std::shared_ptr<SimpleScreen> getGameScreen();

    /** @return the game controller */
    static std::shared_ptr<Controller> getGameController();

    /** @return the game event queue */
    static std::shared_ptr<CoreEventQueue> getGameEventQueue(); 
    
    /** requires the game to exit at the next main loop iteration */
    static void requireExit();

    /** activates / desactivate game simulation */
    static void simulate(bool simulate);

private:  
    static std::unique_ptr<Game> s_gameInstance;

public:
    Game(int exitKeyCode, int toggleSimulationKeyCode);
    virtual ~Game();

    void addGameObject(std::shared_ptr<GameObject> object);
    void removeGameObject(std::shared_ptr<GameObject> object);
    void addCoreEvent(std::shared_ptr<CoreEvent> event);

    void mainLoop();

private:
    timePoint_t m_currentFrameBegin;
    timePoint_t m_currentFrameEnd;

    std::list<std::shared_ptr<GameObject>> m_objects;
    std::shared_ptr<Collider> m_collider;

    std::shared_ptr<Renderer> m_renderer;
    std::shared_ptr<SimpleScreen> m_screen;

    std::shared_ptr<Controller> m_controller;

    std::shared_ptr<CoreEventQueue> m_eventQueue;

    bool m_shouldExit;
    bool m_runSimulation;

    keyboardKey_t m_exitKey;
    keyboardKey_t m_toggleSimulationKey;
};

}

#endif //NDE_GAME_HPP