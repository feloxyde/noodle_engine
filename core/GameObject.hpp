/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_GAMEOBJECT_HPP
#define NDE_GAMEOBJECT_HPP
#include <memory>
#include "../utils/ndetypes.hpp"
#include <vector>
#include <list>

namespace nde
{

class CoreEvent;
class PhysicBody;
class AbstractCoreEventListener;
class GraphicObject;

/** General purpose game object, meant to be inherited from by specialized game objects,
* as map elements or player character
*/
class GameObject : public std::enable_shared_from_this<GameObject>
{
public: 
    /** Creates the object, the object needs to be registered to the game with registerToGame() after creation is complete */
    GameObject();

    /** destroys the object
    * DO NOT call this directly, but by using the kill() method !
    */
    virtual ~GameObject();

    /** Method called every time a simulation frame is beginning, right after the update of game controller
    *   it is meant to be overriden to give the object a systematic comportment
    *   
    *   @param elapsed (passed by Game class) : the frame duration
    */
    virtual void onFrameBegin(timeInterval_t elapsed);

    /** Method called every time a simulation frame is ending, right before rendering the world
    *   it is meant to be overriden to give the object a systematic comportement, or special graphics according to the simulation
    *   
    *   @param elapsed (passed by Game class) : the frame duration
    */
    virtual void onFrameEnd(timeInterval_t elapsed);

    /** Method called upon passing an event to the object, do not call, do not override */
    void onEvent(std::shared_ptr<CoreEvent> event);

    /** returns true if the object has not yet been killed using kill() method */
    bool isAlive();

    /** sets the object to die. It will be removed from game simulation at end of the frame. */
    void kill();

    /** adds an event listener to the object
    *
    * @param listener the listener to add
    */
    void addEventListener(std::shared_ptr<AbstractCoreEventListener> listener);

    /** removes an event listener from the object
    *
    * @param listener the listener to remove
    */
    void removeEventListener(std::shared_ptr<AbstractCoreEventListener> listener);

    /** @return the physicbody of the object, can be nullptr */
    std::shared_ptr<PhysicBody> getPhysicBody();

    /** @return the collection of graphic objects of the object, can be empty */
    std::vector<std::shared_ptr<GraphicObject>>& getGraphicObjects();

    /** registers object to the game, declaring itself as owner of his body if not null, 
    *    adding body to collider if not null,  adding graphic objects to the top level renderer.
    */
    void registerToGame();

    /** sets the physicBody of the object, DO NOT CALL */
    void setPhysicBody(std::shared_ptr<PhysicBody> body);


protected: 
    std::shared_ptr<PhysicBody> m_body;
    bool m_isAlive;
    std::list<std::shared_ptr<AbstractCoreEventListener>> m_eventListeners;
    std::vector<std::shared_ptr<GraphicObject>> m_graphicObjects;
};


} //namespace nde


#endif //NDE_GAMEOBJECT_HPP