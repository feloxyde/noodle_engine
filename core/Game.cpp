/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Game.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>
#include "../utils/ndetypes.hpp"
#include <vector>

#include "GameObject.hpp"
#include "../physics/Collider.hpp"
#include "../graphics/Renderer.hpp"
#include "../graphics/View.hpp"

#include "../interface/Screen.hpp"
#include "../interface/Controller.hpp"

#include "CoreEventQueue.hpp"
#include "CoreEvent.hpp" //not mandatory

namespace nde {

/* PUBLIC STATIC METHODS */

void Game::init(int exitKeyCode, int toggleSimulationKeyCode)
{
    s_gameInstance = std::make_unique<Game>(exitKeyCode, toggleSimulationKeyCode); 
}

void Game::registerGameObject(std::shared_ptr<GameObject> object)
{
    s_gameInstance->addGameObject(object);
}


void Game::registerCoreEvent(std::shared_ptr<CoreEvent> event)
{
    s_gameInstance->addCoreEvent(event);
}

void Game::runMainLoop()
{
    s_gameInstance->mainLoop();
}

timePoint_t Game::currentFrameEnd()
{
    return s_gameInstance->m_currentFrameEnd;
}

timePoint_t Game::currentFrameBegin()
{
    return s_gameInstance->m_currentFrameBegin;
}

std::shared_ptr<Collider> Game::getGameCollider()
{
    return s_gameInstance->m_collider;
}

std::shared_ptr<Renderer> Game::getGameRenderer()
{
    return s_gameInstance->m_renderer;
}

std::shared_ptr<SimpleScreen> Game::getGameScreen()
{
    return s_gameInstance->m_screen; 
}

std::shared_ptr<Controller> Game::getGameController()
{
    return s_gameInstance->m_controller;
}

std::shared_ptr<CoreEventQueue> Game::getGameEventQueue()
{
    return s_gameInstance->m_eventQueue;
}
    
void Game::requireExit()
{
    s_gameInstance->m_shouldExit = true;
}

void Game::simulate(bool simulate)
{
    s_gameInstance->m_runSimulation = simulate;
}

/* PRIVATE STATIC MEMBERS */

std::unique_ptr<Game> Game::s_gameInstance;


/* PRIVATE OBJECT METHODS */

Game::Game(int exitKeyCode, int toggleSimulationKeyCode):
m_currentFrameBegin(0.0f),
m_currentFrameEnd(0.0f),
m_objects(),
m_collider(),
m_renderer(),
m_screen(),
m_controller(),
m_eventQueue(),
m_shouldExit(false),
m_runSimulation(false),
m_exitKey(0),
m_toggleSimulationKey(0)
{

    m_eventQueue = std::make_shared<CoreEventQueue>();

    m_screen = std::make_shared<SimpleScreen>(0);

    m_controller = Controller::create(m_screen->getWindow());

    m_toggleSimulationKey = m_controller->addKey(toggleSimulationKeyCode);
    m_exitKey = m_controller->addKey(exitKeyCode);


    glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
    glEnable(GL_DEPTH_TEST);
  
    View& view = *(m_screen->getView());

    m_renderer = std::make_shared<GlobalRenderer>();

    view.watch(m_renderer.get());

    m_collider = std::make_shared<Collider>(m_eventQueue);
}

Game::~Game()
{
    //nothing to do
}

void Game::addGameObject(std::shared_ptr<GameObject> object)
{
    
    m_objects.push_back(object);
    if(object->getPhysicBody() != nullptr)
    {
        m_collider->addPhysicBody(object->getPhysicBody());
    }
    for(auto gro = object->getGraphicObjects().begin(); gro != object->getGraphicObjects().end(); ++gro)
    {
        if(*gro == nullptr){
        }
        else {
            m_renderer->referGraphicObject(*gro);
        }
    }
}

void Game::removeGameObject(std::shared_ptr<GameObject> object)
{
    if(object->getPhysicBody() != nullptr)
    {
        m_collider->removePhysicBody(object->getPhysicBody());
    }
    for(auto gro = object->getGraphicObjects().begin(); gro != object->getGraphicObjects().end(); ++gro)
    {
        m_renderer->removeGraphicObject(*gro);
    }
}


void Game::addCoreEvent(std::shared_ptr<CoreEvent> event)
{
    this->m_eventQueue->addEvent(event);
}

void Game::mainLoop()
{
    glfwSetTime(0.0);
    m_screen->update(0.0);

    while (!m_shouldExit) {
        
        timeInterval_t elapsed = glfwGetTime();
        glfwSetTime(0.0);

        m_controller->update();
        if(m_controller->hasKeyBeenPressed(m_exitKey))
        {
            return;
        }
        if(m_controller->hasKeyBeenPressed(m_toggleSimulationKey))
        {
            m_runSimulation = !m_runSimulation;
        }
        
        //running simulation

        if(m_runSimulation){

            m_currentFrameEnd += elapsed;
            
            for(auto go = m_objects.begin(); go != m_objects.end(); ++go)
            {
                (*go)->onFrameBegin(elapsed);
            } 

            m_collider->simulateForward(elapsed);

            m_eventQueue->flushTo(m_currentFrameEnd);

            //removing every killed objects

            auto go = m_objects.begin();
            while (go != m_objects.end())
            {
                if((*go)->isAlive())
                {
                    ++go;
                } else {
                    this->removeGameObject(*go);
                    go = m_objects.erase(go);
                }
            }

            for(auto go = m_objects.begin(); go != m_objects.end(); ++go)
            {
                (*go)->onFrameEnd(elapsed);
            } 

            m_screen->update(elapsed); //for now we dont care about time
            
            m_currentFrameBegin = m_currentFrameEnd;
        
        } else {
        
            m_screen->update(0.0); //for now we dont care about time

        }
        

    }

}


}