/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#version 330 core

// Interpolated values from the vertex shaders
in vec2 UV;
in vec3 normal;

// Ouput data
out vec3 color;


void main(){


    // Output color = color of the texture at the specified UV
    vec3 color0 = vec3(1.0, 0.0, 0.0); // red
    vec3 color1 = vec3(0.0, 0.0, 1.0); // blue
    vec3 color2 = vec3(1.0, 0.5, 0.0); // orange
    vec3 color3 = vec3(0.0, 1.0, 0.0); // green 
    vec3 color4 = vec3(1.0, 1.0, 0.0); // yellow
    vec3 color5 = vec3(1.0, 1.0, 1.0); // white
    

    vec3 lightDirection = normalize(vec3(-1.0, 3.0, -0.3)); //lighting direction (ambiant light)
    vec3 lightColor = vec3(1.0, 1.0, 1.0);
    vec3 ambiantLight = vec3(0.1, 0.1, 0.1);
    vec3 lightPower = vec3(1.0, 1.0, 1.0);

    if (UV.x > 4.5) {
        //5
        color = color5;
    } else if (UV.x > 3.5) {
        //4
        color = color4;
    } else if (UV.x > 2.5) {
        //3
        color = color3;
    } else if (UV.x > 1.5) {
        //2
        color = color2;
    } else if (UV.x > 0.5) {
        //1
        color = color1;
    } else {
        //0
        color = color0;
    }

    float cosTheta = clamp(dot (normal, lightDirection), 0,1);
    color = clamp(ambiantLight + color * lightColor * cosTheta * lightPower, 0,1);


}