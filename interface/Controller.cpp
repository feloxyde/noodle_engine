/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Controller.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
namespace nde{

/* ##################################################################### */
/* ########## INPUT CLASS IMPLEMENTATION ############################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

Input::Input()
{
    //nothing
}

Input::~Input()
{
    //noting
}

/* ##################################################################### */
/* ########## KEY CLASS IMPLEMENTATION ################################# */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

Key::Key(keyboardKey_t key):
Input::Input(),
m_key(key),
m_isPressed(false),
m_hasBeenPressed(false),
m_hasBeenReleased(false)
{}

Key::~Key()
{
    //nothing to do
}

void Key::reactTo(keyboardKey_t key, keyAction_t action)
{
    if(key == m_key)
    {
        if(action == GLFW_PRESS)
        {
            m_isPressed = true;
            m_hasBeenPressed = true;
        }
        else if (action == GLFW_RELEASE)
        {
            m_isPressed = false;
            m_hasBeenReleased = true;
        }
    }
}

bool Key::isPressed()
{
    return m_isPressed;
}

bool Key::hasBeenPressed()
{
    return m_hasBeenPressed;
}
bool Key::hasBeenReleased()
{
    return m_hasBeenReleased;
}

void Key::onUpdate()
{
    m_hasBeenPressed = false;
    m_hasBeenReleased = false;
}


/* ##################################################################### */
/* ########## AXIS CLASS IMPLEMENTATION ################################ */
/* ##################################################################### */

/* PUBLIC STATIC CONSTANTS */

axisValue_t const Axis::AXIS_INCREMENT = 1.0f;

/* PUBLIC OBJECT METHODS */

Axis::Axis(keyboardKey_t minus, keyboardKey_t plus):
Input::Input(),
m_minusKey(minus),
m_plusKey(plus),
m_value(0.0f)
{}

Axis::~Axis()
{
    //nothing to do
}

void Axis::reactTo(keyboardKey_t key, keyAction_t action)
{
    
    if(key == m_minusKey){
        if(action == GLFW_PRESS) 
        {
            m_value -= Axis::AXIS_INCREMENT;
        } 
        else if (action == GLFW_RELEASE)
        {
            m_value += Axis::AXIS_INCREMENT;
        }
    }
    else if(key == m_plusKey)
    {
        if(action == GLFW_PRESS) 
        {
            m_value += Axis::AXIS_INCREMENT;
        } 
        else if (action == GLFW_RELEASE)
        {
            m_value -= Axis::AXIS_INCREMENT;
        }
    }
}

axisValue_t Axis::getValue()
{
    return m_value;
}

void Axis::onUpdate()
{
    //nothing to do
}


/* ##################################################################### */
/* ########## CONTROLLER CLASS IMPLEMENTATION ########################## */
/* ##################################################################### */

/* CALLBACK STATIC TO FILE FUNCTIONS */

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{

  auto ctrl =  Controller::get();
  ctrl->sendNewMousePos(glm::vec2((float)xpos, (float)ypos));

}

/* UNUSED FOR THE MOMENT
static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

  switch (button) {
    case GLFW_MOUSE_BUTTON_2 :
      if(action == GLFW_PRESS) mright = true;
      if(action == GLFW_RELEASE) mright = false;
  }

}
*/

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    auto ctrl = Controller::get();
    ctrl->sendKeyAction(key, action);
}

/* PUBLIC STATIC METHODS */

std::shared_ptr<Controller> Controller::create(GLFWwindow* windowTarget)
{
    if(s_controllerInstance != nullptr)
    {
        return std::shared_ptr<Controller>();
    }

    s_controllerInstance = std::make_shared<Controller>(windowTarget);
    return s_controllerInstance;

}

std::shared_ptr<Controller> Controller::get()
{
    return s_controllerInstance;
}

/* PRIVATE STATIC MEMBERS */

std::shared_ptr<Controller> Controller::s_controllerInstance;
inputIdentifier_t Controller::s_nextKeyID = 0;
inputIdentifier_t Controller::s_nextAxisID = 0;

/* PUBLIC OBJECT METHODS */

Controller::~Controller()
{

}

void Controller::update()
{
    m_mouseMove = glm::vec2(0.0f, 0.0f);
    for(auto it = m_keys.begin(); it != m_keys.end(); ++it)
    {
        it->onUpdate();
    }
    for(auto it = m_axis.begin(); it != m_axis.end(); ++it)
    {
        it->onUpdate();
    }

    glfwPollEvents();
}

mousePos_t Controller::getMousePos()
{
    return m_mousePos;
}
mouseMove_t Controller::getMouseMove()
{
    return m_mouseMove;
}
    
inputIdentifier_t Controller::addKey(keyboardKey_t key)
{
    m_keys.push_back(Key(key));
    return s_nextKeyID++;
}

inputIdentifier_t Controller::addAxis(keyboardKey_t minus, keyboardKey_t plus)
{
    m_axis.push_back(Axis(minus, plus));
    return s_nextAxisID++;
}
    
axisValue_t Controller::getAxisValue(inputIdentifier_t axisID)
{
    return m_axis[axisID].getValue();
}

bool Controller::isKeyPressed(inputIdentifier_t keyID)
{
    return m_keys[keyID].isPressed();
}

bool Controller::hasKeyBeenPressed(inputIdentifier_t keyID)
{
    return m_keys[keyID].hasBeenPressed();
}

bool Controller::hasKeyBeenReleased(inputIdentifier_t keyID)
{
    return m_keys[keyID].hasBeenReleased();
}

void Controller::sendNewMousePos(mousePos_t newPos)
{
    m_mouseMove = newPos - m_mousePos;
    m_mousePos = newPos;
}

void Controller::sendKeyAction(keyboardKey_t key, keyAction_t action)
{   
    for(auto it = m_keys.begin(); it != m_keys.end(); ++it)
    {
        it->reactTo(key, action);
    }
    for(auto it = m_axis.begin(); it != m_axis.end(); ++it)
    {
        it->reactTo(key, action);
    }
}

/* PUBLIC OBJECT METHODS */

Controller::Controller(GLFWwindow* targetWindow)
{
  glfwSetCursorPosCallback(targetWindow, cursor_position_callback);
  //glfwSetMouseButtonCallback(targetWindow, mouse_button_callback);
  glfwSetKeyCallback(targetWindow, key_callback);
}


}//namespace nde
