/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_SCREEN_H
#define NDE_SCREEN_H

#include "../utils/ndetypes.hpp"
#include "../graphics/View.hpp"
#include "../graphics/ndeGL.hpp"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string>

namespace nde {

class Screen {
public:
  virtual void update(timeInterval_t elapsed) = 0;
};

/** a simple screen filled with a player's view */
class SimpleScreen : public Screen {
public:
  static std::string const DEFAULT_SHADER_NAME;
  static GLfloat const FULLSCREEN_QUAD_DATA[];


public:
  /** creates a screen. This is called by the Game::init method.
  * 
  * @param preferedScreen the screen ID to use (GLFW) as fullscreen render target
  */
  SimpleScreen(screenID_t preferedScreen);

  /** deletes the screen */
  virtual ~SimpleScreen();

  /** updates the screen, do not call */
  virtual void update(timeInterval_t elapsed); //updates the screen

  /** sets view of the screen, do not call */
  void setView(View* view);

  /** @return the view of the screen */
  View* getView(){return m_view;}

  /** @return the windows associated to the fullscreen */
  GLFWwindow* getWindow(){return m_window;}

  /** return wether the windows should quit or not */
  bool shouldNotQuit(){return !glfwWindowShouldClose(m_window);}

private:

  View* m_view;
  GLFWwindow* m_window;
  screenSize_t m_width;
  screenSize_t m_height;
  VertexBuffer<GLfloat, 3, GL_FLOAT>* m_quad;
  std::shared_ptr<Shader> m_shader;
  VertexArrayObject* m_vao;
};

}

#endif //NDE_SCREEN_H
