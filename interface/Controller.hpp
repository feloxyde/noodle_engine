/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_CONTROLLER_HPP
#define NDE_CONTROLLER_HPP

#include "../utils/ndetypes.hpp"
#include <memory>
#include <vector>

class GLFWwindow;

namespace nde{

/** class used internally (Controller), do not use */
class Input 
{
public:
    Input();
    virtual ~Input();

    virtual void reactTo(keyboardKey_t key, keyAction_t action) = 0;
    virtual void onUpdate() = 0;
};

/** class used internally (Controller), do not use */
class Key : public Input
{
public:
    Key(keyboardKey_t key);
    virtual ~Key();

    virtual void reactTo(keyboardKey_t key, keyAction_t action);
    bool isPressed();
    bool hasBeenPressed();
    bool hasBeenReleased();

    void onUpdate();

private:
    keyboardKey_t m_key;
    bool m_isPressed;
    bool m_hasBeenPressed;
    bool m_hasBeenReleased;
};

/** class used internally (Controller), do not use */
class Axis : public Input 
{
public:
    static axisValue_t const AXIS_INCREMENT;

public:
    Axis(keyboardKey_t minus, keyboardKey_t plus);
    virtual ~Axis();

    virtual void reactTo(keyboardKey_t key, keyAction_t action);
    axisValue_t getValue();

    virtual void onUpdate();

private:
    keyboardKey_t m_minusKey;
    keyboardKey_t m_plusKey;
    axisValue_t m_value;
};

typedef std::vector<Axis> AxisVector_t;
typedef std::vector<Key> KeyVector_t;



/** A simple way of handling inputs 
*
*   Do not create the controller directly, it is done when init the game
*
*
*/
class Controller
{
public:
    static std::shared_ptr<Controller> create(GLFWwindow* windowTarget);
    static std::shared_ptr<Controller> get();

private:
    static std::shared_ptr<Controller> s_controllerInstance;
    static inputIdentifier_t s_nextKeyID;
    static inputIdentifier_t s_nextAxisID;

public:
    virtual ~Controller();

    void update(); //reset counters and then run the callbacks

    /** @return the current positon of the mouse on screen */
    mousePos_t getMousePos();
    /** @return the movement of the mouse since last frame */
    mouseMove_t getMouseMove();
    
    /** adds a key to the controller to listen to, and returns its ID
    *
    *   @key the GLFW keycode to listen to
    *   
    *   @return the ID of the added key
    */
    inputIdentifier_t addKey(keyboardKey_t key);

    /** adds an axis to the controller, combo of two keys. the axis will have then a value
    *   between -1.0f and 1.0f, depending on wether one, none or both keys are pressed.
    *   minus only = -1.0f, plus only = 1.0f, both or none = 0.0f
    *   
    *   @param minus the GLFW keycode of the lower direction of the axis
    *   @param plus the GLFW keycode of the higher direction of the axis
    *
    *   @return the ID of the added axis
    */
    inputIdentifier_t addAxis(keyboardKey_t minus, keyboardKey_t plus);
    
    /** returns the value of a previously added axis 
    *
    *   @param axisID the identifier of the axis, returned by addAxis when registering it
    *   
    *   @return the value of the axis
    */
    axisValue_t getAxisValue(inputIdentifier_t axisID);

    /** checks wether previously registered key is currently pressed or not
    *
    *   @keyID the identifier of the key, returned by addKey when registering it
    *
    *   @return true if the key is currently pressed, false otherwise
    */
    bool isKeyPressed(inputIdentifier_t keyID);

    /** checks wether previously registered key has been pressed during the current frame or not
    *
    *   @keyID the identifier of the key, returned by addKey when registering it
    *
    *   @return true if the key is has been pressed during the  current frame, false otherwise
    */
    bool hasKeyBeenPressed(inputIdentifier_t keyID);

    /** checks wether previously registered key has been released during the current frame or not
    *
    *   @keyID the identifier of the key, returned by addKey when registering it
    *
    *   @return true if the key is has been released during the  current frame, false otherwise
    */
    bool hasKeyBeenReleased(inputIdentifier_t keyID);

    /** called by the glfw callbacks during update, do not call */
    void sendNewMousePos(mousePos_t newPos);

    /** called by the glfw callbacks during update, do not call */
    void sendKeyAction(keyboardKey_t key, keyAction_t action);

public:
    Controller(GLFWwindow* target);

private: /* #FIXME public RN for simplifying, shall be made private later on */
    mousePos_t m_mousePos;
    mouseMove_t m_mouseMove;
    KeyVector_t m_keys;
    AxisVector_t m_axis;

};

}//namespace nde

#endif //NDE_CONTROLLER_HPP