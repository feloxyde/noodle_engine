/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Screen.hpp"

namespace nde {

/* ##################################################################### */
/* ########## SIMPLESCREEN CLASS IMPLEMENTATION ######################## */
/* ##################################################################### */

/* SHIT #FIXME will have to be pushed outside the screen in an Glfw init function or so */
static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}


std::string const SimpleScreen::DEFAULT_SHADER_NAME = "screen";

/* PRIVATE STATIC CONSTANTS */
GLfloat const SimpleScreen::FULLSCREEN_QUAD_DATA[] = {
  -1.0f, -1.0f,  0.0f,
   1.0f, -1.0f,  0.0f,
  -1.0f,  1.0f,  0.0f,
  -1.0f,  1.0f,  0.0f,
   1.0f, -1.0f,  0.0f,
   1.0f,  1.0f,  0.0f
};


/* PUBLIC OBJECT METHODS */


SimpleScreen::SimpleScreen(screenID_t preferedScreen):
m_view(nullptr), m_window(nullptr), m_width(0), m_height(0),
m_quad(nullptr),
m_shader(nullptr),
m_vao(nullptr)
{
  glfwSetErrorCallback(error_callback);

  if (!glfwInit()){
      std::cout << "failed to initialize glfw" << std::endl;
      exit(EXIT_FAILURE);
  }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  int mcount;
  GLFWmonitor** monitors = glfwGetMonitors(&mcount);

  if (monitors == nullptr){
    std::cout << "error while looking for monitors" << std::endl;
    exit(EXIT_FAILURE);
  }

  GLFWmonitor* prim = monitors[0];

  if (mcount >= preferedScreen){

    prim = monitors[preferedScreen];

  }

  const GLFWvidmode* mode = glfwGetVideoMode(prim);
  glfwWindowHint(GLFW_RED_BITS, mode->redBits);
  glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

  std::string mname(glfwGetMonitorName(prim));
  std::cout << "Displaying on monitor " << mname << std::endl;

  m_window = glfwCreateWindow(mode->width, mode->height, "noodle engine", prim, NULL);


  m_width = mode->width;
  m_height = mode->height;
  if (!m_window)
  {
      glfwTerminate();
      exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(m_window);

  glewExperimental=GL_TRUE; // Needed in core profile
  if (glewInit() != GLEW_OK) {
    std::cout << "failed to initialize glew" << std::endl;
    exit(EXIT_FAILURE);
  }
  glfwSwapInterval(1);

  glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

  m_view = new View(m_width, m_height);

  m_quad = new VertexBuffer<GLfloat, 3, GL_FLOAT>(FULLSCREEN_QUAD_DATA, sizeof(FULLSCREEN_QUAD_DATA), 0);
  m_shader = Shader::loadShader(DEFAULT_SHADER_NAME);

  m_vao = new VertexArrayObject();

  m_vao->bind(); //#FIXME is it logic to use only one VAO for the whole thing ?

//  glDrawArrays(GL_TRIANGLES, 0, 6);

}

SimpleScreen::~SimpleScreen()
{
  glfwDestroyWindow(m_window);
  glfwTerminate();
}

void SimpleScreen::update(timeInterval_t elapsed)
{
  //#FIXME add update view here
  m_view->update(elapsed);

  m_shader->use();
  //disable any framebuffer
  FrameBuffer::bindZero();
  glViewport(0,0, m_width, m_height);


  glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  //setting rendered texture

  glActiveTexture(GL_TEXTURE0);
  m_view->use();

  GLuint texID = m_shader->getUniformLocation("renderedTexture");

  glUniform1i(texID, 0);

  m_quad->bind();
  m_quad->enable();

  
    glVertexAttribPointer(
        0,
        3,
        GL_FLOAT,
        GL_FALSE,
        0,
        (void*)0
      );
  

  glDrawArrays(GL_TRIANGLES, 0, 6);

  m_quad->disable();
  glfwSwapBuffers(m_window);

}


}
