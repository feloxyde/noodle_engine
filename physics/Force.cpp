/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Force.hpp"
#include <limits>
#include <math.h>
#include <glm/gtc/type_ptr.hpp>
#include "../utils/geometry.hpp"
#include <limits>
#include <list>
#include <memory>

namespace nde {
/* ##################################################################### */
/* ########## FORCE CLASS IMPLEMENTATION ############################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

Force::Force()
{} //nothing to do

Force::~Force()
{
    //nothing to do
}

/* ##################################################################### */
/* ########## Impulse CLASS IMPLEMENTATION ############################# */
/* ##################################################################### */


Impulse::Impulse(glm::vec3 force, timeInterval_t timeleft):
Force::Force(),
m_force(force),
m_timeleft(timeleft)
{}

Impulse::~Impulse()
{
    //nothing to do
}

bool Impulse::isResist(speed_t currentSpeed, axis_t axis)
{
    return false;
}

bool Impulse::reduceTime(timeInterval_t elapsed)
{
    if(elapsed >= m_timeleft)
    {
        return false;
    }
    m_timeleft -= elapsed;
    return true;
}

float Impulse::getForce(speed_t objectSpeed, axis_t axis, timeInterval_t elapsed)
{   
    float axisComponent = glm::value_ptr(m_force)[axis];
    if(elapsed > m_timeleft)
    {
        axisComponent = axisComponent * m_timeleft / elapsed;
    }
    return axisComponent;
}

float Impulse::getResistForce(glm::vec3 forceSum, axis_t axis, timeInterval_t elapsed)
{
    return 0.0f;
}

float Impulse::getTimeToResist(float acceleration,float currentSpeed, axis_t axis)
{
    return -std::numeric_limits<float>::infinity();
}

/* ##################################################################### */
/* ########## PlaneSpeedForce CLASS IMPLEMENTATION ##################### */
/* ##################################################################### */

PlaneSpeedForce::PlaneSpeedForce(speed_t targetSpeed, glm::vec3 planeNormal, float force):
Force::Force(),
m_targetSpeed(targetSpeed),
m_planeNormal(planeNormal),
m_force(force)
{}
PlaneSpeedForce::~PlaneSpeedForce()
{
    //nothing to do
}
    
bool PlaneSpeedForce::isResist(speed_t currentSpeed, axis_t axis)
{
    return glm::value_ptr(m_targetSpeed)[axis] == glm::value_ptr(currentSpeed)[axis];
}
/* reduce timespan of the force and returns true if the force still have time left */
bool PlaneSpeedForce::reduceTime(timeInterval_t elapsed)
{
    return false;
}

float PlaneSpeedForce::getForce(speed_t objectSpeed, axis_t axis, timeInterval_t elapsed)
{
    speed_t speedError = m_targetSpeed - objectSpeed;
    speed_t planeSpeedError = speedError - Geometry::colinearPart(speedError, m_planeNormal);

    if(planeSpeedError.x == 0.0f && planeSpeedError.y == 0.0f && planeSpeedError.z == 0.0f)
    {
        return 0.0f;
    }

    speed_t correctionDirection = glm::normalize(planeSpeedError);

    return glm::value_ptr(correctionDirection*m_force)[axis];
}

float PlaneSpeedForce::getResistForce(glm::vec3 forceSum, axis_t axis, timeInterval_t elapsed)
{
    if(forceSum.x == 0.0f && forceSum.y == 0.0f && forceSum.z == 0.0f)
    {
        return 0.0f;
    } 
    glm::vec3 planeForceSum = forceSum - Geometry::colinearPart(forceSum, m_planeNormal);
    if(planeForceSum.x == 0.0f && planeForceSum.y == 0.0f && planeForceSum.z == 0.0f)
    {
        return 0.0f;
    }

    glm::vec3 planeSForceDir = glm::normalize(planeForceSum);
    
    float force = m_force;
    if(force > glm::length(forceSum)){
        force = glm::length(forceSum);
    }
  return glm::value_ptr(-force*planeSForceDir)[axis];
}

float PlaneSpeedForce::getTimeToResist(float acceleration, float currentSpeed, axis_t axis)
{
    return (glm::value_ptr(m_targetSpeed)[axis] - currentSpeed)/acceleration;
}

} //namespace nde