/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PhysicBody.hpp"
#include <limits>
#include <math.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "Collider.hpp"
#include "Force.hpp"
#include "PhysicLayer.hpp"

#include <limits>
namespace nde {


/* ##################################################################### */
/* ########## DynamicPhysicBody CLASS IMPLEMENTATION ################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

PhysicBody::PhysicBody(std::shared_ptr<PhysicLayer> layer, float frictionForce):
Body::Body(),
m_collider(),
m_layer(layer),
m_frictionForce(frictionForce)
{}

PhysicBody::~PhysicBody()
{
    //nothing to do
}

void PhysicBody::setCollider(std::weak_ptr<Collider> collider)
{
    m_collider = collider;
}

std::weak_ptr<Collider> PhysicBody::getCollider()
{
    return m_collider;
}

bool PhysicBody::collidesWith(std::shared_ptr<PhysicBody> other)
{
    if (this == other.get())
    {
        return false;
    }

    return this->getLayer()->collidesWith(other->getLayer()) 
        && other->getLayer()->collidesWith(this->getLayer());
}


std::shared_ptr<PhysicLayer> PhysicBody::getLayer()
{
    return m_layer;
}

std::weak_ptr<GameObject> PhysicBody::getOwner()
{
 return m_owner;
}

void PhysicBody::setOwner(std::weak_ptr<GameObject> owner)
{
    m_owner = owner;
}

void PhysicBody::setFrictionForce(float frictionForce)
{
    m_frictionForce = frictionForce;
}

float PhysicBody::getFrictionForce()
{
    return m_frictionForce;
}


/* ##################################################################### */
/* ########## DynamicPhysicBody CLASS IMPLEMENTATION ################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */
    
DynamicPhysicBody::DynamicPhysicBody(std::shared_ptr<PhysicLayer> layer, mass_t mass, float frictionForce):
PhysicBody::PhysicBody(layer, frictionForce),
m_speed(0.0f, 0.0f, 0.0f),
m_mass(mass),
m_forces()
{
//nothing to do here
}

DynamicPhysicBody::~DynamicPhysicBody()
{
//nothing to do here
}


speed_t DynamicPhysicBody::getSpeed()
{
    return m_speed;
}


void DynamicPhysicBody::setSpeed(speed_t speed)
{
    m_speed = speed;
}

mass_t DynamicPhysicBody::getMass()
{
    return m_mass;
}

static void packForces(ForceList_t& list, speed_t currentSpeed, std::vector<ForceList_t>& drag, std::vector<ForceList_t>& resist)
{
   
    
    for(size_t axis = 0; axis < 3; ++axis)
    {
        drag.push_back(ForceList_t());
        resist.push_back(ForceList_t());
    }

    auto sf = list.begin();
    while(sf != list.end())
    {
        for(size_t axis = 0; axis < 3; ++axis)
        {
            if((*sf)->isResist(currentSpeed, axis)){
                resist[axis].push_back(*sf);
            } else {
                drag[axis].push_back(*sf);
            }
        } 
        sf++;
    }
}

void DynamicPhysicBody::applyForces(timeInterval_t duration)
{
    glm::vec3 accel(0.0f, 0.0f, 0.0f);
    glm::vec3 impSum(0.0f, 0.0f, 0.0f);

    //now we continue
    timeInterval_t timeleft = duration;
    std::vector<ForceList_t> drag;
    std::vector<ForceList_t> resist;
    glm::vec3 sfSum(0.0f);

    while (timeleft > 0.0f)
    {
        sfSum = glm::vec3(0.0f);
        
        packForces(m_forces, m_speed, drag, resist);
        //now for each axis, we compute the resulting force.

        for(size_t axis = 0; axis < 3; ++axis)
        {
            //first the drag forces
            for(auto sf = drag[axis].begin(); sf != drag[axis].end(); ++sf)
            {
                glm::value_ptr(sfSum)[axis] +=  (*sf)->getForce(m_speed, axis, duration);
            }
            //then the resistive forces

            if(sfSum.x != 0.0f || sfSum.y != 0.0f || sfSum.z != 0.0f){
                for(auto sf = resist[axis].begin(); sf != resist[axis].end(); ++sf)
                {
                    glm::value_ptr(sfSum)[axis] += (*sf)->getResistForce(sfSum, axis, duration);
                }   
            }         
        }
    
        //now that we have the new force sum, we can compute the first time when a drag force will become a resist force   
        float dragToResistTime(std::numeric_limits<float>::infinity());
        accel = (sfSum)/m_mass;
        
        for(axis_t axis = 0; axis < 3; ++axis)
        {     
            float nspeed = glm::value_ptr(m_speed)[axis];
            float acc = glm::value_ptr(accel)[axis];
          
            for(auto sf = drag[axis].begin(); sf != drag[axis].end(); ++sf)
            {
                float timeToResist = timeleft;
                if(acc != 0.0f){
                    timeToResist = (*sf)->getTimeToResist(acc, nspeed, axis);   
                }
                if(timeToResist > 0 && timeToResist < dragToResistTime){
                    dragToResistTime = timeToResist;
                }
            }
        }

        if(dragToResistTime > timeleft)
        {
            dragToResistTime = timeleft;
        }
        m_speed += accel * dragToResistTime;
        timeleft -= dragToResistTime;
        //retablishing speed forces: 
        drag.clear();
        resist.clear();
    } 
    
    auto sf = m_forces.begin();

    while(sf != m_forces.end())
    {
        if((*sf)->reduceTime(duration))
        {
            ++sf;
        } else {
            sf = m_forces.erase(sf);
        }
    }
}

void DynamicPhysicBody::addForce(std::shared_ptr<Force> force)
{
    m_forces.push_back(force);
}


    
void DynamicPhysicBody::collisionEffect(Collision const& collision, std::shared_ptr<PhysicBody> other)
{
    //doing nothing
}


/* ##################################################################### */
/* ########## StaticPhysicBody CLASS IMPLEMENTATION #################### */
/* ##################################################################### */

/* PUBLIC METHODS */

StaticPhysicBody::StaticPhysicBody(std::shared_ptr<PhysicLayer> layer, float frictionForce):
PhysicBody::PhysicBody(layer, frictionForce)
{
    //nothing
}

StaticPhysicBody::~StaticPhysicBody()
{
    //nothing
}

void StaticPhysicBody::setSpeed(speed_t speed)
{
 //nothing
}

speed_t StaticPhysicBody::getSpeed()
{
    return glm::vec3(0.0f, 0.0f, 0.0f);
}

void StaticPhysicBody::collisionEffect(Collision const& collision, std::shared_ptr<PhysicBody> other)
{
    
    //adding speedForce to the other element
    other->addForce(
        std::make_shared<PlaneSpeedForce>(
            glm::vec3(0.0f, 0.0f, 0.0f), collision.m_normal, this->getFrictionForce()*other->getFrictionForce()*other->getMass()));
}

void StaticPhysicBody::applyForces(timeInterval_t duration)
{
    //nothing
}

void StaticPhysicBody::addForce(std::shared_ptr<Force> force)
{
    //nothing
}

mass_t StaticPhysicBody::getMass()
{
    return std::numeric_limits<float>::infinity();
}

}//namespace nde