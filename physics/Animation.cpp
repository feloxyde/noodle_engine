/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Animation.hpp"
#include <iostream>
namespace nde {
/* ##################################################################### */
/* ########## POSE CLASS IMPLEMENTATION ################################ */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

Pose::Pose(bodyPartID_t target):
m_target(target)
{}

Pose::~Pose()
{}

bodyPartID_t Pose::target()
{
  return m_target;
}

/* ##################################################################### */
/* ########## RELATIVEPOSITION CLASS IMPLEMENTATION #################### */
/* ##################################################################### */

RelativePosition::RelativePosition(bodyPartID_t target, glm::vec3 const& position):
Pose::Pose(target), m_position(position)
{}

RelativePosition::~RelativePosition()
{}

void RelativePosition::applyToBody(Body& body, timeInterval_t interpRatio)
{

  body.getPart(this->target())->translate(
    (m_position - body.getPart(this->target())->getRelativePosition()) * (float)interpRatio);
}

/* ##################################################################### */
/* ########## RELATIVEORIENTATION CLASS IMPLEMENTATION ################# */
/* ##################################################################### */

RelativeOrientation::RelativeOrientation(bodyPartID_t target, glm::quat const& orientation):
Pose::Pose(target), m_orientation(orientation)
{}

RelativeOrientation::~RelativeOrientation()
{}

void RelativeOrientation::applyToBody(Body& body, timeInterval_t interpRatio)
{
  //intermediate orientation using slerp
  glm::quat inter = glm::slerp(body.getPart(this->target())->getRelativeOrientation(), m_orientation, (float)interpRatio);
  //rotation of the body
  body.getPart(this->target())->rotate(Geometry::reorient(body.getPart(this->target())->getRelativeOrientation(), inter));

}


/* ##################################################################### */
/* ########## KEYFRAME CLASS IMPLEMENTATION ############################ */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

KeyFrame::KeyFrame():
m_poses()
{}


KeyFrame::~KeyFrame()
{} //nothing to do

/** adds a position to the keyframe */
void KeyFrame::addPose(Pose *position)
{
  m_poses.push_back(position);
}

  /** applies position to the body, using an interpolation ratio and a ignored parts set.
  *
  * the interpolation ratio represents the part of the actual interpolation to apply.
  * 0 means no change, 1 means reach the position, 0.5 means half etc.
  * It can be tricky in the way it is relative to the actual position.
  *
  * all positions targeting a part that is listed in the ignored will not be applied.
  */
void KeyFrame::applyToBody(Body& body, timeInterval_t interpRatio, std::set<bodyPartID_t> const& ignoredParts)
{
  //for each position,
  for(auto pos = m_poses.begin(); pos != m_poses.end(); ++pos)
  {
    //we apply only if the current part should not be ignored
    if (ignoredParts.find((*pos)->target()) == ignoredParts.end())
    {
        (*pos)->applyToBody(body, interpRatio);
    }
  }

}
/** return a set containing the values of the affected parts */
std::set<bodyPartID_t> KeyFrame::affectedParts()
{
  std::set<bodyPartID_t> parts;
  for(auto pos = m_poses.begin(); pos != m_poses.end(); ++pos)
  {
    parts.emplace((*pos)->target());
  }
  return parts;
}

/* ##################################################################### */
/* ########## ANIMATION CLASS IMPLEMENTATION ########################### */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

Animation::Animation():
m_frames(), m_nexts(), m_reachTimes()
{}

Animation::~Animation()
{}

//adds a frame to the animation and returns the rank in which she is added
frameRank_t Animation::addFrame(KeyFrame* frame, frameRank_t next, timeInterval_t reachTime)
{
  m_frames.push_back(frame);
  m_nexts.push_back(next);
  m_reachTimes.push_back(reachTime);
  return m_frames.size();
}

KeyFrame* Animation::getFrame(frameRank_t rank)
{
  return m_frames[rank];
}

frameRank_t Animation::getNext(frameRank_t rank)
{
  return m_nexts[rank];
}

timeInterval_t Animation::getReachTime(frameRank_t rank)
{
  return m_reachTimes[rank];
}


frameRank_t Animation::maxRank()
{
  return m_frames.size() - 1;
}

} //namespace nde
