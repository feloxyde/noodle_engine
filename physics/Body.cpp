/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Body.hpp"
#include <limits>

namespace nde {

/* ##################################################################### */
/* ########## BODYPART CLASS IMPLEMENTATION ############################ */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */

BodyPart::BodyPart(BoundingBox* box, std::weak_ptr<Transformable> referenceFrame):
Transformable::Transformable(referenceFrame), m_box(box)
{}

BodyPart::~BodyPart()
{
  delete m_box;
}

BoundingBox& BodyPart::getBox()
{
  return *m_box;
}

/* PROTECTED OBJECT METHODS */

/** transforms the body of the transformable if he has one */
void BodyPart::bodyTransform(glm::mat4 const& transformation)
{
  m_box->transform(transformation);
}
/** translate the body of the transformable if he has one */
void BodyPart::bodyTranslate(glm::vec3 const& translation)
{
  m_box->translate(translation);
}


/* ##################################################################### */
/* ########## BODYPART CLASS IMPLEMENTATION ############################ */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */
/** represents a bodyopart extended by other bodyparts */

ExtendedBodyPart::ExtendedBodyPart(BoundingBox* box, std::weak_ptr<Transformable> referenceFrame):
BodyPart::BodyPart(box, referenceFrame),
m_extensions()
{

}

ExtendedBodyPart::~ExtendedBodyPart()
{}


void ExtendedBodyPart::setExtensions(std::vector<std::shared_ptr<BodyPart>>& extensions)
{
  m_extensions = extensions;
  for (auto it = m_extensions.begin(); it != m_extensions.end(); ++it)
  {
    if (*it != nullptr)
      (*it)->setReferenceFrame(this->weak_from_this());
  }
}

/* PROTECTED OBJECT METHODS */


void ExtendedBodyPart::bodyTransform(glm::mat4 const& transformation)
{
  this->BodyPart::bodyTransform(transformation);
  for (auto it = m_extensions.begin(); it != m_extensions.end(); ++it)
  {
    if (*it != nullptr)
      (*it)->transform(transformation);
  }

}

void ExtendedBodyPart::bodyTranslate(glm::vec3 const& translation)
{
  this->BodyPart::bodyTranslate(translation);
  for (auto it = m_extensions.begin(); it != m_extensions.end(); ++it)
  {
    if (*it != nullptr)
      (*it)->translate(translation);
  }
}

/* ##################################################################### */
/* ########## BODY CLASS IMPLEMENTATION ################################ */
/* ##################################################################### */

/* PUBLIC STATIC METHODS */

bool Body::staticCollisionTest(Body& body1, Body& body2, glm::vec3& extractor)
{
  //#FIXME extractor computation ?
  bodyPartID_t body1PartID = 0;
  bodyPartID_t body2PartID = 0;

  std::vector<std::shared_ptr<BodyPart>>& body1Parts= body1.getParts();
  std::vector<std::shared_ptr<BodyPart>>& body2Parts= body2.getParts();

  while(body1PartID < body1Parts.size())
  {
    while(body2PartID < body2Parts.size())
    {
      if(BoundingBox::staticCollisionTest(body1Parts[body1PartID]->getBox(), body2Parts[body2PartID]->getBox(), extractor))
      {
        BoundingBox::staticCollisionTest(body1.asAxisAlignedBoundingBox(), body2.asAxisAlignedBoundingBox(), extractor);
        return true;
      }
      ++body2PartID;
    }
    ++body1PartID;
  }

  return false;

}

/* computes a sewpt collision test */
bool Body::sweptCollisionTest(Body& body1, glm::vec3& move1,
                           Body& body2, glm::vec3& move2,
                           collisionTime_t& timeIn, collisionTime_t& timeOut,
                           glm::vec3& normal, bodyPartID_t& fpart1, bodyPartID_t& fpart2)
{
  //#FIXME normal pls
  //check for AABB collision first ?
  timeIn = std::numeric_limits<collisionTime_t>::infinity();
  timeOut = -std::numeric_limits<collisionTime_t>::infinity();
  glm::vec3 normalIn(0.0);
  
  collisionTime_t timeInTMP;
  collisionTime_t timeOutTMP;

  bool collision = false;

  std::vector<std::shared_ptr<BodyPart>>& body1Parts = body1.getParts();
  std::vector<std::shared_ptr<BodyPart>>& body2Parts = body2.getParts();

  for (bodyPartID_t part1 = 0; part1 < body1.getParts().size(); ++part1){
    for (bodyPartID_t part2 = 0; part2 < body2.getParts().size(); ++part2){
      if(BoundingBox::sweptCollisionTest
        (body1.getPart(part1)->getBox(), move1,
         body2.getPart(part2)->getBox(), move2,
         timeInTMP, timeOutTMP, normalIn))
      {
        collision = true;
        if (timeInTMP < timeIn){ //tricky, first to enter is needed !
          timeIn = timeInTMP;
          fpart1 = part1;
          fpart2 = part2;
          normal = normalIn;
        }
        if (timeOutTMP > timeOut) // /!\ tricky, we want the MAX out time
          timeOut = timeOutTMP;
      }
    }
  }
  /*if(collision){
    std::cout << "collision detected" << std::endl;
  } else {
    std::cout << "no collision detected" << std::endl;
  }*/
  return collision;
}

/* PUBLIC OBJECT METHODS */

Body::Body():
Transformable::Transformable(), m_parts(), m_rootParts(),
m_AABB(glm::vec3(0.0,0.0,0.0),glm::vec3(0.0,0.0,0.0)), m_validAABB(false)
{}

Body::~Body()
{

}


bodyPartID_t Body::addPart(std::shared_ptr<BodyPart> part, bool asRoot)
{
  m_validAABB = false;
  m_parts.push_back(part);
  //#FIXME maybe keep relative ?
  if(asRoot)
  {
    m_rootParts.push_back(part);
    part->setReferenceFrame(this->weak_from_this());
  }

  return m_parts.size()-1;
}

bodyPartID_t Body::addRootPart(std::shared_ptr<BodyPart> part)
{
  return addPart(part, true);
}

std::shared_ptr<BodyPart> Body::getPart(bodyPartID_t partID)
{
  return m_parts[partID];
}

std::vector<std::shared_ptr<BodyPart>>& Body::getParts()
{
  return m_parts;
}

AxisAlignedBoundingBox Body::asAxisAlignedBoundingBox()
{
  if (!m_validAABB){ // if the AABB is not valid
    //we need to compute the new aabb
    if(m_parts.size() != 0){
      m_AABB = m_parts[0]->getBox().asAxisAlignedBoundingBox();
      for (bodyPartID_t partID = 1; partID < m_parts.size(); ++partID)
      {
        m_AABB = AxisAlignedBoundingBox::merge(m_AABB, m_parts[partID]->getBox().asAxisAlignedBoundingBox());
      }
    }
    else {
      m_AABB = AxisAlignedBoundingBox(this->getPosition(), this->getPosition());
    }

    m_validAABB = true;
  }

  return m_AABB;

}


/* PROTECTED OBJECT METHODS */

/** transforms the body of the transformable if he has one */
void Body::bodyTransform(glm::mat4 const& transformation)
{
  for (bodyPartID_t partID = 0; partID < m_rootParts.size(); ++partID)
  {
    m_rootParts[partID]->transform(transformation);
  }
  m_validAABB = false;
}

/** translate the body of the transformable if he has one */
void Body::bodyTranslate(glm::vec3 const& translation)
{
  for (bodyPartID_t partID = 0; partID < m_rootParts.size(); ++partID)
  {
    m_rootParts[partID]->translate(translation);
  }
  m_validAABB = false;
}

} //namespace nde
