/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Collider.hpp"
#include <list>
#include <algorithm>
#include <iostream>
#include <math.h>
#include <cmath>
#include "PhysicBody.hpp"
#include <glm/gtx/norm.hpp>
#include <glm/gtx/vector_angle.hpp>
#include "../utils/geometry.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/projection.hpp>
#include "Force.hpp"
#include "../core/CoreEventQueue.hpp"
#include "CollisionEvent.hpp"
#include "../core/GameObject.hpp"

namespace nde {


static std::string vec3ToString(glm::vec3 const& vec)
{
  std::string a;
  a += "(";
  a += std::to_string(vec.x);
  a += ";";
  a += std::to_string(vec.y);
  a += ";";
  a += std::to_string(vec.z);
  a += ")";
  return a;
}



/* ##################################################################### */
/* ########## COMPUTATIONAL CLASSES AND FUNCTIONS ###################### */
/* ##################################################################### */



typedef std::list<Collision> CollisionList_t;

struct CollisionFilter
{
  CollisionFilter(Collision const& initCol):
    initialCollision(initCol)
  {}

  Collision const& initialCollision;

  bool operator()(Collision const& value){
    return (value.m_objectA == initialCollision.m_objectA 
         || value.m_objectB == initialCollision.m_objectB 
         || value.m_objectA == initialCollision.m_objectB
         || value.m_objectB == initialCollision.m_objectA);
  }
};

std::unique_ptr<CollisionList_t> listCollisions(timeInterval_t elapsed, PhysicBodyVector_t const& bodies)
{
    auto collisions = std::make_unique<CollisionList_t>();
    
      for(size_t oA = 0; oA < bodies.size()-1; ++oA){
      for(size_t oB = oA+1; oB < bodies.size(); ++oB){
        auto objectA = bodies[oA];
        auto objectB = bodies[oB];
        collisionTime_t timeIn(0.0f), timeOut(0.0f);
        glm::vec3 normal(0);
        glm::vec3 moveA = objectA->getSpeed() * (float)elapsed;
        glm::vec3 moveB = objectB->getSpeed() * (float)elapsed;
        bodyPartID_t partA, partB; //unused for now
        if(objectA->collidesWith(objectB) && Body::sweptCollisionTest(
                    *(objectA), moveA,
                    *(objectB), moveB, 
                    timeIn, timeOut, 
                    normal, partA, partB)){
          collisions->push_back(Collision(objectA, objectB, timeIn, normal));
        }
      }
    }
    
    collisions->sort();

    return collisions;
}

std::unique_ptr<CollisionList_t> listCollisions(
    std::shared_ptr<PhysicBody> objectA, 
    std::shared_ptr<PhysicBody> objectB, 
    PhysicBodyVector_t const& bodies, timeInterval_t timeLeft)
{
    auto collisions = std::make_unique<CollisionList_t>();

    for(auto obj = bodies.begin(); obj != bodies.end(); ++obj){
    collisionTime_t timeIn(0.0f), timeOut(0.0f);
    glm::vec3 normal(0);
    glm::vec3 moveObj = (*obj)->getSpeed()* (float)timeLeft;
    glm::vec3 moveA = objectA->getSpeed() * (float)timeLeft;
    glm::vec3 moveB = objectB->getSpeed() * (float)timeLeft;
    bodyPartID_t partA, partB; //unused, maybe later
            if(objectA->collidesWith(*(obj)) && Body::sweptCollisionTest(
                        *(objectA), moveA,
                        (*(*obj)), moveObj, 
                        timeIn, timeOut, 
                        normal, partA, partB)){
            Collision c(objectA, *obj, timeIn, normal);
            //we check if the object is going in the direction or not of the other following the normal
            collisions->push_back(c);
            }
        
            if(objectB->collidesWith(*(obj)) && Body::sweptCollisionTest(
                        *(objectB), moveB,
                        (*(*obj)), moveObj, 
                        timeIn, timeOut, 
                        normal, partA, partB)){
                        Collision c(objectB, *obj, timeIn, normal);
                collisions->push_back(c);
            }
        
        
      }

    collisions->sort();

    return collisions;
}

/* ##################################################################### */
/* ########## COLLIDER CLASS IMPLEMENTATION ############################ */
/* ##################################################################### */

/* PUBLIC OBJECT METHODS */
 


void Collider::collisionResponse(Collision const& collision, std::shared_ptr<CoreEventQueue> eventQueue)
{

    speed_t speedA = collision.m_objectA->getSpeed(); 
    speed_t speedB = collision.m_objectB->getSpeed();
    
    mass_t massA = collision.m_objectA->getMass();
    mass_t massB = collision.m_objectB->getMass();

    //speed colinear to normal, used for kinetic equation
    speed_t colSpeedA = Geometry::colinearPart(speedA, collision.m_normal);
    speed_t colSpeedB = Geometry::colinearPart(speedB, collision.m_normal);

    //speed not colinear to normal, conserved.
    speed_t nonColSpeedA = speedA - colSpeedA;
    speed_t nonColSpeedB = speedB - colSpeedB;
    
    speed_t newColSpeedA = glm::vec3(0.0f);
    speed_t newColSpeedB = glm::vec3(0.0f);
    
    //computing new speeds
    if(!(isinf(massA) || isinf(massB))){
        newColSpeedA = (colSpeedA*(massA - massB) + 2.0f*massB*colSpeedB)/(massA + massB);
        newColSpeedB = (colSpeedB*(massB - massA) + 2.0f*massA*colSpeedA)/(massA + massB);
    }

    speed_t newSpeedA = newColSpeedA + nonColSpeedA;
    speed_t newSpeedB = newColSpeedB + nonColSpeedB;


    collision.m_objectA->setSpeed(newSpeedA);
    collision.m_objectB->setSpeed(newSpeedB);

    collision.m_objectA->collisionEffect(collision, collision.m_objectB);
    collision.m_objectB->collisionEffect(collision, collision.m_objectA);

    auto ownerA = collision.m_objectA->getOwner().lock();
    auto ownerB = collision.m_objectB->getOwner().lock();
    
    ownerA->getPhysicBody();

    eventQueue->addEvent(std::make_shared<CollisionEvent>(
            0.0f, //has to be changed #FIXME use the real time here please, this one is irrelevant
            ownerA,
            ownerB,
            collision.m_normal
        ));
    eventQueue->addEvent(std::make_shared<CollisionEvent>(
            0.0f, //has to be changed #FIXME use the real time here please, this one is irrelevant
            ownerB,
            ownerA,
            collision.m_normal
        ));

    
}

Collider::Collider(std::shared_ptr<CoreEventQueue> eventQueue):
m_bodies(),
m_eventQueue(eventQueue)
{

}

Collider::~Collider()
{
    //nothing to do
}



/** simulate forward for elapsed time */
void Collider::simulateForward(timeInterval_t elapsed)
{

    float gravity = -10.0f;
    //gravity
    for(auto po = m_bodies.begin(); po != m_bodies.end(); ++po)
    {
      if(!isinf((*po)->getMass())){
        (*po)->addForce(std::make_shared<Impulse>(glm::vec3(0.0f, gravity*(*po)->getMass(), 0.0f), elapsed));
      }
    }
    //update speed of all objects
    for(auto obj = m_bodies.begin(); obj != m_bodies.end(); ++obj){
        (*obj)->applyForces(elapsed);
    }
    auto collisions = listCollisions(elapsed, m_bodies);
    //note, collisions are already time-sorted

    timeInterval_t timeLeft = elapsed;


    while(collisions->size() > 0)
    {
        //collisions are sorted

        Collision collision = collisions->front();
        collisions->pop_front();
        //we can do better here, like checking if the state of either
        //objects changes ?
        collisions->remove_if(CollisionFilter(collision));

        //advancing collisions until the position
        for(auto obj = m_bodies.begin(); obj != m_bodies.end(); ++obj){
            (*obj)->translate((*obj)->getSpeed() * (float)(collision.m_time* timeLeft)); 
        }
        //we also retime every collisions to match the new time interval
        for(auto col = collisions->begin(); col != collisions->end(); ++col){
            (*col).m_time *= (1-collision.m_time); 
        }
        //we reduce time left by the collision time
        timeLeft *= (1-collision.m_time);

        //computing physical response
        Collider::collisionResponse(collision, m_eventQueue);

        auto collisionsAB = listCollisions(collision.m_objectA, collision.m_objectB, m_bodies, timeLeft);
        collisions->merge(*(collisionsAB));
    }

    //now we advance all objects to the time left
    for(auto obj = m_bodies.begin(); obj != m_bodies.end(); ++obj){
        (*obj)->translate((*obj)->getSpeed() * (float)(timeLeft)); 
    }

}

/** adds a physic body to the collider */
void Collider::addPhysicBody(std::shared_ptr<PhysicBody> body)
{
    m_bodies.push_back(body);
}
    
/** removes a physic body from the collider */
void Collider::removePhysicBody(std::shared_ptr<PhysicBody> body)
{
    auto bod = std::find(m_bodies.begin(), m_bodies.end(), body);
    if(bod != m_bodies.end())
    {
        m_bodies.erase(bod);
    }

}
    
/** return true if the body is registered in the collider */
bool Collider::isPhysicBodyPresent(std::shared_ptr<PhysicBody> body)
{
    return  m_bodies.end() != std::find(m_bodies.begin(), m_bodies.end(), body);   
}

} //namespace nde