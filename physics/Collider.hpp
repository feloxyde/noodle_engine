/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <glm/glm.hpp>

//#include <glm/gtx/transform.hpp>
#include "../utils/ndetypes.hpp"
#include <list>
#include <vector>
#include <memory>

#ifndef NDE_COLLIDER_HPP
#define NDE_COLLIDER_HPP

namespace nde {

class PhysicBody;
class CoreEventQueue;

typedef  std::vector<std::shared_ptr<PhysicBody>> PhysicBodyVector_t;

/** this class represents a collision : two objects, a time, and a normal
note that the time is relative to the collider algorithm and does not refer
to the general time of the engine. Please note that it is used internally and
not meant to be used outside of collider */
struct Collision
{
    Collision(
        std::shared_ptr<PhysicBody> objectA,
        std::shared_ptr<PhysicBody> objectB,
        collisionTime_t time,
        glm::vec3 normal):
    m_objectA(objectA),
    m_objectB(objectB),
    m_time(time),
    m_normal(normal)
    {}


    virtual ~Collision(){}

    std::shared_ptr<PhysicBody> m_objectA;
    std::shared_ptr<PhysicBody> m_objectB;
    collisionTime_t m_time;
    glm::vec3 m_normal;

    bool operator <(Collision const& B)
    {
        return this->m_time < B.m_time;
    }

    bool operator ==(Collision const& B)
    {
        return (this->m_objectA == B.m_objectA 
        && this->m_objectB == B.m_objectB)
        || (this->m_objectA == B.m_objectB
        && this->m_objectB == B.m_objectA);
    }
};

typedef std::list<Collision> CollisionList_t;

/** This class is a Collider, running the physics simulation
*
*   It is supposed to be handled by the Game class, and not 
*   to be used standalone.
*/
class Collider 
{
public:
    static void collisionResponse(Collision const& collision, std::shared_ptr<CoreEventQueue> eventQueue);

public:
    Collider(std::shared_ptr<CoreEventQueue> eventQueue);

    virtual ~Collider();

    /** simulate forward for elapsed time */
    void simulateForward(timeInterval_t elapsed);

    /** adds a physic body to the collider */
    void addPhysicBody(std::shared_ptr<PhysicBody> body);
    
    /** removes a physic body from the collider */
    void removePhysicBody(std::shared_ptr<PhysicBody> body);
    
    /** return true if the body is registered in the collider */
    bool isPhysicBodyPresent(std::shared_ptr<PhysicBody> body);

private:
    
    PhysicBodyVector_t m_bodies;
    std::shared_ptr<CoreEventQueue> m_eventQueue;
};

} //namespace nde

#endif //NDE_COLLIDER_HPP