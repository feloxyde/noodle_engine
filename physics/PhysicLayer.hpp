/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_PHYSIC_LAYER_HPP
#define NDE_PHYSIC_LAYER_HPP
#include <memory>
namespace nde {

/** A class permitting to control wether physic bodies collides togeter or not,
*   by creating "layers"
*/
class PhysicLayer
{
public:
    /** Creates a layer,
    *   Non colliding rules have priority over colliding rules
    *
    *   @param selfColliding set to true if objects associated with this layer shall collide together
    *   @param collidingOthers set to true if objects associated with this layer shall collide
    *          with objects associated with other layers
    */
    PhysicLayer(bool selfColliding, bool collidingOthers);
    /** destroys the object */
    virtual ~PhysicLayer();
    /** check if objects associated with this layer can collide objects associated with another layer 
    *
    *   @param the other layer
    *
    *   @return true if objects associated with this layer can collide objects associated with another layer 
    */
    virtual bool collidesWith(std::shared_ptr<PhysicLayer> otherLayer);

    //virtual bool hasPhysicsResponseWith(std::shared_ptr<PhysicLayer> otherLayer);

private:
    bool m_isSelfColliding;
    bool m_isCollidingOthers;
};




} //namespace nde



#endif //NDE_PHYSIC_LAYER_HPP