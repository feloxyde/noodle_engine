/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_PHYSIC_BODY_HPP
#define NDE_PHYSIC_BODY_HPP

#include  "../utils/geometry.hpp"
#include "Body.hpp"
#include <memory>
#include <list>

namespace nde {

class Collider;
class Collision;
class Force;
class PhysicLayer;
class GameObject;


typedef std::list<std::shared_ptr<Force>> ForceList_t;

/** A body with added physics properties : mass and friction
*
*/
class PhysicBody : public Body
{
public:
    /** Creates a physic body 
    *   
    *   @param layer physic layer of the body
    *   @param frictionForce the friction multiplier of the body, will be combined with 
    *          one of other bodies when friction will be involved.
    */
    PhysicBody(std::shared_ptr<PhysicLayer> layer, float frictionForce = 1.0f);
    
    /** destroys the object */
    virtual ~PhysicBody();
    
    /** sets the collider of the object, call is automatic and handled by Game class
    *
    *   @param collider the collider
    */
    void setCollider(std::weak_ptr<Collider> collider);
    /** @return the collider associated with the body. Can be nullptr or invalid */
    std::weak_ptr<Collider> getCollider();
    
    /** returns true if the body collides can collide with other 
    *
    *   @param other another body
    *
    *   @return true if the two bodies may collide
    */
    bool collidesWith(std::shared_ptr<PhysicBody> other);

    /** @return physic layer associated with the object */
    std::shared_ptr<PhysicLayer> getLayer();

    /**  applies forces previously added to the object 
    *
    *   @param duration the frame duration, passed by collider
    */
    virtual void applyForces(timeInterval_t duration) = 0;

    /** adds a force to the body
    *
    *   @param force the force to add
    */
    virtual void addForce(std::shared_ptr<Force> force) = 0;

    /** applies effects on collision, for an immediate physics reaction
    *
    *   @param collision the collision involved, passed by collider
    *   @param other the other body involved in collision, passed by collider
    */
    virtual void collisionEffect(Collision const& collision, std::shared_ptr<PhysicBody> other) = 0;
    
    /** @return speed of the body */
    virtual speed_t getSpeed() = 0;
    /** @return mass of the body, may return +inf */
    virtual mass_t getMass() = 0;
    /** set speed of the body, do not call during physics simulation phase
    *
    *   @param speed the new speed
    */
    virtual void setSpeed(speed_t speed) = 0;

    /** @return owner of the body */
    std::weak_ptr<GameObject> getOwner();
   
    /** sets owner of the body, called automaticaly when registering game object if the body is the one owned by the object
    *   this relation serves to push collision events on game objects
    *
    *   @param owner owner of the object
    */
    void setOwner(std::weak_ptr<GameObject> owner);

    /** change the friction force of the object
    *
    *   @param frictionForce the new friction force
    */
    void setFrictionForce(float frictionForce);

    /** @return the friction force of the object */
    float getFrictionForce();

    
private: 
    std::weak_ptr<Collider> m_collider;
    std::shared_ptr<PhysicLayer> m_layer;
    std::weak_ptr<GameObject> m_owner;
    float m_frictionForce;
};



/** A physicbody with an effective mass, able to be influenced by forces */
class DynamicPhysicBody : public PhysicBody
{
public:
    /** Creates a DynamicPhysicBody
    *
    *   @param layer physic layer of the body
    *   @param mass the mass of the body
    *   @param frictionForce the friction multiplier of the body, will be combined with 
    *          one of other bodies when friction will be involved.
    */
    DynamicPhysicBody(std::shared_ptr<PhysicLayer> layer, mass_t mass, float frictionForce = 1.0f);
    
    /** destroys the object */
    virtual ~DynamicPhysicBody();

    /** please refer to PhysicBody documentation */
    virtual void applyForces(timeInterval_t duration);
    /** please refer to PhysicBody documentation */
    virtual void addForce(std::shared_ptr<Force> force);
    
    /** please refer to PhysicBody documentation, this object has no collision effect */
    virtual void collisionEffect(Collision const& collision, std::shared_ptr<PhysicBody> other);  


    /** please refer to PhysicBody documentation */
    virtual speed_t getSpeed();

    /** please refer to PhysicBody documentation */
    virtual mass_t getMass();

    /** please refer to PhysicBody documentation */
    virtual void setSpeed(speed_t speed);

private:
    speed_t m_speed;
    mass_t m_mass;
    ForceList_t m_forces;
};

/** a body with infinite mass, not influenced by forces */
class StaticPhysicBody : public PhysicBody
{
public:
    /** Creates a StaticPhysicBody
    *
    *   @param layer physic layer of the body
    *   @param frictionForce the friction multiplier of the body, will be combined with 
    *          one of other bodies when friction will be involved.
    */
    StaticPhysicBody(std::shared_ptr<PhysicLayer> layer, float frictionForce);
    virtual ~StaticPhysicBody();

    /** this object is not influenced by forces, please refer to PhysicBody documentation */
    virtual void applyForces(timeInterval_t duration);
    /** discards force when added, please refer to PhysicBody documentation */
    virtual void addForce(std::shared_ptr<Force> force);
    /** setups friction force on the colliding object, please refer to PhysicBody documentation */
    virtual void collisionEffect(Collision const& collision, std::shared_ptr<PhysicBody> other);  
    /** @return glm::vec3(0.0f), please refer to PhysicBody documentation */
    virtual speed_t getSpeed();
    /** @return +inf, please refer to PhysicBody documentation */
    virtual mass_t getMass();
    /** please refer to PhysicBody documentation */
    virtual void setSpeed(speed_t speed);
};
} //namespace nde

#endif //NDE_PHYSICS_BODY_HPP