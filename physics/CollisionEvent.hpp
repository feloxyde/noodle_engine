/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_COLLISION_EVENT_HPP
#define NDE_COLLISION_EVENT_HPP


#include "../core/CoreEvent.hpp"
#include <glm/glm.hpp>


namespace nde {

class GameObject;

/** This is a CollisionEvent, triggered when two objects collide each other
*
*   two objects are setups  : 
*   One target, that is the object involved in the collision potentially receiving the event if a bouncer has been set,
*   One colliding object, which is the other object involved in the collision.
*
*   For each collision, two CollisionEvents are triggered, one per object involved. 
*
*   please note that as for now, every collision event have a timepoint set at 0.0f
*
*
*/
class CollisionEvent : public TargetedCoreEvent
{
public:
    /** creates a collision event, this is not supposed to be called directly, since used only by the collider */
    CollisionEvent(timePoint_t timePoint, std::shared_ptr<GameObject> target ,std::shared_ptr<GameObject> collidingObject, glm::vec3 normal);
    
    /** destroys the object */
    virtual ~CollisionEvent();

    /** @return the object colliding with the target of the event */
    std::shared_ptr<GameObject> getCollidingObject();

    /** @return the normal of the collision */
    glm::vec3 getNormal();

private:
    std::shared_ptr<GameObject> m_collidingObject;
    glm::vec3 m_normal;
};

} //namespace nde


#endif //NDE_COLLISION_EVENT_HPP