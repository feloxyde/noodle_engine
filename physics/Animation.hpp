/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_ANIMATION_H
#define NDE_ANIMATION_H

#include "../utils/ndetypes.hpp"
#include  "../utils//geometry.hpp"
#include <glm/gtc/quaternion.hpp>
#include <glm/glm.hpp>
#include "Body.hpp"
#include <list>
#include <vector>
#include <set>

//#FIXME a "attachpoint" will be needed in transformables, that will be at
//the same time position and rotcenter.

namespace nde {
/** position in a body sense, meaning the combination between orientation and
*   position in geometrical sense
*
*/
class Pose {

public:
  Pose(bodyPartID_t target);
  virtual ~Pose();

  virtual void applyToBody(Body& body, timeInterval_t interpRatio) = 0;

  bodyPartID_t target();

private:
  bodyPartID_t m_target;
};

/** a position geometrical, respectively to reference frame */
class RelativePosition : public Pose {

public:
  RelativePosition(bodyPartID_t target, glm::vec3 const& position);
  virtual ~RelativePosition();

  virtual void applyToBody(Body& body, timeInterval_t interpRatio);

private:
  glm::vec3 m_position;
};

/** a relative orientation, relatively to the reference frame */
class RelativeOrientation : public Pose {

public:
  RelativeOrientation(bodyPartID_t target, glm::quat const& orientation);
  virtual ~RelativeOrientation();

  virtual void applyToBody(Body& body, timeInterval_t interpRatio);

private:
  glm::quat m_orientation;
};


/** represents a list of parts and their relative position */
class KeyFrame {

public:
  KeyFrame();
  virtual ~KeyFrame();

  /** adds a position to the keyframe */
  void addPose(Pose* position);

  /** applies position to the body, using an interpolation ratio and a ignored parts set.
  *
  * the interpolation ratio represents the part of the actual interpolation to apply.
  * 0 means no change, 1 means reach the position, 0.5 means half etc.
  * It can be tricky in the way it is relative to the actual position.
  *
  * all positions targeting a part that is listed in the ignored will not be applied.
  */
  void applyToBody(Body& body, timeInterval_t interpRatio, std::set<bodyPartID_t> const& ignoredParts);

  /** return a set containing the values of the affected parts */
  std::set<bodyPartID_t> affectedParts();

private:
  std::vector<Pose*> m_poses; //to delete ??
};


/** represents a succession of keyframes with time interval between them
*   and an interpolation mode (linear, square etc)
*
*/


class Animation {
public:

  Animation();
  virtual ~Animation();

  //adds a frame to the animation and returns the rank in which she is added
  frameRank_t addFrame(KeyFrame* frame, frameRank_t next, timeInterval_t reachTime);

  KeyFrame* getFrame(frameRank_t rank);
  frameRank_t getNext(frameRank_t rank);
  timeInterval_t getReachTime(frameRank_t rank);

  frameRank_t maxRank();

private:
  std::vector<KeyFrame*> m_frames; //to delete ??, list of the frames
  std::vector<frameRank_t> m_nexts; //frame after the frame
  std::vector<timeInterval_t> m_reachTimes; //time to reach the frame
  //std::vector<interpolationMode_t> m_interpModes;

};


/** a class meant to weild the parameters of
* an Animation
*
*
*/

/*
class AnimationHeader {
public:



private:

};
*/

/** A class that reads and play animations


class AnimationReader {



  void read(timeInterval_t elapsed);



};


*/


} //nde namespace
#endif //NDE_ANIMATION_H
