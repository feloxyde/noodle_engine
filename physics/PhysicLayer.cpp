/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "PhysicLayer.hpp"

namespace nde {

PhysicLayer::PhysicLayer(bool selfColliding, bool collidingOthers): 
m_isSelfColliding(selfColliding),
m_isCollidingOthers(collidingOthers)
{}

PhysicLayer::~PhysicLayer()
{
    //nothing to do
}

bool PhysicLayer::collidesWith(std::shared_ptr<PhysicLayer> otherLayer)
{
    if(otherLayer.get() == this){
        if(m_isSelfColliding){
            return true;
        }
    } else if(m_isCollidingOthers){
        return true;
    }

    return false;
}

} //namespace nde