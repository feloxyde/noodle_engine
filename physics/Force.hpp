/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_FORCE_HPP
#define NDE_FORCE_HPP

#include "../utils/ndetypes.hpp"
#include <glm/glm.hpp>

namespace nde  {

/** this class defines the abstraction of a Force, that can have constraints and duration
*
*   It is meant to be inherited from when implementing new kind of forces
*/
class Force 
{
public:
    /** Force constructor */
    Force();
    /** destroys the object */
    virtual ~Force();

    /** returns true if the force is currently resisting to the speed on the axis
    *   meaning the force shall use resistForce into the computation 
    *   all parameters are passed by the PhysicBody when applying forces to itself
    *   
    *   @param currentSpeed the current speed of the object
    *   @param axis the axis 0 = x , 1 = y or 2 = z on which the test should occur
    *
    *   @return wether the force is resistive or not on the axis
    **/
    virtual bool isResist(speed_t currentSpeed, axis_t axis) = 0;
    
    /** reduces lifespawn of the force and returns wether it will be alive or not at end of timeframe
    *
    *   @param elapsed time of the simulation frame, passed by PhysicBody when applying the force
    *
    *   @return true if the force is still alive at the end of the frame
    */
    virtual bool reduceTime(timeInterval_t elapsed) = 0;

    /** returns the amount of the force on the given axis, for the given object speed
    *   all parameters are passed by the PhysicBody when applying forces to itself
    *
    *   @param objectSpeed the speed of the object
    *   @param axis the axis 0 = x , 1 = y or 2 = z on which the force should apply
    *   @param elapsed time of the simulation frame
    *
    *   @return the value of the force on the axis
    */
    virtual float getForce(speed_t objectSpeed, axis_t axis, timeInterval_t elapsed) = 0;
    
    /** returns the amount of the resistive force on the given axis, for the given object speed
    *   all parameters are passed by the PhysicBody when applying forces to itself
    *
    *   @param forceSum sum of the non resistive forces applied to the object, minus already applied resistive forces
    *   @param axis the axis 0 = x , 1 = y or 2 = z on which the force should apply
    *   @param elapsed time of the simulation frame
    *
    *   @return the value of the force on the axis
    */
    virtual float getResistForce(glm::vec3 forceSum, axis_t axis, timeInterval_t elapsed) = 0;

    /** Returns the time before the force goes into resistive mode on the given axis
    *
    *   @param acceleration acceleration of the object on the set axis
    *   @param currentSpeed speed of the object on the set axis
    *   @param elapsed time of the simulation frame
    *
    *   @return time before the force goes into resistive mode on the set axis
    */
    virtual float getTimeToResist(float acceleration, float currentSpeed, axis_t axis) = 0;
};

/** This is a force applied constantly over time, an Impulse
*   please note that the Impulse word is innacurate and will be changed later to ForceOverTime
*/
class Impulse : public Force
{
public: 
    /** Creates an impulse
    *   
    *   @param force the amount of force
    *   @param timeleft the timespan on which the force is applied
    */
    Impulse(glm::vec3 force, timeInterval_t timeleft);
    /** destroys the object */
    virtual ~Impulse();

    /** this force is never resistive */
    virtual bool isResist(speed_t currentSpeed, axis_t axis);

    virtual bool reduceTime(timeInterval_t elapsed);
    /** this force is independent from speed */
    virtual float getForce(speed_t objectSpeed, axis_t axis, timeInterval_t elapsed);
    /** this force is never resistive */
    virtual float getResistForce(glm::vec3 forceSum, axis_t axis, timeInterval_t elapsed);
    /** this force is never resistive */
    virtual float getTimeToResist(float acceleration,float currentSpeed, axis_t axis);

private:
    glm::vec3 m_force;
    timeInterval_t m_timeleft;
}; 

/** represents a force acting to reach/maintain a speed alongside a plane. the speed
* should be included in the plane. 
* it is particularly useful for creating friction effects
*/
class PlaneSpeedForce : public Force
{
public:
    /** Create plane speed force
    *   
    *   @param targetSpeed speed the force will try to reach/maintain
    *   @param planeNormal the normal of the plane of resisting. dot product between targetSpeed and normal shall be 0
    *   @param force maximal length value of the resulting force vector
    */
    PlaneSpeedForce(speed_t targetSpeed, glm::vec3 planeNormal, float force);
    virtual ~PlaneSpeedForce();
    
    /** this force can be resistive if the body speed matchs the target speed */
    virtual bool isResist(speed_t currentSpeed, axis_t axis);

    virtual bool reduceTime(timeInterval_t elapsed);
    /** this force is non resistive when the speed of the body differs from the target speed */
    virtual float getForce(speed_t objectSpeed, axis_t axis, timeInterval_t elapsed);
     /** this force can be resistive if the body speed matchs the target speed */
    virtual float getResistForce(glm::vec3 forceSum, axis_t axis, timeInterval_t elapsed);
    /** this force will resist when the body will reach the target speed */
    virtual float getTimeToResist(float acceleration, float currentSpeed, axis_t axis);
private:
    speed_t m_targetSpeed;
    glm::vec3 m_planeNormal;
    float m_force;
};


} //namespace nde

#endif //NDE_FORCE_HPP