/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_BODY_HPP
#define NDE_BODY_HPP

#include  "../utils/geometry.hpp"
#include <glm/glm.hpp>
#include <memory>
namespace nde {

/** this class represents the abstraction of a component of a mesh.
*   it can be either a simple mesh part or a composed
*/

class BoundingBox;

/** a part of a body */
class BodyPart : public Transformable {

public:
  /** Creates a body part
  *
  *   @param box the bounding box associated with the body part, will get
  *          the transformations applied to the body applied to it as well.
  *   @param referenceFrame the reference frame of the part, as in Transformable class
  */  
  BodyPart(BoundingBox* box, std::weak_ptr<Transformable> referenceFrame);

  /** destroys the bodypart */
  virtual ~BodyPart();

  /** @return the bounding box of the body part */
  BoundingBox& getBox();

protected:
  /** transforms the body of the transformable if he has one */
  virtual void bodyTransform(glm::mat4 const& transformation);
  /** translate the body of the transformable if he has one */
  virtual void bodyTranslate(glm::vec3 const& translation);

private:

  BoundingBox* m_box;

};

/** represents a bodyopart extended by other bodyparts */
class ExtendedBodyPart : public BodyPart {

public:
  /** creates an extended body part, see the constructor of BodyPart for more information */
  ExtendedBodyPart(BoundingBox* box, std::weak_ptr<Transformable> referenceFrame);
  /** destroys the extended body part */
  virtual ~ExtendedBodyPart();
  
  /** set up the extensions of the body part, and set this as the reference frame 
  *
  * @param extensions the collection of bodyparts to add to the part. 
  * A part shall not add itself, directly or not, as extension
  */
  virtual void setExtensions(std::vector<std::shared_ptr<BodyPart>>& extensions);

protected:
  /** transforms the body of the transformable if he has one */
  virtual void bodyTransform(glm::mat4 const& transformation);
  /** translate the body of the transformable if he has one */
  virtual void bodyTranslate(glm::vec3 const& translation);

private:
  std::vector<std::shared_ptr<BodyPart>> m_extensions; //to delete !
};


/** represents a body, composed of several bounding boxes that can be accessed via indices */
class Body : public Transformable {
public:
  /** computes a static collision test between two bodies
  *   
  *   @param body1 the first body
  *   @param body2 the second body
  *   @param extractor (out) if the test returns true, value is the shortest transformation to get body1 out of body2
  *
  *   @return true if body1 and 2 are colliding, false otherwise
  */
  static bool staticCollisionTest(Body& body1, Body& body2, glm::vec3& extractor);

  /** Computes a swept collision test. The two bodies are supposed non colliding statically 
  *
  *   @param body1 the first body 
  *   @param move1 the movement of the first body to sweep on
  *   @param body2 the second body
  *   @param move2 the movement of the second body to sweep on
  *   @param timeIn (out) if collision occurs, the ratio (0>=, <=1) of the movement at witch bodies collides 
  *   @param timeOut (out) if collision occurs, the ratio (0>=) of the movement at which bodies stop colliding
  *   @param normal (out) if collison occurs, the normal of the collision
  *   @param part1 (unused)
  *   @param part2 (unused)
  *
  *   @return true if body1 and 2 are colliding, false orthewise
  */
   static bool sweptCollisionTest(Body& body1, glm::vec3& move1,
                             Body& body2, glm::vec3& move2,
                             collisionTime_t& timeIn, collisionTime_t& timeOut,
                             glm::vec3& normal, bodyPartID_t& part1, bodyPartID_t& part2);

public:

    /** Creates a body */
    Body();
    /** destroys the body */
    virtual ~Body();

    /** adds a part to the body and returns its ID 
    *   
    *   @param part the part to add
    *   @param asRoot set it to true if the part is root, meaning that it shall be transform along witht the body
    *
    *   @return the ID of the part in the body
    */
    bodyPartID_t addPart(std::shared_ptr<BodyPart> part, bool asRoot = false);

    /** same as addPart, with asRoot parameter set to true */
    bodyPartID_t addRootPart(std::shared_ptr<BodyPart> part); //just a shortcut for addPart(part, true);

    /** returns the part with the given ID 
    *
    *   @param partID id of a part previously added
    *
    *   @return the part corresponding to the ID
    */
    std::shared_ptr<BodyPart> getPart(bodyPartID_t partID);

    /** @return all parts of the body */
    std::vector<std::shared_ptr<BodyPart>>& getParts();

    /** @returns the tinyest AABB englobing the whole body */
    AxisAlignedBoundingBox asAxisAlignedBoundingBox();

protected:
  /** transforms the body of the transformable if he has one */
  virtual void bodyTransform(glm::mat4 const& transformation);
  /** translate the body of the transformable if he has one */
  virtual void bodyTranslate(glm::vec3 const& translation);

private:
  std::vector<std::shared_ptr<BodyPart>> m_parts; //all the parts are listed here, and accessed by indices
  std::vector<std::shared_ptr<BodyPart>> m_rootParts; //the list of parts that are to transform when transforming the Mesh
  AxisAlignedBoundingBox m_AABB;
  bool m_validAABB;
};

}

#endif //NDE_BODY_HPP
