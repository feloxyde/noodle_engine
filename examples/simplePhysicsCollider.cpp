/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

//#include <GL/gl3.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../devtools.hpp"
#include "../graphics/ndeGL.hpp"
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "../graphics/Model.hpp"
#include <vector>
#include <glm/gtc/quaternion.hpp>
#include "../utils/geometry.hpp"
#include <cmath>
#include "../physics/Body.hpp"
#include "../graphics/View.hpp"
#include "../graphics/GraphicObject.hpp"
#include "../interface/Screen.hpp"
#include "../graphics/Renderer.cpp"
#include "../physics/Animation.hpp"
#include <set>
#include "../utils/Human.hpp"
#include <algorithm>
#include <list>
#include "../physics/Collider.hpp"
#include "../physics/PhysicBody.hpp"
#include "../physics/Force.hpp"
#include "../physics/PhysicLayer.hpp"
#include "../core/CoreEventQueue.hpp"
#include "../core/GameObject.hpp"

using namespace nde;
using namespace std;
using namespace glm;

static double xpos_old(0.0), ypos_old(0.0);
static double xmov(0.0), ymov(0.0);
static bool mousemoved = false;

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
  xmov = xpos - xpos_old;
  ymov = ypos - ypos_old;
  xpos_old = xpos;
  ypos_old = ypos;
/*
  if(xmov > 50.0){
    xmov = 0.0;
  }
  if(xmov < -50.0){
    xmov = -0.0;
  }
  if(ymov > 50.0){
    ymov = 0.0;
  }
  if(ymov < -50.0){
    ymov = 0.0;
  }
*/
  mousemoved = true;
}

bool mleft(false), mright(false);

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

  switch (button) {
    case GLFW_MOUSE_BUTTON_2 :
      if(action == GLFW_PRESS) mright = true;
      if(action == GLFW_RELEASE) mright = false;
  }

}

static double xAxis(0), yAxis(0), zAxis(0);

bool play = false;

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (key)
    {
          case GLFW_KEY_ESCAPE : if( action == GLFW_PRESS)
              glfwSetWindowShouldClose(window, GLFW_TRUE);
              break;

          case GLFW_KEY_W : if(action == GLFW_PRESS) zAxis -= 1.0;
                            else if (action == GLFW_RELEASE) zAxis += 1.0;
                            break;

          case GLFW_KEY_S : if(action == GLFW_PRESS) zAxis += 1.0;
                            else if (action == GLFW_RELEASE) zAxis -= 1.0;
                            break;

          case GLFW_KEY_D : if(action == GLFW_PRESS) xAxis += 1.0;
                            else if (action == GLFW_RELEASE) xAxis -= 1.0;
                            break;

          case GLFW_KEY_A : if(action == GLFW_PRESS) xAxis -= 1.0;
                            else if (action == GLFW_RELEASE) xAxis += 1.0;
                            break;

          case GLFW_KEY_Q : if(action == GLFW_PRESS) play = !play;
                            break;

          case GLFW_KEY_SPACE : if(action == GLFW_PRESS) yAxis += 1.0;
                            else if (action == GLFW_RELEASE) yAxis -= 1.0;
                            break;

          case GLFW_KEY_LEFT_SHIFT : if(action == GLFW_PRESS) yAxis -= 1.0;
                            else if (action == GLFW_RELEASE) yAxis += 1.0;
                            break;
    }

}

struct SimplePhysicsObject
{
    SimplePhysicsObject(
        string n,
        nde::length_t width, nde::length_t height, nde::length_t depth,
        shared_ptr<Shader> shader,
        vec3 color,
        vec3 initialPos,
        mass_t mass = 1.0f,
        bool isStatic = false):
      body(),
      graphicObject(),
      name(n),
      boxID()
    {
        if(isStatic){
          body = std::make_shared<StaticPhysicBody>(
              std::make_shared<PhysicLayer>(true, true),
              mass);
        }
        else {
          body = std::make_shared<DynamicPhysicBody>(
              std::make_shared<PhysicLayer>(true, true),
              mass);
        }
        ///MEMORY LEAK HERE !!!, until box is made shared into body part
        auto obb = new OrientedBoundingBox(width, height, depth);
        auto bodyPart = std::make_shared<BodyPart>(obb, this->body);
        this->boxID = body->addRootPart(bodyPart);
        shared_ptr<Model> model = generateBoxModel(this->body->getPart(boxID)->getBox(), color);
        graphicObject = make_shared<AttachedGraphicObject>(shader, model, nullptr, this->body);
        this->body->reposition(initialPos);
        this->gameObject = make_shared<GameObject>();
        this->body->setOwner(gameObject);

    }

    shared_ptr<GameObject> gameObject; //empty game object
    shared_ptr<PhysicBody> body;
    shared_ptr<GraphicObject> graphicObject;
    std::string name;
    bodyPartID_t boxID;
};


int main()
{

  SimpleScreen screen = SimpleScreen(0);

  glfwSetCursorPosCallback(screen.getWindow(), cursor_position_callback);
  glfwSetMouseButtonCallback(screen.getWindow(), mouse_button_callback);
  glfwSetKeyCallback(screen.getWindow(), key_callback);


  glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
  glEnable(GL_DEPTH_TEST);

  string sname = "monochrome3D";
  auto chrome = Shader::loadShader(sname);
  //GLuint programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
//  glEnable(GL_TEXTURE_2D);
//  unsigned i = 0;
/*
  mat4 model = mat4(1.0f);
  Camera cam(vec3(5,0,5),vec3(0,0,0),vec3(0,1,0));
  mat4 view = cam.lookAt();
  Projection proj(70.0f, (float)fulls.width()/(float)fulls.height(), 0.1f, 100.0f );
  mat4 projection = proj.perspective();
  mat4 MVP = projection*view*model;
*/
  View& view = *(screen.getView());

  GlobalRenderer globalRenderer;
  view.watch(&globalRenderer);

  
  vec3 yellow(1.0, 1.0, 0.0);
  vec3 blue(0.0, 0.0, 1.0);
  vec3 green(0.0, 1.0, 0.0);
  vec3 red(1.0, 0.0, 0.0);
  vec3 pink(1.0, 0.0, 1.0);
  vec3 gray(0.7, 0.7, 0.7);

  Collider collider(std::make_shared<CoreEventQueue>());

  vector<shared_ptr<SimplePhysicsObject>> physicsObjects;

  physicsObjects.push_back(make_shared<SimplePhysicsObject>(
    "map",
    200.0, 0.1, 200.0,
    chrome, gray,
    vec3(0.0, -2.0, 0.0),
    0.1f, true
    ));



  /* create physics object here */
  physicsObjects.push_back(make_shared<SimplePhysicsObject>(
    "yellow",
    1.0, 1.0, 1.0, 
    chrome, yellow, 
    vec3(3.0, 6.0, -6.0)
    ));

  physicsObjects[1]->body->addForce(std::make_shared<Impulse>(vec3(-0.5, 0.0, 2.0), 1.0f));


  physicsObjects.push_back(make_shared<SimplePhysicsObject>(
    "red",
    1.0, 1.0, 1.0, 
    chrome, red, 
    vec3(0.0, 6.0, 0.0)
    ));
  physicsObjects[2]->body->addForce(std::make_shared<Impulse>(vec3(0.0, 0.0, 1.0), 1.0f));


  physicsObjects.push_back(make_shared<SimplePhysicsObject>(
    "pink",
    1.0, 1.0, 1.0, 
    chrome, pink, 
    vec3(-3.0, 6.0, 0.0)
    ));
  physicsObjects[3]->body->addForce(std::make_shared<Impulse>(vec3(0.5, 0.0, 1.0), 1.0f));


  //non moving object, acts as a visual reference.
  physicsObjects.push_back(make_shared<SimplePhysicsObject>(
    "blue",
    1.0, 1.0, 1.0, 
    chrome, blue, 
    vec3(0.0, 6.0, 6.0)
    ));
  
  //no impulse cause not moving at first
  //physicsObjects[4]->body->addForce(Impulse(vec3(0.0, 0.0, 0.0), 0.0f));


  /* end physics object creation block */
  for(auto po = physicsObjects.begin(); po != physicsObjects.end(); ++po)
  {
    globalRenderer.referGraphicObject((*po)->graphicObject);
    collider.addPhysicBody((*po)->body);
  }

  std::cout << "created all the bodies" << std::endl;


float gap = 10.0;
  unsigned fcount = 0;
  double seconds = 0.0;
  bool reset = false;
/*
double time1 = 2.0;
double time2 = 2.0;
double elapsed = 0.0;
bool f1 = false;
*/

timeInterval_t elapsed = 0.0;
unsigned currentRank = 0;
double timeLeft = 0.0;
double timeToReach = 0.0;

float gravity = -10.0f;
float mouseSensitivity = 0.15f;

while(screen.shouldNotQuit()){

    //updating objects
    //add gravity to the objects :
    if(play){


    collider.simulateForward(seconds);
    }
    screen.update(0.0); //for now we dont care about time
    glfwPollEvents();

    if(mousemoved && fcount > 3){
      view.camera().rotateHorizontally(-(((float)xmov)*seconds*mouseSensitivity));
      view.camera().rotateVertically((((float)ymov)*seconds*mouseSensitivity));
      mousemoved = false;
    }

    view.camera().moveForward(vec3(-xAxis/10, yAxis/10 , -zAxis/10 ));
    seconds = glfwGetTime(); //getting time
    glfwSetTime(0.0); //reset


  fcount++;
  }


return EXIT_SUCCESS;
}
