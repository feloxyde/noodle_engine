/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/


/* this example expects a .frag and .vert shader in order to run */
/*
HOW to use : 
the qwsd and shit/space to move camera around
the mouse to orient camera
the arrow to have a tree state selection of plane rotation :
  State 0 : left/right will select Z axis, up will select X and down will select Y
  State 1 : left will select plane -1 along axis, up/down 0 and right 1
  State 2 : left/down will rotate negative(clockwise), up/right will rotate positive (counterclockwise)
*/


#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../devtools.hpp"
#include "../graphics/ndeGL.hpp"
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "../graphics/Model.hpp"
#include <vector>
#include <glm/gtc/quaternion.hpp>
#include "../utils/geometry.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include "../graphics/View.hpp"
#include "../graphics/GraphicObject.hpp"
#include "../interface/Screen.hpp"
#include "../graphics/Renderer.cpp"
#include <glm/gtx/rotate_vector.hpp>
#include <memory>

class RubixModel  
{
public:
  static constexpr GLfloat CUBE_VERTICES[] = {
    // ABCD face
    -0.5f, -0.5f, -0.5f, //A
    0.5f, -0.5f, -0.5f, //B
    0.5f, -0.5f, 0.5f, //C
    -0.5f, -0.5f, 0.5f, //D

    //EFGH face (+4)
    -0.5f, 0.5f, 0.5f, //E
    0.5f, 0.5f, 0.5f, //F
    0.5f, 0.5f, -0.5f, //G
    -0.5f, 0.5f, -0.5f,  //H

    //ABGH (+8)
    -0.5f, -0.5f, -0.5f, //A
    0.5f, -0.5f, -0.5f, //B
    0.5f, 0.5f, -0.5f, //G
    -0.5f, 0.5f, -0.5f,  //H
    
    //CDEF (+12)
     0.5f, -0.5f, 0.5f, //C
    -0.5f, -0.5f, 0.5f, //D
    -0.5f, 0.5f, 0.5f, //E
    0.5f, 0.5f, 0.5f, //F
    
    //BCFG (+16)
    0.5f, -0.5f, -0.5f, //B
    0.5f, -0.5f, 0.5f, //C
    0.5f, 0.5f, 0.5f, //F
    0.5f, 0.5f, -0.5f, //G

    //ADHE (+20)
    -0.5f, -0.5f, -0.5f, //A
    -0.5f, -0.5f, 0.5f, //D
    -0.5f, 0.5f, -0.5f,  //H
    -0.5f, 0.5f, 0.5f //E
  };

  static constexpr GLuint CUBE_INDEXES[] = {
    0, 1, 2, 0, 2, 3, //ABC, ACD
    3+4, 0+4, 1+4, 3+4, 2+4, 1+4,  //HEF, HGF (+4)
    0+8, 3+8, 1+8, 2+8, 3+8, 1+8, //AHB, GHB (+8)
    2+12, 1+12, 0+12, 2+12, 3+12, 0+12, //EDC, EFC //(+12)
    3+16, 0+16, 1+16, 3+16, 2+16, 1+16, //GBC, GFC (+16)
    0+20, 1+20, 3+20, 0+20, 2+20, 3+20 //ADE, AHE (+20)
  };

  static constexpr GLfloat CUBE_TEXTURE_COORDINATES[] = {
    // ABCD face
    0.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    
    //EFGH face (+4)
    1.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 1.0f,
    
    //ABGH (+8)
    2.0f, 2.0f,
    2.0f, 2.0f,
    2.0f, 2.0f,
    2.0f, 2.0f,

    //CDEF (+12)
    3.0f, 3.0f,
    3.0f, 3.0f,
    3.0f, 3.0f,
    3.0f, 3.0f,

    //BCFG (+16)
    4.0f, 4.0f,
    4.0f, 4.0f,
    4.0f, 4.0f,
    4.0f, 4.0f,

    //ADHE (+20)
    5.0f, 5.0f,
    5.0f, 5.0f,
    5.0f, 5.0f,
    5.0f, 5.0f

  };

  constexpr static unsigned verticesByteSize = sizeof(CUBE_VERTICES);
  constexpr static unsigned indexesByteSize = sizeof(CUBE_INDEXES);
  constexpr static unsigned texCoordByteSize = sizeof(CUBE_TEXTURE_COORDINATES);
};

constexpr GLfloat RubixModel::CUBE_VERTICES[];
constexpr GLuint RubixModel::CUBE_INDEXES[];
constexpr GLfloat RubixModel::CUBE_TEXTURE_COORDINATES[];


static double xpos_old(0.0), ypos_old(0.0);
static double xmov(0.0), ymov(0.0);
static bool mousemoved = false;

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
  xmov = xpos - xpos_old;
  ymov = ypos - ypos_old;
  xpos_old = xpos;
  ypos_old = ypos;

  if(xmov > 50.0){
    xmov = 0.0;
  }
  if(xmov < -50.0){
    xmov = -0.0;
  }
  if(ymov > 50.0){
    ymov = 0.0;
  }
  if(ymov < -50.0){
    ymov = 0.0;
  }

  mousemoved = true;
}

bool mleft(false), mright(false);

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

  switch (button) {
    case GLFW_MOUSE_BUTTON_2 :
      if(action == GLFW_PRESS) mright = true;
      if(action == GLFW_RELEASE) mright = false;
  }

}

static double xAxis(0), yAxis(0), zAxis(0);

void rubix_rotate_plane(glm::vec3 axis, float plan, float direction);

static unsigned arrow_selector_state = 0;
static glm::vec3 arrow_selected_axis(0.0f, 0.0f, 0.0f);
static float arrow_selected_plane = 0.0f;
static float arrow_selected_direction = 0.0f;

static void arrow_cube_rotate_selection(int key)
{
  switch(arrow_selector_state){
    case 2:
        if(key == GLFW_KEY_LEFT || key == GLFW_KEY_DOWN) arrow_selected_direction = -1.0f;
        if(key == GLFW_KEY_UP || key == GLFW_KEY_RIGHT ) arrow_selected_direction = 1.0f;
        rubix_rotate_plane(arrow_selected_axis,  arrow_selected_plane,  arrow_selected_direction);
        arrow_selector_state = 0;
      break;
    case 1:
        if(key == GLFW_KEY_LEFT) arrow_selected_plane = -1.0f;
        if(key == GLFW_KEY_UP || key == GLFW_KEY_DOWN ) arrow_selected_plane = 0.0f;
        if(key == GLFW_KEY_RIGHT) arrow_selected_plane = 1.0f;
        arrow_selector_state++;
        break;
    default:
        if(key == GLFW_KEY_LEFT || key == GLFW_KEY_RIGHT) arrow_selected_axis = glm::vec3(0.0f, 0.0f, 1.0f);
        if(key == GLFW_KEY_UP) arrow_selected_axis = glm::vec3(1.0f, 0.0f, 0.0f);
        if(key == GLFW_KEY_DOWN) arrow_selected_axis = glm::vec3(0.0f, 1.0f, 0.0f);
        arrow_selector_state++;
      break;
  }
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (key)
    {
          case GLFW_KEY_ESCAPE : if( action == GLFW_PRESS)
              glfwSetWindowShouldClose(window, GLFW_TRUE);
              break;

          case GLFW_KEY_W : if(action == GLFW_PRESS) zAxis += 1.0;
                            else if (action == GLFW_RELEASE) zAxis -= 1.0;
                            break;

          case GLFW_KEY_S : if(action == GLFW_PRESS) zAxis -= 1.0;
                            else if (action == GLFW_RELEASE) zAxis += 1.0;
                            break;

          case GLFW_KEY_D : if(action == GLFW_PRESS) xAxis -= 1.0;
                            else if (action == GLFW_RELEASE) xAxis += 1.0;
                            break;

          case GLFW_KEY_A : if(action == GLFW_PRESS) xAxis += 1.0;
                            else if (action == GLFW_RELEASE) xAxis -= 1.0;
                            break;

          case GLFW_KEY_SPACE : if(action == GLFW_PRESS) yAxis += 1.0;
                            else if (action == GLFW_RELEASE) yAxis -= 1.0;
                            break;

          case GLFW_KEY_LEFT_SHIFT : if(action == GLFW_PRESS) yAxis -= 1.0;
                            else if (action == GLFW_RELEASE) yAxis += 1.0;
                            break;
          case GLFW_KEY_LEFT:
                            if(action == GLFW_PRESS)
                            arrow_cube_rotate_selection(key);
                            break;
          case GLFW_KEY_UP:
                            if(action == GLFW_PRESS)
                            arrow_cube_rotate_selection(key);
                            break;
          case GLFW_KEY_DOWN:
                            if(action == GLFW_PRESS)
                            arrow_cube_rotate_selection(key);
                            break;
          case GLFW_KEY_RIGHT:
                            if(action == GLFW_PRESS)
                            arrow_cube_rotate_selection(key);
                            break;
           
    }

}




typedef struct RubixPart {
  std::shared_ptr<nde::Transformable> position;
  std::shared_ptr<nde::Transformable> animationCurrentPos;
  std::shared_ptr<nde::GraphicObject> graphicObject;
  
  RubixPart(glm::vec3 pos, std::shared_ptr<nde::Model> model, std::shared_ptr<nde::Shader> shader):
    position(std::make_shared<nde::Transformable>()),
    animationCurrentPos(std::make_shared<nde::Transformable>()),
    graphicObject(std::make_shared<nde::AttachedGraphicObject>(shader, model, nullptr, animationCurrentPos))
    {
     animationCurrentPos->setPosition(pos);
     position->setPosition(pos);
    }

  bool isInPlane(glm::vec3 axis, float plane)
  {
    return (axis.x != 0.0f && std::round(plane) == std::round(getPosition().x))||
           (axis.y != 0.0f && std::round(plane) == std::round(getPosition().y))||
           (axis.z != 0.0f && std::round(plane) == std::round(getPosition().z));
  }

  glm::vec3 getPosition()
  {
    return position->getPosition();
  }

  void rotate(glm::vec3 axis, float direction)
  {
  //memorisation of old pos
  animationCurrentPos->reposition(getPosition());
  animationCurrentPos->reorient(position->getOrientation());
  
  //setting up new pos
  position->rotate(axis, direction*M_PI/2.0, glm::vec3(0.0f, 0.0f, 0.0f));

  //round the new position 
  glm::vec3 pos = getPosition();
  glm::vec3 newpos(std::round(pos.x), std::round(pos.y),std::round(pos.z));
  position->setPosition(newpos);
  }

  void animate(nde::timeInterval_t interpRatio)
  {
      //interpolating every objects to its next interpolation position
      glm::quat inter = glm::slerp(animationCurrentPos->getOrientation(), position->getOrientation(), (float)interpRatio);
      animationCurrentPos->rotate(nde::Geometry::reorient(animationCurrentPos->getOrientation(), inter), glm::vec3(0.0f, 0.0f, 0.0f));
  }


} RubixPart;



const size_t rubixSize = 3;
const size_t rubixElemCount = rubixSize*rubixSize*rubixSize;

RubixPart* rubix[rubixElemCount];

bool animationInProgress = false; //will be used to prevent multi plan rotating
nde::timeInterval_t animationTimeElapsed = 0.0f; 
const nde::timeInterval_t animationDuration = 0.7f; //1sec animation duration

void rubix_init(nde::Renderer& globalRenderer)
{
  std::string sname = "rubix3D";
  auto rubixShader = nde::Shader::loadShader(sname);
  auto model = nde::Model::createTexturedIndexedModel(
    RubixModel::CUBE_VERTICES, 
    RubixModel::verticesByteSize, 
    RubixModel::CUBE_TEXTURE_COORDINATES, 
    RubixModel::texCoordByteSize, 
    RubixModel::CUBE_INDEXES, 
    RubixModel::indexesByteSize);

  for (size_t x = 0; x < rubixSize; ++x)
    for (size_t y = 0; y < rubixSize; ++y)
      for (size_t z = 0; z < rubixSize; ++z){
        RubixPart* rubixPart = new RubixPart(glm::vec3((int)x - 1, (int)y - 1, (int)z - 1), model, rubixShader);
        rubix[x + y*rubixSize + z*rubixSize*rubixSize] = rubixPart;
        globalRenderer.referGraphicObject((rubixPart->graphicObject));
      }
}


/* rotates a plan of the rubix (8 cubes)
* axis : 0 = x , 1 = y, 2 = z
* plan : order of the plan (0, 1, 2) on the axis
* direction : +1 or -1, wether the rotation is + or - alongside axis
*/
void rubix_rotate_plane(glm::vec3 axis, float plan, float direction)
{
  if(!animationInProgress){
    for(size_t i = 0; i < rubixElemCount; ++i)
    {
      if(rubix[i]->isInPlane(axis, plan))
      {
        rubix[i]->rotate(axis, direction);
      }
    }
    animationInProgress = true;
    animationTimeElapsed = 0.0f;
  }
}

void rubix_animate(nde::timeInterval_t elapsed)
{ 
  //non optimised version, should eventually test for plan
  if(animationInProgress)
  {
    //calculating interpolation ratio :  
    nde::timeInterval_t interpRatio = 1.0f;
    if(animationDuration > elapsed + animationTimeElapsed)
    {
      interpRatio = elapsed / (animationDuration - animationTimeElapsed);
      animationTimeElapsed += elapsed;
    }
    for(size_t i = 0; i < rubixElemCount; ++i)
    {
      rubix[i]->animate(interpRatio);
    }
 
    if(interpRatio == 1.0f){ 
      animationInProgress = false;
    } 
  }
}



int main()
{

  nde::SimpleScreen screen = nde::SimpleScreen(0);

  glfwSetCursorPosCallback(screen.getWindow(), cursor_position_callback);
  glfwSetMouseButtonCallback(screen.getWindow(), mouse_button_callback);
  glfwSetKeyCallback(screen.getWindow(), key_callback);



  glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
  glEnable(GL_DEPTH_TEST);

  //GLuint programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
//  glEnable(GL_TEXTURE_2D);
//  unsigned i = 0;
/*
  glm::mat4 model = glm::mat4(1.0f);
  nde::Camera cam(glm::vec3(5,0,5),glm::vec3(0,0,0),glm::vec3(0,1,0));
  glm::mat4 view = cam.lookAt();
  nde::Projection proj(70.0f, (float)fulls.width()/(float)fulls.height(), 0.1f, 100.0f );
  glm::mat4 projection = proj.perspective();
  glm::mat4 MVP = projection*view*model;
*/
  nde::View& view = *(screen.getView());

  nde::GlobalRenderer globalRenderer;

  view.watch(&globalRenderer);

  rubix_init(globalRenderer);

float gap = 10.0f;
  unsigned fcount = 0;
  double seconds = 0.0f;
  double oldseconds = 0.0f;
  bool reset = false;
while(screen.shouldNotQuit()){

    screen.update(0.0f); //for now we dont care about

    glfwPollEvents();

    if(mousemoved && fcount > 3){
      view.camera().rotateHorizontally(-(xmov/550.0f));
      view.camera().rotateVertically((ymov/550.0f));
      mousemoved = false;
    }



    view.camera().moveForward(glm::vec3(xAxis/8, yAxis/8 , zAxis/8 ));

    seconds = glfwGetTime();
    
    rubix_animate((float(seconds - oldseconds)));

    oldseconds = seconds;


  fcount++;
  }


return EXIT_SUCCESS;
}
