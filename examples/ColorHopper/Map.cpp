/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Map.hpp"
#include "../../physics/PhysicBody.hpp"
#include "../../graphics/ndeGL.hpp"

void initMap()
{
  glm::vec3 yellow_c(1.0, 1.0, 0.0);
  glm::vec3 blue_c(0.0, 0.0, 1.0);
  glm::vec3 green_c(0.0, 1.0, 0.0);
  glm::vec3 red_c(1.0, 0.0, 0.0);
  glm::vec3 pink_c(1.0, 0.0, 1.0);
  glm::vec3 gray_c(0.7, 0.7, 0.7);

  std::string sname = "monochrome3D";
  auto chrome = nde::Shader::loadShader(sname);



  auto ground = std::make_shared<MapElement>(
    "floor",
    200.0, 0.1, 200.0,
    glm::vec3(0.0, -2.0, 0.0),
    gray_c,
    chrome
  );
  ground->registerToGame();


  auto pink = std::make_shared<MapElement>(
    "pink",
    1.0f, 3.0f, 1.0f,
    glm::vec3(0.0f, -1.0f, 0.0f),
    pink_c,
    chrome
  ); 
  pink->registerToGame();


  auto yellow = std::make_shared<MapElement>(
    "yellow",
    1.0f, 1.5f, 1.0f,
    glm::vec3(10.0f, -1.0f, 2.0f),
    yellow_c,
    chrome
  ); 
  yellow->registerToGame();

    auto blue = std::make_shared<MapElement>(
    "blue",
    3.0f, 5.0f, 3.0f,
    glm::vec3(-6.0f, -1.0f, 0.0f),
    blue_c,
    chrome
  ); 
  blue->registerToGame();

    auto green = std::make_shared<MapElement>(
    "green",
    1.0f, 10.0f, 1.0f,
    glm::vec3(0.0f, -1.0f, -5.0f),
    green_c,
    chrome
  ); 
  green->registerToGame();

    auto red = std::make_shared<MapElement>(
    "red",
    5.0f, 0.5f, 5.0f,
    glm::vec3(0.0f, 9.0f, 0.0f),
    red_c,
    chrome
  ); 
  red->registerToGame();




}

std::shared_ptr<nde::PhysicLayer> MapElement::mapPhysicLayer = std::make_shared<nde::PhysicLayer>(false, true);

MapElement::MapElement(std::string name, 
        nde::length_t width, nde::length_t height, nde::length_t depth,
        glm::vec3 initialPos, 
        glm::vec3 color,
        std::shared_ptr<nde::Shader> shader):
CubicObject::CubicObject(
    name,
    width, height, depth,
    color,
    initialPos,
    shader,
    mapPhysicLayer,
    1.0f,
    true,
    true)
{
    this->getPhysicBody()->setFrictionForce(6.0f);
}

MapElement::~MapElement()
{

}
