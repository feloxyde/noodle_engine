/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "CubicObject.hpp"
#include "../../devtools.hpp"
#include "../../physics/PhysicBody.hpp"
#include "../../graphics/GraphicObject.hpp"

CubicObject::CubicObject(std::string n, 
      nde::length_t width, nde::length_t height, nde::length_t depth,
      glm::vec3 color, 
      glm::vec3 initialPos,
      std::shared_ptr<nde::Shader> shader,
      std::shared_ptr<nde::PhysicLayer> layer,
      nde::mass_t mass,
      bool isStatic,
      bool isVisible):
      GameObject::GameObject()
{
  if(isStatic){
    m_body = std::make_shared<nde::StaticPhysicBody>(
      layer,
      mass);
  }
  else {
     m_body = std::make_shared<nde::DynamicPhysicBody>(
        layer,
        mass);
  }
        
  auto obb = new nde::OrientedBoundingBox(width, height, depth);
  auto bodyPart = std::make_shared<nde::BodyPart>(obb, m_body);
  nde::bodyPartID_t id = m_body->addRootPart(bodyPart);
  if(isVisible){
    std::shared_ptr<nde::Model> model = generateBoxModel(m_body->getPart(id)->getBox(), color);
    auto go = std::make_shared<nde::AttachedGraphicObject>(shader, model, nullptr, this->m_body);
    m_graphicObjects.push_back(go);
  }
  this->m_body->reposition(initialPos);
}
  
CubicObject::~CubicObject()
{

}
  
void  CubicObject::onFrameBegin(nde::timeInterval_t elapsed)
{
    //nothing to do
}
  

void  CubicObject::onFrameEnd(nde::timeInterval_t elapsed)
{
    //nothing to do
}
