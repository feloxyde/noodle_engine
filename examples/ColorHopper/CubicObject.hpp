/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORHOPPER_CUBIC_OBJECT_HPP
#define COLORHOPPER_CUBIC_OBJECT_HPP


#include "../../core/GameObject.hpp"
#include <memory>
#include <string>
#include <glm/glm.hpp>
#include "../../graphics/ndeGL.hpp"
#include "../../physics/PhysicLayer.hpp"

/** an object representing a colored cube (not exactly a cube, but a parallepipedal object.)
*
*
*/
class CubicObject : public nde::GameObject 
{
public:

  CubicObject(std::string n, 
      nde::length_t width, nde::length_t height, nde::length_t depth,
      glm::vec3 color, 
      glm::vec3 initialPos,
      std::shared_ptr<nde::Shader> shader,
      std::shared_ptr<nde::PhysicLayer> layer,
      nde::mass_t mass = 1.0f,
      bool isStatic = false,
      bool isVisible = true);

  virtual ~CubicObject();
  
  virtual void onFrameBegin(nde::timeInterval_t elapsed);
  

  virtual void onFrameEnd(nde::timeInterval_t elapsed);

};

#endif //COLORHOPPER_CUBIC_OBJECT_HPP