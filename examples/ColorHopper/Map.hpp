/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORHOPPER_MAP_HPP
#define COLORHOPPER_MAP_HPP

#include "CubicObject.hpp"
#include "../../physics/PhysicLayer.hpp"

void initMap();

class MapElement : public CubicObject
{
public:
    static std::shared_ptr<nde::PhysicLayer> mapPhysicLayer;

public:
  MapElement(std::string name, 
            nde::length_t width, nde::length_t height, nde::length_t depth,
            glm::vec3 initialPos, 
            glm::vec3 color,
            std::shared_ptr<nde::Shader> shader);

  virtual ~MapElement();

};

#endif //COLORHOPPER_MAP_HPP