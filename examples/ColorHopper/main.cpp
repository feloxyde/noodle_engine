/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/
//#include <GL/gl3.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../../graphics/View.hpp"
#include "../../interface/Screen.hpp"
#include "../../core/Game.hpp"
#include "../../core/CoreEventListener.hpp"
#include "../../core/CoreEventQueue.hpp"

#include "Map.hpp"
#include "Player.hpp"

using namespace nde;
using namespace std;
using namespace glm;




int main()
{

  Game::init(GLFW_KEY_ESCAPE, GLFW_KEY_Q);

  Game::getGameEventQueue()->addEventListener(std::make_shared<TargetedCoreEventBouncer>());

  Game::getGameScreen()->getView()->camera().replace(vec3(-10.0, 2.0, -5.0));
  Game::getGameScreen()->getView()->camera().setTarget(vec3(0.0, 0.0, 0.0));

  initPlayer();
  initMap();


  Game::runMainLoop();

  return EXIT_SUCCESS;
}
