/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef COLORHOPPER_PLAYER_HPP
#define COLORHOPPER_PLAYER_HPP

#include "../../interface/Controller.hpp"
#include <glm/glm.hpp>
#include "../../graphics/ndeGL.hpp"
#include <memory>
#include "../../physics/PhysicLayer.hpp"
#include "CubicObject.hpp"

#include "../../physics/CollisionEvent.hpp"
#include "../../core/CoreEventListener.hpp"



void initPlayer();


class Player;



class GroundListener : public nde::CoreEventListener<nde::CollisionEvent>
{
public:
    GroundListener(Player* player);

    virtual ~GroundListener();

    virtual void reaction(std::shared_ptr<nde::CollisionEvent>);

private: 
    Player* m_player;
};




class Player : public CubicObject
{
public:
    static constexpr float mouseSensitivity = 0.002;
    static std::shared_ptr<nde::PhysicLayer> playerPhysicLayer;

    static constexpr float jumpImpulse = 6.0f;
    static constexpr float slideImpulse = 6.0f;
    static constexpr float slideJumpImpulse = 10.0f;
    static constexpr float defaultFriction = 1.0f;
    static constexpr float slideFriction = 1.0f; 
    static constexpr float groundSpeed = 3.0f;
    static constexpr float airSpeed = 4.0f;
    static constexpr float groundControl = 20.0f;
    static constexpr float airControl = 6.0f;
public:

  Player(glm::vec3 initialPos);

  virtual ~Player();

    
  virtual void onFrameBegin(nde::timeInterval_t elapsed);
  

  virtual void onFrameEnd(nde::timeInterval_t elapsed);

  void setGrounded();


private:
    void slide();
    void jump();
    void move();

private:
    nde::inputIdentifier_t m_iMoveAxisForward;
    nde::inputIdentifier_t m_iMoveAxisStrafe;
    nde::inputIdentifier_t m_iJump;
    nde::inputIdentifier_t m_iSlide;

    glm::vec3 m_moveDirection;
    glm::vec3 m_lookDirection;

    bool m_isSliding;
    bool m_isGrounded;
    bool m_slideJumped;
};




#endif //COLORHOPPER_PLAYER_HPP