/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "Player.hpp"
#include "../../utils/geometry.hpp"
#include "../../core/Game.hpp"
#include "../../physics/PhysicBody.hpp"
#include "../../physics/Force.hpp"
#include "../../interface/Screen.hpp"
#include "../../graphics/ndeGL.hpp"

void initPlayer()
{
 
  auto player = std::make_shared<Player>(glm::vec3(3.0, 6.0, -6.0));
  player->registerToGame();

}


GroundListener::GroundListener(Player* player):
CoreEventListener::CoreEventListener(),
m_player(player)
{

}

GroundListener::~GroundListener()
{
  //nothing to do
}

void GroundListener::reaction(std::shared_ptr<nde::CollisionEvent>)
{
  m_player->setGrounded();
}



constexpr float Player::mouseSensitivity;
constexpr float Player::jumpImpulse;
constexpr float Player::slideImpulse;
constexpr float Player::slideJumpImpulse;
constexpr float Player::defaultFriction;
constexpr float Player::slideFriction; 
constexpr float Player::groundSpeed;
constexpr float Player::airSpeed;
constexpr float Player::groundControl;
constexpr float Player::airControl;


std::shared_ptr<nde::PhysicLayer> Player::playerPhysicLayer = std::make_shared<nde::PhysicLayer>(false, true);


Player::Player(glm::vec3 initialPos):
CubicObject::CubicObject(
      "player",
      0.5f, 1.0f, 0.5f,
      glm::vec3(0.0f),
      initialPos,
      std::shared_ptr<nde::Shader>(),
      playerPhysicLayer,
      1.0f,
      false,
      false),
      m_moveDirection(glm::vec3(0.0f)),
      m_lookDirection(glm::vec3(1.0f)),
      m_isSliding(false),
      m_isGrounded(false), 
      m_slideJumped(false)
{
    m_iMoveAxisForward = nde::Game::getGameController()->addAxis(GLFW_KEY_S, GLFW_KEY_W);
    m_iMoveAxisStrafe = nde::Game::getGameController()->addAxis(GLFW_KEY_A, GLFW_KEY_D);
    //inputIdentifier_t iToggleSimulation = controller->addKey(GLFW_KEY_Q);
    m_iJump = nde::Game::getGameController()->addKey(GLFW_KEY_SPACE);
    m_iSlide = nde::Game::getGameController()->addKey(GLFW_KEY_LEFT_SHIFT);

    this->addEventListener(std::make_shared<GroundListener>(this));
}

Player::~Player(){}

    
void  Player::onFrameBegin(nde::timeInterval_t elapsed)
  {
    glm::vec2 mouseMove = nde::Game::getGameController()->getMouseMove(); 
    
    nde::Game::getGameScreen()->getView()->camera().rotateHorizontally(-(((float)mouseMove.x)*Player::mouseSensitivity));
    nde::Game::getGameScreen()->getView()->camera().rotateVertically((((float)mouseMove.y)*Player::mouseSensitivity));
  
    glm::vec3 moveAxis = glm::vec3(
      -nde::Game::getGameController()->getAxisValue(m_iMoveAxisStrafe), 
      0.0f, 
      nde::Game::getGameController()->getAxisValue(m_iMoveAxisForward));
    
    if(moveAxis.x != 0.0 || moveAxis.z != 0.0)
    { 
    glm::vec3 move = glm::vec3(moveAxis.x, 0.0f,moveAxis.z);
    glm::vec3 moveDirection = nde::Geometry::forwardAlong(
          move, 
          nde::Game::getGameScreen()->getView()->camera().getLookDirection(), 
          nde::Game::getGameScreen()->getView()->camera().getUp());
    m_moveDirection = normalize(moveDirection);
    } else 
    {
      m_moveDirection = glm::vec3(0.0f, 0.0f, 0.0f);
    }

    m_lookDirection = normalize(nde::Game::getGameScreen()->getView()->camera().getLookDirection());


    this->slide();
    this->jump();
    this->move();

    m_isGrounded = false;
 
 
  }
  

void Player::onFrameEnd(nde::timeInterval_t elapsed)
{
    nde::Game::getGameScreen()->getView()->camera().replace(this->m_body->getPosition());
}

void Player::setGrounded()
{
  this->m_isGrounded = true;
}


void Player::slide() 
{
  if(nde::Game::getGameController()->hasKeyBeenPressed(m_iSlide)){
    m_isSliding = true;
    this->getPhysicBody()->setFrictionForce(Player::slideFriction);
    this->getPhysicBody()->addForce(std::make_shared<nde::Impulse>(m_moveDirection*Player::slideImpulse*10.0f, 0.1f));
  } 
  if(nde::Game::getGameController()->hasKeyBeenReleased(m_iSlide)) 
  {
    m_isSliding = false;
    m_slideJumped = false;
    this->getPhysicBody()->setFrictionForce(Player::defaultFriction);
  }
}

void Player::jump()
{
    if(nde::Game::getGameController()->hasKeyBeenPressed(m_iJump))
    {
      if(m_isSliding){
        if(!m_slideJumped){
         this->m_body->addForce(std::make_shared<nde::Impulse>
                (m_lookDirection*Player::slideJumpImpulse*10.0f, 0.1f));
                m_slideJumped = true;
        }
      } else if(m_isGrounded){
        this->m_body->addForce(std::make_shared<nde::Impulse>(glm::vec3(0.0f, Player::jumpImpulse*10.0, 0.0f), 0.1f));
      } 
    }
}

void Player::move()
{
    if(!m_isSliding){
        float objectForce = 0.0f;
        float objectSpeed = 0.0f;
        
  
        if(m_isGrounded){
          objectForce = Player::groundControl;
          objectSpeed = Player::groundSpeed;
        } else {
          if(m_moveDirection.x != 0.0f || m_moveDirection.z != 0.0f){
            objectForce = Player::airControl;
          }
          objectSpeed = Player::airSpeed;
        }
      
        this->m_body->addForce(std::make_shared<nde::PlaneSpeedForce>(m_moveDirection*objectSpeed, glm::vec3(0.0f, 1.0f, 0.0f), objectForce));
    }
  
  
}