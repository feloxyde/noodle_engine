/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

/* this example expects a .frag and .vert shader in order to run */
/*
HOW to use : 
the qwsd and shit/space to move camera around
*/


#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../devtools.hpp"
#include <iostream>
#include "../graphics/ndeGL.hpp"
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "../graphics/Model.hpp"
#include <vector>
#include <glm/gtc/quaternion.hpp>
#include "../utils/geometry.hpp"
#define _USE_MATH_DEFINES
#include <cmath>
#include "../graphics/View.hpp"
#include "../graphics/GraphicObject.hpp"
#include "../interface/Screen.hpp"
#include "../graphics/Renderer.cpp"
#include <glm/gtx/rotate_vector.hpp>
#include <memory>

class NormalizedCubeModel  
{
public:
  static constexpr GLfloat CUBE_VERTICES[] = {
    // ABCD face
    -0.5f, -0.5f, -0.5f, //A
    0.5f, -0.5f, -0.5f, //B
    0.5f, -0.5f, 0.5f, //C
    -0.5f, -0.5f, 0.5f, //D

    //EFGH face (+4)
    -0.5f, 0.5f, 0.5f, //E
    0.5f, 0.5f, 0.5f, //F
    0.5f, 0.5f, -0.5f, //G
    -0.5f, 0.5f, -0.5f,  //H

    //ABGH (+8)
    -0.5f, -0.5f, -0.5f, //A
    0.5f, -0.5f, -0.5f, //B
    0.5f, 0.5f, -0.5f, //G
    -0.5f, 0.5f, -0.5f,  //H
    
    //CDEF (+12)
     0.5f, -0.5f, 0.5f, //C
    -0.5f, -0.5f, 0.5f, //D
    -0.5f, 0.5f, 0.5f, //E
    0.5f, 0.5f, 0.5f, //F
    
    //BCFG (+16)
    0.5f, -0.5f, -0.5f, //B
    0.5f, -0.5f, 0.5f, //C
    0.5f, 0.5f, 0.5f, //F
    0.5f, 0.5f, -0.5f, //G

    //ADHE (+20)
    -0.5f, -0.5f, -0.5f, //A
    -0.5f, -0.5f, 0.5f, //D
    -0.5f, 0.5f, -0.5f,  //H
    -0.5f, 0.5f, 0.5f //E
  };

  static constexpr GLuint CUBE_INDEXES[] = {
    0, 1, 2, 0, 2, 3, //ABC, ACD
    3+4, 0+4, 1+4, 3+4, 2+4, 1+4,  //HEF, HGF (+4)
    0+8, 3+8, 1+8, 2+8, 3+8, 1+8, //AHB, GHB (+8)
    2+12, 1+12, 0+12, 2+12, 3+12, 0+12, //EDC, EFC //(+12)
    3+16, 0+16, 1+16, 3+16, 2+16, 1+16, //GBC, GFC (+16)
    0+20, 1+20, 3+20, 0+20, 2+20, 3+20 //ADE, AHE (+20)
  };

  static constexpr GLfloat CUBE_TEXTURE_COORDINATES[] = {
    // ABCD face
    0.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 0.0f,
    
    //EFGH face (+4)
    1.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 1.0f,
    1.0f, 1.0f,
    
    //ABGH (+8)
    2.0f, 2.0f,
    2.0f, 2.0f,
    2.0f, 2.0f,
    2.0f, 2.0f,

    //CDEF (+12)
    3.0f, 3.0f,
    3.0f, 3.0f,
    3.0f, 3.0f,
    3.0f, 3.0f,

    //BCFG (+16)
    4.0f, 4.0f,
    4.0f, 4.0f,
    4.0f, 4.0f,
    4.0f, 4.0f,

    //ADHE (+20)
    5.0f, 5.0f,
    5.0f, 5.0f,
    5.0f, 5.0f,
    5.0f, 5.0f

  };

  static constexpr GLfloat CUBE_NORMALS[] = {
      // ABCD face
    0.0f, -1.0f, 0.0f, //A
    0.0f, -1.0f, 0.0f, //B
    0.0f, -1.0f, 0.0f, //C
    0.0f, -1.0f, 0.0f, //D

    //EFGH face (+4)
    0.0f, 1.0f, 0.0f, //E
    0.0f, 1.0f, 0.0f, //F
    0.0f, 1.0f, 0.0f, //G
    0.0f, 1.0f, 0.0f,  //H

    //ABGH (+8)
    0.0f, 0.0f, -1.0f, //A
    0.0f, 0.0f, -1.0f, //B
    0.0f, 0.0f, -1.0f, //G
    0.0f, 0.0f, -1.0f,  //H
    
    //CDEF (+12)
    0.0f, 0.0f, 1.0f, //C
    0.0f, 0.0f, 1.0f, //D
    0.0f, 0.0f, 1.0f, //E
    0.0f, 0.0f, 1.0f, //F
    
    //BCFG (+16)
    1.0f, 0.0f, 0.0f, //B
    1.0f, 0.0f, 0.0f, //C
    1.0f, 0.0f, 0.0f, //F
    1.0f, 0.0f, 0.0f, //G

    //ADHE (+20)
    -1.0f, 0.0f, 0.0f, //A
    -1.0f, 0.0f, 0.0f, //D
    -1.0f, 0.0f, 0.0f, //H
    -1.0f, 0.0f, 0.0f  //E
  };

  constexpr static unsigned verticesByteSize = sizeof(CUBE_VERTICES);
  constexpr static unsigned indexesByteSize = sizeof(CUBE_INDEXES);
  constexpr static unsigned texCoordByteSize = sizeof(CUBE_TEXTURE_COORDINATES);
  constexpr static unsigned normalsByteSize = sizeof(CUBE_NORMALS);
};

constexpr GLfloat NormalizedCubeModel::CUBE_VERTICES[];
constexpr GLuint NormalizedCubeModel::CUBE_INDEXES[];
constexpr GLfloat NormalizedCubeModel::CUBE_TEXTURE_COORDINATES[];
constexpr GLfloat NormalizedCubeModel::CUBE_NORMALS[];


static double xpos_old(0.0), ypos_old(0.0);
static double xmov(0.0), ymov(0.0);
static bool mousemoved = false;

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
  xmov = xpos - xpos_old;
  ymov = ypos - ypos_old;
  xpos_old = xpos;
  ypos_old = ypos;

  if(xmov > 50.0){
    xmov = 0.0;
  }
  if(xmov < -50.0){
    xmov = -0.0;
  }
  if(ymov > 50.0){
    ymov = 0.0;
  }
  if(ymov < -50.0){
    ymov = 0.0;
  }

  mousemoved = true;
}

bool mleft(false), mright(false);

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

  switch (button) {
    case GLFW_MOUSE_BUTTON_2 :
      if(action == GLFW_PRESS) mright = true;
      if(action == GLFW_RELEASE) mright = false;
  }

}

static double xAxis(0), yAxis(0), zAxis(0);
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (key)
    {
          case GLFW_KEY_ESCAPE : if( action == GLFW_PRESS)
              glfwSetWindowShouldClose(window, GLFW_TRUE);
              break;

          case GLFW_KEY_W : if(action == GLFW_PRESS) zAxis += 1.0;
                            else if (action == GLFW_RELEASE) zAxis -= 1.0;
                            break;

          case GLFW_KEY_S : if(action == GLFW_PRESS) zAxis -= 1.0;
                            else if (action == GLFW_RELEASE) zAxis += 1.0;
                            break;

          case GLFW_KEY_D : if(action == GLFW_PRESS) xAxis -= 1.0;
                            else if (action == GLFW_RELEASE) xAxis += 1.0;
                            break;

          case GLFW_KEY_A : if(action == GLFW_PRESS) xAxis += 1.0;
                            else if (action == GLFW_RELEASE) xAxis -= 1.0;
                            break;

          case GLFW_KEY_SPACE : if(action == GLFW_PRESS) yAxis += 1.0;
                            else if (action == GLFW_RELEASE) yAxis -= 1.0;
                            break;

          case GLFW_KEY_LEFT_SHIFT : if(action == GLFW_PRESS) yAxis -= 1.0;
                            else if (action == GLFW_RELEASE) yAxis += 1.0;
                            break;
       
    }

}




int main()
{

  nde::SimpleScreen screen = nde::SimpleScreen(0);

  glfwSetCursorPosCallback(screen.getWindow(), cursor_position_callback);
  glfwSetMouseButtonCallback(screen.getWindow(), mouse_button_callback);
  glfwSetKeyCallback(screen.getWindow(), key_callback);

  

  glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
  glEnable(GL_DEPTH_TEST);

  //GLuint programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
//  glEnable(GL_TEXTURE_2D);
//  unsigned i = 0;
/*
  glm::mat4 model = glm::mat4(1.0f);
  nde::Camera cam(glm::vec3(5,0,5),glm::vec3(0,0,0),glm::vec3(0,1,0));
  glm::mat4 view = cam.lookAt();
  nde::Projection proj(70.0f, (float)fulls.width()/(float)fulls.height(), 0.1f, 100.0f );
  glm::mat4 projection = proj.perspective();
  glm::mat4 MVP = projection*view*model;
*/  std::string sname = "normals3D";
  auto shader_texture = nde::Shader::loadShader(sname);


  auto normalCube = nde::Model::createTexturedNormalizedIndexedModel(
    NormalizedCubeModel::CUBE_VERTICES, 
    NormalizedCubeModel::verticesByteSize,
    NormalizedCubeModel::CUBE_TEXTURE_COORDINATES, 
    NormalizedCubeModel::texCoordByteSize,
    NormalizedCubeModel::CUBE_NORMALS,
    NormalizedCubeModel::normalsByteSize,
    NormalizedCubeModel::CUBE_INDEXES, 
    NormalizedCubeModel::indexesByteSize); 
    
  auto OBB1 = std::make_shared<nde::OrientedBoundingBox>(0.5,0.5,0.5);

  auto g_body1 = std::make_shared<nde::AttachedGraphicObject>(shader_texture, normalCube, nullptr, OBB1);

  OBB1->translate(glm::vec3(0.0, 0.0, 0.0));

  nde::View& view = *(screen.getView());

  nde::GlobalRenderer globalRenderer;

  view.watch(&globalRenderer);


  globalRenderer.referGraphicObject(g_body1);

  
float gap = 10.0f;
  unsigned fcount = 0;
  double seconds = 0.0f;
  double oldseconds = 0.0f;
  bool reset = false;
while(screen.shouldNotQuit()){

    screen.update(0.0f); //for now we dont care about

    glfwPollEvents();

    if(mousemoved && fcount > 3){
      view.camera().rotateHorizontally(-(xmov/550.0f));
      view.camera().rotateVertically((ymov/550.0f));
      mousemoved = false;
    }



    view.camera().moveForward(glm::vec3(xAxis/8, yAxis/8 , zAxis/8 ));

    seconds = glfwGetTime();
    

    oldseconds = seconds;


  fcount++;
  }


return EXIT_SUCCESS;
}
