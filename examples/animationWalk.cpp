/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

//#include <GL/gl3.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../devtools.hpp"
#include <iostream>
#include "../graphics/ndeGL.hpp"
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "../graphics/Model.hpp"
#include <vector>
#include <glm/gtc/quaternion.hpp>
#include "../utils/geometry.hpp"
#include <cmath>
#include "../physics/Body.hpp"
#include "../graphics/View.hpp"
#include "../graphics/GraphicObject.hpp"
#include "../interface/Screen.hpp"
#include "../graphics/Renderer.cpp"
#include "../physics/Animation.hpp"
#include <set>
#include "../utils/Human.hpp"

static double xpos_old(0.0), ypos_old(0.0);
static double xmov(0.0), ymov(0.0);
static bool mousemoved = false;

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
  xmov = xpos - xpos_old;
  ymov = ypos - ypos_old;
  xpos_old = xpos;
  ypos_old = ypos;

  if(xmov > 50.0){
    xmov = 0.0;
  }
  if(xmov < -50.0){
    xmov = -0.0;
  }
  if(ymov > 50.0){
    ymov = 0.0;
  }
  if(ymov < -50.0){
    ymov = 0.0;
  }

  mousemoved = true;
}

bool mleft(false), mright(false);

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

  switch (button) {
    case GLFW_MOUSE_BUTTON_2 :
      if(action == GLFW_PRESS) mright = true;
      if(action == GLFW_RELEASE) mright = false;
  }

}

static double xAxis(0), yAxis(0), zAxis(0);

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (key)
    {
          case GLFW_KEY_ESCAPE : if( action == GLFW_PRESS)
              glfwSetWindowShouldClose(window, GLFW_TRUE);
              break;

          case GLFW_KEY_W : if(action == GLFW_PRESS) zAxis -= 1.0;
                            else if (action == GLFW_RELEASE) zAxis += 1.0;
                            break;

          case GLFW_KEY_S : if(action == GLFW_PRESS) zAxis += 1.0;
                            else if (action == GLFW_RELEASE) zAxis -= 1.0;
                            break;

          case GLFW_KEY_D : if(action == GLFW_PRESS) xAxis += 1.0;
                            else if (action == GLFW_RELEASE) xAxis -= 1.0;
                            break;

          case GLFW_KEY_A : if(action == GLFW_PRESS) xAxis -= 1.0;
                            else if (action == GLFW_RELEASE) xAxis += 1.0;
                            break;

          case GLFW_KEY_SPACE : if(action == GLFW_PRESS) yAxis += 1.0;
                            else if (action == GLFW_RELEASE) yAxis -= 1.0;
                            break;

          case GLFW_KEY_LEFT_SHIFT : if(action == GLFW_PRESS) yAxis -= 1.0;
                            else if (action == GLFW_RELEASE) yAxis += 1.0;
                            break;
    }

}


int main()
{

  nde::SimpleScreen screen = nde::SimpleScreen(0);

  glfwSetCursorPosCallback(screen.getWindow(), cursor_position_callback);
  glfwSetMouseButtonCallback(screen.getWindow(), mouse_button_callback);
  glfwSetKeyCallback(screen.getWindow(), key_callback);

  std::string version((char*)glGetString(GL_VERSION));
  std::cout << version << std::endl;
  //creation of a VBO

  glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
  glEnable(GL_DEPTH_TEST);

  std::string sname = "monochrome3D";
  auto chrome = nde::Shader::loadShader(sname);
  //GLuint programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
//  glEnable(GL_TEXTURE_2D);
//  unsigned i = 0;
/*
  glm::mat4 model = glm::mat4(1.0f);
  nde::Camera cam(glm::vec3(5,0,5),glm::vec3(0,0,0),glm::vec3(0,1,0));
  glm::mat4 view = cam.lookAt();
  nde::Projection proj(70.0f, (float)fulls.width()/(float)fulls.height(), 0.1f, 100.0f );
  glm::mat4 projection = proj.perspective();
  glm::mat4 MVP = projection*view*model;
*/
  nde::View& view = *(screen.getView());

  nde::GlobalRenderer globalRenderer;
  view.watch(&globalRenderer);

  //lets create an human body

  glm::vec3 yellow(1.0, 1.0, 0.0);
  glm::vec3 blue(0.0, 0.0, 1.0);
  glm::vec3 green(0.0, 1.0, 0.0);
  glm::vec3 red(1.0, 0.0, 0.0);
  glm::vec3 pink(1.0, 0.0, 1.0);

  nde::Body* body = nde::Human::body();


  //thorax
  auto thorax = body->getPart(nde::Human::thoraxID);
  std::shared_ptr<nde::Model> thoraxModel = generateBoxModel(thorax->getBox(), yellow);
  auto g_thorax = std::make_shared<nde::AttachedGraphicObject>(chrome, thoraxModel, nullptr, thorax);
  globalRenderer.referGraphicObject(g_thorax);
 
  //head
  auto head = body->getPart(nde::Human::headID);
  std::shared_ptr<nde::Model> headModel = generateBoxModel(head->getBox(), green);
  auto g_head = std::make_shared<nde::AttachedGraphicObject>(chrome, headModel, nullptr, head);
  globalRenderer.referGraphicObject(g_head);

  //leftArm
  auto leftArm = body->getPart(nde::Human::leftArmID);
  std::shared_ptr<nde::Model> leftArmModel = generateBoxModel(leftArm->getBox(), green);
  auto g_leftArm = std::make_shared<nde::AttachedGraphicObject>(chrome, leftArmModel, nullptr, leftArm);
  globalRenderer.referGraphicObject(g_leftArm);

  //left forearm
  auto leftForearm = body->getPart(nde::Human::leftForearmID);
  std::shared_ptr<nde::Model> leftForearmModel = generateBoxModel(leftForearm->getBox(), green);
  auto g_leftForearm = std::make_shared<nde::AttachedGraphicObject>(chrome, leftForearmModel, nullptr, leftForearm);
  globalRenderer.referGraphicObject(g_leftForearm);

  //left hand
  auto leftHand = body->getPart(nde::Human::leftHandID);
  std::shared_ptr<nde::Model> leftHandModel = generateBoxModel(leftHand->getBox(), red);
  auto g_leftHand = std::make_shared<nde::AttachedGraphicObject>(chrome, leftHandModel, nullptr, leftHand);
  globalRenderer.referGraphicObject(g_leftHand);


  //rightArm
  auto rightArm = body->getPart(nde::Human::rightArmID);
  std::shared_ptr<nde::Model> rightArmModel = generateBoxModel(rightArm->getBox(), green);
  auto g_rightArm = std::make_shared<nde::AttachedGraphicObject>(chrome, rightArmModel, nullptr, rightArm);
  globalRenderer.referGraphicObject(g_rightArm);

  //right forearm
  auto rightForearm = body->getPart(nde::Human::rightForearmID);
  std::shared_ptr<nde::Model> rightForearmModel = generateBoxModel(rightForearm->getBox(), green);
  auto g_rightForearm = std::make_shared<nde::AttachedGraphicObject>(chrome, rightForearmModel, nullptr, rightForearm);
  globalRenderer.referGraphicObject(g_rightForearm);

  //right hand
  auto rightHand = body->getPart(nde::Human::rightHandID);
  std::shared_ptr<nde::Model> rightHandModel = generateBoxModel(rightHand->getBox(), red);
  auto g_rightHand = std::make_shared<nde::AttachedGraphicObject>(chrome, rightHandModel, nullptr, rightHand);
  globalRenderer.referGraphicObject(g_rightHand);

  //right leg
  auto rightLeg = body->getPart(nde::Human::rightLegID);
  std::shared_ptr<nde::Model> rightLegModel = generateBoxModel(rightLeg->getBox(), pink);
  auto g_rightLeg = std::make_shared<nde::AttachedGraphicObject>(chrome, rightLegModel, nullptr, rightLeg);
  globalRenderer.referGraphicObject(g_rightLeg);

  //right shin
  auto rightShin = body->getPart(nde::Human::rightShinID);
  std::shared_ptr<nde::Model> rightShinModel = generateBoxModel(rightShin->getBox(), pink);
  auto g_rightShin = std::make_shared<nde::AttachedGraphicObject>(chrome, rightShinModel, nullptr, rightShin);
  globalRenderer.referGraphicObject(g_rightShin);

  //right foot
  auto rightFoot = body->getPart(nde::Human::rightFootID);
  std::shared_ptr<nde::Model> rightFootModel = generateBoxModel(rightFoot->getBox(), red);
  auto g_rightFoot = std::make_shared<nde::AttachedGraphicObject>(chrome, rightFootModel, nullptr, rightFoot);
  globalRenderer.referGraphicObject(g_rightFoot);

  //left leg
  auto leftLeg = body->getPart(nde::Human::leftLegID);
  std::shared_ptr<nde::Model> leftLegModel = generateBoxModel(leftLeg->getBox(), pink);
  auto g_leftLeg = std::make_shared<nde::AttachedGraphicObject>(chrome, leftLegModel, nullptr, leftLeg);
  globalRenderer.referGraphicObject(g_leftLeg);

  //left shin
  auto leftShin = body->getPart(nde::Human::leftShinID);
  std::shared_ptr<nde::Model> leftShinModel = generateBoxModel(leftShin->getBox(), pink);
  auto g_leftShin  = std::make_shared<nde::AttachedGraphicObject>(chrome, leftShinModel, nullptr, leftShin);
  globalRenderer.referGraphicObject(g_leftShin);

  //left foot
  auto leftFoot = body->getPart(nde::Human::leftFootID);
  std::shared_ptr<nde::Model> leftFootModel = generateBoxModel(leftFoot->getBox(), red);
  auto g_leftFoot = std::make_shared<nde::AttachedGraphicObject>(chrome, leftFootModel, nullptr, leftFoot);
  globalRenderer.referGraphicObject(g_leftFoot);

  nde::Animation* anim = nde::Human::walk();

  std::set<nde::bodyPartID_t> ignored;

  //body->translate(glm::vec3(0.0 , 0.0, 3.0));
  view.camera().moveForward(glm::vec3(0, 0, -7));

float gap = 10.0;
  unsigned fcount = 0;
  double seconds = 0.0;
  bool reset = false;
/*
double time1 = 2.0;
double time2 = 2.0;
double elapsed = 0.0;
bool f1 = false;
*/

double elapsed = 0.0;
unsigned currentRank = 0;
double timeLeft = 0.0;
double timeToReach = 0.0;

while(screen.shouldNotQuit()){




    timeLeft = seconds;
    timeToReach = anim->getReachTime(currentRank) - elapsed;
    while (timeLeft >= timeToReach && currentRank <= anim->maxRank())
    {
      //animate fully the frame
      anim->getFrame(currentRank)->applyToBody(*body, 1.0, ignored);
      currentRank = anim->getNext(currentRank);
      timeLeft -= timeToReach;
      elapsed = 0.0;
      timeToReach = anim->getReachTime(currentRank); //the new current time to reach
      std::cout << "SWITCHING TO KEYFRAME " << currentRank << std::endl;
    }
    if (currentRank <= anim->maxRank())
    anim->getFrame(currentRank)->applyToBody(*body, timeLeft/timeToReach, ignored);
    elapsed += timeLeft;


    screen.update(0.0); //for now we dont care about time
    glfwPollEvents();

    if(mousemoved && fcount > 3){
      view.camera().rotateHorizontally(-(xmov/550.0f));
      view.camera().rotateVertically((ymov/550.0f));
      mousemoved = false;
    }


    view.camera().moveForward(glm::vec3(-xAxis/10, yAxis/10 , -zAxis/10 ));

    seconds = glfwGetTime(); //getting time
    glfwSetTime(0.0); //reset



  fcount++;
  }


return EXIT_SUCCESS;
}
