/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

/* this example expects a .frag and .vert shader in order to run */

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include "../devtools.hpp"
#include "../graphics/ndeGL.hpp"
#include <string>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include "../graphics/Model.hpp"
#include <vector>
#include <glm/gtc/quaternion.hpp>
#include "../utils/geometry.hpp"
#include <cmath>
#include "../graphics/View.hpp"
#include "../graphics/GraphicObject.hpp"
#include "../interface/Screen.hpp"
#include "../graphics/Renderer.cpp"
#include <memory>


static double xpos_old(0.0), ypos_old(0.0);
static double xmov(0.0), ymov(0.0);
static bool mousemoved = false;

static void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
  xmov = xpos - xpos_old;
  ymov = ypos - ypos_old;
  xpos_old = xpos;
  ypos_old = ypos;

  if(xmov > 50.0){
    xmov = 0.0;
  }
  if(xmov < -50.0){
    xmov = -0.0;
  }
  if(ymov > 50.0){
    ymov = 0.0;
  }
  if(ymov < -50.0){
    ymov = 0.0;
  }

  mousemoved = true;
}

bool mleft(false), mright(false);

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{

  switch (button) {
    case GLFW_MOUSE_BUTTON_2 :
      if(action == GLFW_PRESS) mright = true;
      if(action == GLFW_RELEASE) mright = false;
  }

}

static double xAxis(0), yAxis(0), zAxis(0);

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (key)
    {
          case GLFW_KEY_ESCAPE : if( action == GLFW_PRESS)
              glfwSetWindowShouldClose(window, GLFW_TRUE);
              break;

          case GLFW_KEY_W : if(action == GLFW_PRESS) zAxis += 1.0;
                            else if (action == GLFW_RELEASE) zAxis -= 1.0;
                            break;

          case GLFW_KEY_S : if(action == GLFW_PRESS) zAxis -= 1.0;
                            else if (action == GLFW_RELEASE) zAxis += 1.0;
                            break;

          case GLFW_KEY_D : if(action == GLFW_PRESS) xAxis -= 1.0;
                            else if (action == GLFW_RELEASE) xAxis += 1.0;
                            break;

          case GLFW_KEY_A : if(action == GLFW_PRESS) xAxis += 1.0;
                            else if (action == GLFW_RELEASE) xAxis -= 1.0;
                            break;

          case GLFW_KEY_SPACE : if(action == GLFW_PRESS) yAxis += 1.0;
                            else if (action == GLFW_RELEASE) yAxis -= 1.0;
                            break;

          case GLFW_KEY_LEFT_SHIFT : if(action == GLFW_PRESS) yAxis -= 1.0;
                            else if (action == GLFW_RELEASE) yAxis += 1.0;
                            break;
    }

}


int main()
{

  nde::SimpleScreen screen = nde::SimpleScreen(0);

  glfwSetCursorPosCallback(screen.getWindow(), cursor_position_callback);
  glfwSetMouseButtonCallback(screen.getWindow(), mouse_button_callback);
  glfwSetKeyCallback(screen.getWindow(), key_callback);


  glClearColor(0.0f, 0.4f, 0.4f, 1.0f);
  glEnable(GL_DEPTH_TEST);

  std::string sname = "monochrome3D";
  auto chrome = nde::Shader::loadShader(sname);
  //GLuint programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
//  glEnable(GL_TEXTURE_2D);
//  unsigned i = 0;
/*
  glm::mat4 model = glm::mat4(1.0f);
  nde::Camera cam(glm::vec3(5,0,5),glm::vec3(0,0,0),glm::vec3(0,1,0));
  glm::mat4 view = cam.lookAt();
  nde::Projection proj(70.0f, (float)fulls.width()/(float)fulls.height(), 0.1f, 100.0f );
  glm::mat4 projection = proj.perspective();
  glm::mat4 MVP = projection*view*model;
*/
  nde::View& view = *(screen.getView());




  glm::vec3 yellow(1.0, 1.0, 0.0);
  glm::vec3 blue(0.0, 0.0, 1.0);
  glm::vec3 red(1.0, 0.0, 0.0);
  glm::vec3 green(0.0, 1.0, 0.0);

  auto yellowCube = std::make_shared<ColorCubeModel>(yellow);
  auto blueCube = std::make_shared<ColorCubeModel>(blue);
  auto redCube = std::make_shared<ColorCubeModel>(red);
  auto greenCube = std::make_shared<ColorCubeModel>(green);

  auto OBB1 =std::make_shared<nde::OrientedBoundingBox>(0.5,0.5,0.5);
  auto OBB4 =std::make_shared<nde::OrientedBoundingBox>(0.5,0.5,0.5);
  auto OBB5 =std::make_shared<nde::OrientedBoundingBox>(0.5,0.5,0.5);
  auto OBB6 =std::make_shared<nde::OrientedBoundingBox>(0.5,0.5,0.5);


  auto g_body1 = std::make_shared<nde::AttachedGraphicObject>(chrome, yellowCube, nullptr, OBB1);
  auto g_body4 = std::make_shared<nde::AttachedGraphicObject>(chrome, greenCube, nullptr, OBB4);
  auto g_body5 = std::make_shared<nde::AttachedGraphicObject>(chrome, blueCube, nullptr, OBB5);
  auto g_body6 = std::make_shared<nde::AttachedGraphicObject>(chrome, redCube, nullptr, OBB6);


  OBB1->translate(glm::vec3(0.0, 0.0, 0.0));
  OBB4->translate(glm::vec3(0.0, 1.0, 0.0));
  OBB5->translate(glm::vec3(1.0, 0.0, 0.0));
  OBB6->translate(glm::vec3(0.0, 0.0, 1.0));

  nde::GlobalRenderer globalRenderer;

  view.watch(&globalRenderer);

  globalRenderer.referGraphicObject(g_body1);
  globalRenderer.referGraphicObject(g_body4);
  globalRenderer.referGraphicObject(g_body5);
  globalRenderer.referGraphicObject(g_body6);

float gap = 10.0;
  unsigned fcount = 0;
  double seconds = 0.0;
  bool reset = false;
while(screen.shouldNotQuit()){

    screen.update(0.0); //for now we dont care about

    glfwPollEvents();

    if(mousemoved && fcount > 3){
      view.camera().rotateHorizontally(-(xmov/550.0f));
      view.camera().rotateVertically((ymov/550.0f));
      mousemoved = false;
    }



    view.camera().moveForward(glm::vec3(xAxis/10, yAxis/10 , zAxis/10 ));

    seconds = glfwGetTime();



  fcount++;
  }


return EXIT_SUCCESS;
}
