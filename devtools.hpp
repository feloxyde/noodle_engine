/* Copyright 2019 Félix Bertoni

This file is part of Noodle Engine.

Noodle Engine is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Noodle Engine is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Noodle Engine.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef NDE_DEVTOOLS_HPP
#define NDE_DEVTOOLS_HPP
/* this file provides a simple and (extremely) fast tool to create a fullscreen window using
glfw */
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "graphics/Model.hpp"
#include "utils/geometry.hpp"
#include <glm/glm.hpp>
#include <memory>
#include <string>


static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}
/*
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}
*/
class FSWindow {

public:
  FSWindow();
  ~FSWindow();
  bool shouldNotQuit();
  void pollEvents(){glfwPollEvents();}
  void swapBuffers(){glfwSwapBuffers(m_window);}
  unsigned width() {return m_width;}
  unsigned height() {return m_height;}
  GLFWwindow* getWindow(){return m_window;}

private:
  GLFWwindow* m_window;
  unsigned m_width;
  unsigned m_height;
};

FSWindow::FSWindow():
m_window(nullptr), m_width(0), m_height(0)
{
  glfwSetErrorCallback(error_callback);

  if (!glfwInit()){
      std::cout << "failed to initialize glfw" << std::endl;
      exit(EXIT_FAILURE);
 }

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  //getting primary monitor
  //handling monitors to select the right one
  int mcount;
  GLFWmonitor** monitors = glfwGetMonitors(&mcount);

  if (monitors == nullptr){
    std::cout << "error while looking for monitors" << std::endl;
    exit(EXIT_FAILURE);
  }

  GLFWmonitor* prim = monitors[0];

  if (mcount > 1){
    //selecting monitors
    std::cout <<  "multiple monitors are available (ID) : " ;
    for (int i = 0; i < mcount; ++i){
      std::cout << glfwGetMonitorName(monitors[i]) << " (" << i << ")   ";
    }
    std::cout << std::endl;
    std::cout << "Please select Id for displaying : ";
    int selection;
    std::cin >> selection;

    while (selection >= mcount){
      std::cout << "Please select Id for displaying : ";
      std::cin >> selection;
    }

    prim = monitors[selection];
  }

  const GLFWvidmode* mode = glfwGetVideoMode(prim);
  glfwWindowHint(GLFW_RED_BITS, mode->redBits);
  glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
  glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
  glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

  std::string mname(glfwGetMonitorName(prim));
  std::cout << "Displaying on monitor " << mname << std::endl;

  m_window = glfwCreateWindow(mode->width, mode->height, "My Title", prim, NULL);

  //m_window = glfwCreateWindow( 1024, 768, "Tutorial 02 - Red triangle", NULL, NULL);
  m_width = mode->width;
  m_height = mode->height;
  if (!m_window)
  {
      glfwTerminate();
      exit(EXIT_FAILURE);
  }

//  glfwSetKeyCallback(m_window, key_callback);
  glfwMakeContextCurrent(m_window);
  glewExperimental=GL_TRUE; // Needed in core profile
  if (glewInit() != GLEW_OK) {
    std::cout << "failed to initialize glew" << std::endl;
    exit(EXIT_FAILURE);
  }
  glfwSwapInterval(1);

  glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

}

bool FSWindow::shouldNotQuit()
{
  return !glfwWindowShouldClose(m_window);
}
FSWindow::~FSWindow()
{
    glfwDestroyWindow(m_window);
    glfwTerminate();
}

std::string vec3ToString(glm::vec3 const& vec)
{
  std::string a;
  a += "(";
  a += std::to_string(vec.x);
  a += ";";
  a += std::to_string(vec.y);
  a += ";";
  a += std::to_string(vec.z);
  a += ")";
  return a;
}


class ColorCubeModel : public nde::MonochromeIndexedModel {

private:
  static constexpr GLfloat CUBE_VERTICES[] = {
    -0.5f, -0.5f, -0.5f, //A
    0.5f, -0.5f, -0.5f, //B
    0.5f, -0.5f, 0.5f, //C
    -0.5f, -0.5f, 0.5f, //D
    -0.5f, 0.5f, 0.5f, //E
    0.5f, 0.5f, 0.5f, //F
    0.5f, 0.5f, -0.5f, //G
    -0.5f, 0.5f, -0.5f  //H
  };

  static constexpr GLuint CUBE_INDEXES[] = {
    0, 1, 2, 1, 2, 3, //ABC, ACD
    0, 3, 4, 0, 7, 4, //ADE, AHE
    0, 7, 1, 6, 7, 1, //AHB, GHB
    6, 1, 2, 6, 5, 2, //GBC, FBC
    3, 2, 4, 4, 5, 2, //EFC, HFC
    7, 4, 5, 7, 6, 5  //HEF, HGF
  };

  constexpr static unsigned verticesByteSize = sizeof(CUBE_VERTICES);
  constexpr static unsigned indexesByteSize = sizeof(CUBE_INDEXES);


public:

  ColorCubeModel(glm::vec3& color):
  nde::MonochromeIndexedModel(CUBE_VERTICES, verticesByteSize, CUBE_INDEXES, indexesByteSize, color)
  {}
};

constexpr GLfloat ColorCubeModel::CUBE_VERTICES[];
constexpr GLuint ColorCubeModel::CUBE_INDEXES[];

/** generates a part and adds his model to the renderer*/

std::shared_ptr<nde::Model> generateRectangularModel(nde::length_t xl, nde::length_t yl, nde::length_t zl, glm::vec3 color)
{
  const GLfloat VERTICES[] = {
    -0.5f*xl, -0.5f*yl, -0.5f*zl, //A
    0.5f*xl, -0.5f*yl, -0.5f*zl, //B
    0.5f*xl, -0.5f*yl, 0.5f*zl, //C
    -0.5f*xl, -0.5f*yl, 0.5f*zl, //D
    -0.5f*xl, 0.5f*yl, 0.5f*zl, //E
    0.5f*xl, 0.5f*yl, 0.5f*zl, //F
    0.5f*xl, 0.5f*yl, -0.5f*zl, //G
    -0.5f*xl, 0.5f*yl, -0.5f*zl  //H
  };

  const GLuint INDEXES[] = {
    0, 1, 2, 0, 2, 3, //ABC, ACD
    0, 3, 4, 0, 7, 4, //ADE, AHE
    0, 7, 1, 6, 7, 1, //AHB, GHB
    6, 1, 2, 6, 5, 2, //GBC, FBC
    3, 2, 4, 4, 5, 2, //EFC, HFC
    7, 4, 5, 7, 6, 5  //HEF, HGF
  };

  auto model = std::make_shared<nde::MonochromeIndexedModel>(VERTICES, sizeof(VERTICES), INDEXES, sizeof(INDEXES), color);
  return model;

}


class TexturedCubeModel {

public:
  static constexpr GLfloat CUBE_VERTICES[] = {
    // ABCD face
    -0.5f, -0.5f, -0.5f, //A
    0.5f, -0.5f, -0.5f, //B
    0.5f, -0.5f, 0.5f, //C
    -0.5f, -0.5f, 0.5f, //D

    //EFGH face
    -0.5f, 0.5f, 0.5f, //E
    0.5f, 0.5f, 0.5f, //F
    0.5f, 0.5f, -0.5f, //G
    -0.5f, 0.5f, -0.5f,  //H

    //ABGH, CDEF, BCFG and ADHE faces 
    -0.5f, -0.5f, -0.5f, //A
    0.5f, -0.5f, -0.5f, //B
    0.5f, -0.5f, 0.5f, //C
    -0.5f, -0.5f, 0.5f, //D

    -0.5f, 0.5f, 0.5f, //E
    0.5f, 0.5f, 0.5f, //F
    0.5f, 0.5f, -0.5f, //G
    -0.5f, 0.5f, -0.5f,  //H

  };

  static constexpr GLuint CUBE_INDEXES[] = {
    0, 1, 2, 0, 2, 3, //ABC, ACD
    8, 11, 12, 8, 15, 12, //ADE, AHE
    8, 15, 9, 14, 15, 9, //AHB, GHB
    14, 9, 10, 14, 13, 10, //GBC, GFC
    11, 10, 12, 12, 13, 10, //EDC, EFC
    7, 4, 5, 7, 6, 5  //HEF, HGF
  };

  static constexpr GLfloat CUBE_TEXTURE_COORDINATES[] = {
    // ABCD face
    0.0f, 0.0f, //A 
    0.0f, 1.0f, //B
    1.0f, 1.0f, //C
    1.0f, 0.0f, //D

    //EFGH face
    0.0f, 0.0f, //E
    1.0f, 0.0f, //F
    1.0f, 1.0f, //G
    0.0f, 1.0f, //H

    //ABGH, CDEF, BCFG and ADHE faces 

    0.0f, 0.0f, //A 
    0.0f, 1.0f, //B
    0.0f, 0.0f, //C
    0.0f, 1.0f, //D

    1.0f, 1.0f, //E
    1.0f, 0.0f, //F
    1.0f, 1.0f, //G
    1.0f, 0.0f, //H

  };

  constexpr static unsigned verticesByteSize = sizeof(CUBE_VERTICES);
  constexpr static unsigned indexesByteSize = sizeof(CUBE_INDEXES);
  constexpr static unsigned texCoordByteSize = sizeof(CUBE_TEXTURE_COORDINATES);

public:

  //TexturedCubeModel():
  //  TexturedIndexedModel(CUBE_VERTICES, verticesByteSize, CUBE_TEXTURE_COORDINATES, texCoordByteSize, CUBE_INDEXES, indexesByteSize)
  //{}
};

constexpr GLfloat TexturedCubeModel::CUBE_VERTICES[];
constexpr GLuint TexturedCubeModel::CUBE_INDEXES[];
constexpr GLfloat TexturedCubeModel::CUBE_TEXTURE_COORDINATES[];


std::shared_ptr<nde::Model> generateBoxModel(nde::BoundingBox& box, glm::vec3 color)
{
  return generateRectangularModel(
    box.asAxisAlignedBoundingBox().end().x - box.asAxisAlignedBoundingBox().begin().x,
    box.asAxisAlignedBoundingBox().end().y - box.asAxisAlignedBoundingBox().begin().y,
    box.asAxisAlignedBoundingBox().end().z - box.asAxisAlignedBoundingBox().begin().z,
    color);
}


#endif //NDE_DEVTOOLS_HPP