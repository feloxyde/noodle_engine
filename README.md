# Noodle Engine 

This project is currently paused, the most advanced branch is ecs_physics_terrain. If you want to reuse components or fork this project, please notify me and I'll clean it if needed.



A tiny game engine, running natively on Linux, code-based.

I want to address a big Thank You to Ole Christian Eidheim (@eidheim), who has helped this project to take a step forward by offering me an open subject project when I was an exchange at NTNU for the Fall2018 semester.

## Requirement

You shall install the following libraries in order to compile the project :

* **GLEW**  OpenGL ease of use
* **glfw**  Windowing 
* **GL**    For 3D rendering (OpenGL), is provided by a third party, as mesa. 
* **X11**   X11 server interfacing, used by glfw, will usually be provided by your distribution.
* **Xi**    Input library
* **Xrandr** Xrandr, for screen handling
* **Xxf86vm**  Video mode lib
* **Xinerama** Multiple display handling, (will be replaced by RandR if possible later on)
* **Xcursor**   
* **rt**        
* **m**     mathematics, from the start on your distribution
* **pthread**  threading, from the start on your distribution
* **dl**        dynamic loading, should there from the start as well
* **UnitTest++** for testing purpose, can be named unittestpp or alike in some distribs
* **glm** A 3D geometry library

Please note that not all the library may be needed to compile and run the code strictly speaking, but these libraries are meant to provide flexibility while developing the engine. 
 
I currently use an Arch-based distro, Manjaro, to develop. Any other Linux distro should be fine, but if a problem occur, please try using an Arch-based.

Under an Arch-based distribution, the following command should install dependencie : 

sudo pacman -S glew glfw-x11 libxi libxxf86vm libxinerama libxcursor unittestpp glm

## Installation and running example

First, download the release or the master branch of the repos. 

Then install the dependencies specified into requirements if not already done.

Then, go to the root of the directory. **cd noodle\_engine/**

Create a "build" directory and go there. **mkdir build && cd build**

copy the "resources" directory there. **cp -R ../resources .**

run CMake. **cmake ..**

build. **make**

run tests. **make test**

run the example. **./bin/ColorHopper** 

A pink screen should appear, press Q/A (Qwerty/Azerty keyboards) to start simulation, use mouse to look around, WASD/ZQSD to move.
pressing space allows you to jump, hold shift to slide. pressing space while sliding will do a slidejump, propelling you in the direction you look.
Enjoy moving around

Be sure to check the wiki of the repos for more informations !


## Contributing

Contributions are always welcome. However, the code is currently quite messy and not prone to contributions. If you want to contribute to the code please send me a message on GitLab or at felix.bertoni987@gmail.com.

Points where a contribution will be particularly needed : 
* Creating animations tools and primitives
* Advanced rendering effects
* Networking
* User Interface design.

**You can contribute** right now out another way than coding : Use the engine, read the code, give feedback, share it with your friends and family. Especially if you use the engine, or are interested into using it later, tell me, it would be a wonderful source of motivation for me. 

**Testing** is as well a nice option to contribute, and tests will forever be appreciated and useful. Soon will be added explanations on how to use my test structure based on UnitTest++, but you can as well provide an additional structure. 
